# load the adapter
import psycopg2

# load the psycopg extras module
import psycopg2.extras

import json
# Try to connect


def create_table(sqls):
    conn = None
    try:
        conn = psycopg2.connect("dbname='db_ipsl' user='projectuser' password='ngaeprom7861'")
        cur = conn.cursor()

		# create table one 
        for sql in sqls:
            cur.execute(sql)
            print('create Table',sql)
		# close communication with thte PostgreSQL database server
        cur.close()
		# commit 
        conn.commit()

    #except Exception as e:
    #    print ("I am unable to connect to the database.")

    finally:
        if conn is not None:
            conn.close()


sqls = (
    """
    CREATE TABLE T01grid (
        grid_id INTEGER PRIMARY KEY,
        point jsonb,
        coordinates jsonb
    );
    """,
    """
    CREATE TABLE T02future_rpc85 (
        grid_id INTEGER PRIMARY KEY,
        index_su jsonb,
        index_fd jsonb,
        index_id jsonb,
        index_tr jsonb,
        index_gsl jsonb,
        index_dtr jsonb,
        index_txx jsonb,
        index_tnx jsonb,
        index_txn jsonb,
        index_tnn jsonb,
        index_rx1day jsonb,
        index_rx5day jsonb,
        index_sdii jsonb,
        index_r10mm jsonb,
        index_r20mm jsonb,
        index_rnnmm jsonb,
        index_cdd jsonb,
        index_cwd jsonb,
        FOREIGN KEY (grid_id) REFERENCES T01grid (grid_id)
    );
    """,
    """
    CREATE TABLE T03future_rpc45 (
        grid_id INTEGER PRIMARY KEY,
        index_su jsonb,
        index_fd jsonb,
        index_id jsonb,
        index_tr jsonb,
        index_gsl jsonb,
        index_dtr jsonb,
        index_txx jsonb,
        index_tnx jsonb,
        index_txn jsonb,
        index_tnn jsonb,
        index_rx1day jsonb,
        index_rx5day jsonb,
        index_sdii jsonb,
        index_r10mm jsonb,
        index_r20mm jsonb,
        index_rnnmm jsonb,
        index_cdd jsonb,
        index_cwd jsonb,
        FOREIGN KEY (grid_id) REFERENCES T01grid (grid_id)
    );
    """,
    """
    CREATE TABLE T04historical (
        grid_id INTEGER PRIMARY KEY,
        index_su jsonb,
        index_fd jsonb,
        index_id jsonb,
        index_tr jsonb,
        index_gsl jsonb,
        index_dtr jsonb,
        index_txx jsonb,
        index_tnx jsonb,
        index_txn jsonb,
        index_tnn jsonb,
        index_rx1day jsonb,
        index_rx5day jsonb,
        index_sdii jsonb,
        index_r10mm jsonb,
        index_r20mm jsonb,
        index_rnnmm jsonb,
        index_cdd jsonb,
        index_cwd jsonb,
        FOREIGN KEY (grid_id) REFERENCES T01grid (grid_id)
    );
    """,
    """
    CREATE TABLE T05hist_rpc85 (
        grid_id INTEGER PRIMARY KEY,
        index_wsdi jsonb,
        index_csdi jsonb,
        index_tn10p jsonb,
        index_tx10p jsonb,
        index_tn90p jsonb,
        index_tx90p jsonb,
        index_r95ptot jsonb,
        index_r99ptot jsonb,
        index_prctot jsonb,
        FOREIGN KEY (grid_id) REFERENCES T01grid (grid_id)
    );
    """,
    """
    CREATE TABLE T06hist_rpc45 (
        grid_id INTEGER PRIMARY KEY,
        index_wsdi jsonb,
        index_csdi jsonb,
        index_tn10p jsonb,
        index_tx10p jsonb,
        index_tn90p jsonb,
        index_tx90p jsonb,
        index_r95ptot jsonb,
        index_r99ptot jsonb,
        index_prctot jsonb,
        FOREIGN KEY (grid_id) REFERENCES T01grid (grid_id)
    );
    """,
    """
    CREATE TABLE T07measure_temp (
        grid_id INTEGER PRIMARY KEY,
        ms_avg jsonb,
        ms_max jsonb,
        ms_min jsonb,
        FOREIGN KEY (grid_id) REFERENCES T01grid (grid_id)
    );
    """,
    """
    CREATE TABLE T08measure_prec (
        grid_id INTEGER PRIMARY KEY,
        ms_avg jsonb,
        ms_max jsonb,
        ms_min jsonb,
        FOREIGN KEY (grid_id) REFERENCES T01grid (grid_id)
    );
    """
)

sql0 = (
    """
    CREATE TABLE T07measure_rpc85 (
        grid_id INTEGER PRIMARY KEY,
        ts_avg jsonb,
        ts_max jsonb,
        ts_min jsonb,
        pr_avg jsonb,
        pr_max jsonb,
        pr_min jsonb,
        FOREIGN KEY (grid_id) REFERENCES T01grid (grid_id)
    );
    """,
    """
    CREATE TABLE T08measure_rpc45 (
        grid_id INTEGER PRIMARY KEY,
        ts_avg jsonb,
        ts_max jsonb,
        ts_min jsonb,
        pr_avg jsonb,
        pr_max jsonb,
        pr_min jsonb,
        FOREIGN KEY (grid_id) REFERENCES T01grid (grid_id)
    );
    """
)

sql1 = (
    """
    CREATE TABLE ipsl85 (
        grid_id INTEGER PRIMARY KEY,
        index_su jsonb,
        index_fd jsonb,
        index_id jsonb,
        index_tr jsonb,
        index_gsl jsonb,
        index_dtr jsonb,
        index_txx jsonb,
        index_tnx jsonb,
        index_txn jsonb,
        index_tnn jsonb,
        index_rx1day jsonb,
        index_rx5day jsonb,
        index_sdii jsonb,
        index_r10mm jsonb,
        index_r20mm jsonb,
        index_rnnmm jsonb,
        index_cdd jsonb,
        index_cwd jsonb,
        FOREIGN KEY (grid_id) REFERENCES T01grid (grid_id)
    );
    """,
    """
    CREATE TABLE ipsl85_30 (
        grid_id INTEGER PRIMARY KEY,
        index_su jsonb,
        index_fd jsonb,
        index_id jsonb,
        index_tr jsonb,
        index_gsl jsonb,
        index_dtr jsonb,
        index_txx jsonb,
        index_tnx jsonb,
        index_txn jsonb,
        index_tnn jsonb,
        index_rx1day jsonb,
        index_rx5day jsonb,
        index_sdii jsonb,
        index_r10mm jsonb,
        index_r20mm jsonb,
        index_rnnmm jsonb,
        index_cdd jsonb,
        index_cwd jsonb,
        FOREIGN KEY (grid_id) REFERENCES T01grid (grid_id)
    );
    """
)

sql2 = (
    """
    CREATE TABLE T10measure_rpc85 (
        year_id INTEGER PRIMARY KEY,
        year integer NOT NULL,
        ts_avg jsonb,
        ts_max jsonb,
        ts_min jsonb,
        pr_avg jsonb,
        pr_max jsonb,
        pr_min jsonb
    );
    """,
    """
    CREATE TABLE T11measure_rpc45 (
        year_id INTEGER PRIMARY KEY,
        year integer NOT NULL,
        ts_avg jsonb,
        ts_max jsonb,
        ts_min jsonb,
        pr_avg jsonb,
        pr_max jsonb,
        pr_min jsonb
    );
    """
)

sql3 = (
    """
    CREATE TABLE T09measure_hist (
        year_id INTEGER PRIMARY KEY,
        year integer NOT NULL,
        ts_avg jsonb,
        ts_max jsonb,
        ts_min jsonb,
        pr_avg jsonb,
        pr_max jsonb,
        pr_min jsonb
    );
    """,
    """
    CREATE TABLE hist_ms (
        grid_id INTEGER PRIMARY KEY,
        ts_avg jsonb,
        ts_max jsonb,
        ts_min jsonb,
        pr_avg jsonb,
        pr_max jsonb,
        pr_min jsonb
    );
    """
)
sql4 = (
    """
    CREATE TABLE t12index_rpc85 (
        year_id INTEGER PRIMARY KEY,
        year integer NOT NULL,
        index_su jsonb,
        index_fd jsonb,
        index_id jsonb,
        index_tr jsonb,
        index_gsl jsonb,
        index_dtr jsonb,
        index_txx jsonb,
        index_tnx jsonb,
        index_txn jsonb,
        index_tnn jsonb,
        index_rx1day jsonb,
        index_rx5day jsonb,
        index_sdii jsonb,
        index_r10mm jsonb,
        index_r20mm jsonb,
        index_rnnmm jsonb,
        index_cdd jsonb,
        index_cwd jsonb
    );
    """,
    """
    CREATE TABLE t13index_rpc45 (
        year_id INTEGER PRIMARY KEY,
        year integer NOT NULL,
        index_su jsonb,
        index_fd jsonb,
        index_id jsonb,
        index_tr jsonb,
        index_gsl jsonb,
        index_dtr jsonb,
        index_txx jsonb,
        index_tnx jsonb,
        index_txn jsonb,
        index_tnn jsonb,
        index_rx1day jsonb,
        index_rx5day jsonb,
        index_sdii jsonb,
        index_r10mm jsonb,
        index_r20mm jsonb,
        index_rnnmm jsonb,
        index_cdd jsonb,
        index_cwd jsonb
    );
    """,
    """
    CREATE TABLE t14index_hist (
        year_id INTEGER PRIMARY KEY,
        year integer NOT NULL,
        index_su jsonb,
        index_fd jsonb,
        index_id jsonb,
        index_tr jsonb,
        index_gsl jsonb,
        index_dtr jsonb,
        index_txx jsonb,
        index_tnx jsonb,
        index_txn jsonb,
        index_tnn jsonb,
        index_rx1day jsonb,
        index_rx5day jsonb,
        index_sdii jsonb,
        index_r10mm jsonb,
        index_r20mm jsonb,
        index_rnnmm jsonb,
        index_cdd jsonb,
        index_cwd jsonb
    );
    """,
    """
    CREATE TABLE cal130_rpc85 (
        grid_id INTEGER PRIMARY KEY,
        index_su integer,
        index_fd integer,
        index_id integer,
        index_tr integer,
        index_gsl integer,
        index_dtr integer,
        index_txx integer,
        index_tnx integer,
        index_txn integer,
        index_tnn integer,
        index_rx1day integer,
        index_rx5day integer,
        index_sdii integer,
        index_r10mm integer,
        index_r20mm integer,
        index_rnnmm integer,
        index_cdd integer,
        index_cwd integer
    );
    """,
    """"
    CREATE TABLE base130_rpc85 (
        grid_id INTEGER PRIMARY KEY,
        index_su jsonb,
        index_fd jsonb,
        index_id jsonb,
        index_tr jsonb,
        index_gsl jsonb,
        index_dtr jsonb,
        index_txx jsonb,
        index_tnx jsonb,
        index_txn jsonb,
        index_tnn jsonb,
        index_rx1day jsonb,
        index_rx5day jsonb,
        index_sdii jsonb,
        index_r10mm jsonb,
        index_r20mm jsonb,
        index_rnnmm jsonb,
        index_cdd jsonb,
        index_cwd jsonb,
        FOREIGN KEY (grid_id) REFERENCES T01grid (grid_id)
    );""",
    """
    CREATE TABLE t15_baseY130_rpc85 (
        year_id INTEGER PRIMARY KEY,
        n_year integer NOT NULL,
        index_su jsonb,
        index_fd jsonb,
        index_id jsonb,
        index_tr jsonb,
        index_gsl jsonb,
        index_dtr jsonb,
        index_txx jsonb,
        index_tnx jsonb,
        index_txn jsonb,
        index_tnn jsonb,
        index_rx1day jsonb,
        index_rx5day jsonb,
        index_sdii jsonb,
        index_r10mm jsonb,
        index_r20mm jsonb,
        index_rnnmm jsonb,
        index_cdd jsonb,
        index_cwd jsonb,
        ts_avg jsonb,
        ts_max jsonb,
        ts_min jsonb,
        pr_avg jsonb,
        pr_max jsonb,
        pr_min jsonb
    );
    """
)
create_table(sql4)
#create_table(sqls)
