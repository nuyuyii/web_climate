#from climate.models import Grid
import json
import numpy as np

from netCDF4 import Dataset
import datetime as dt 
from decimal import *
#-------------------Database Psql----------------------
import random, string, psycopg2,time
from string import ascii_lowercase
from random import randint


# load the psycopg extras module
import psycopg2.extras

start_time = time.time()


cnt=1
Sl_lisYear=range(1970,2101)
Sl_eyear=[format(x,'04d') for x in Sl_lisYear]


eindex=["index_su","index_fd","index_id",
    "index_tr","index_gsl","index_dtr",
    "index_txx","index_tnx","index_txn",
    "index_tnn","index_rx1day","index_rx5day",
    "index_sdii","index_r10mm","index_r20mm",
    "index_rnnmm","index_cdd","index_cwd" ]
#    "ts_avg","ts_max","ts_min","pr_avg","pr_max","pr_min"]

sqlInsert = """INSERT INTO t15_baseY130_rpc85
         VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s,
          %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) """ # %s, %s, %s, %s, %s, %s)"""

try:
    conn = psycopg2.connect("dbname='db_ipsl' user='projectuser' password='ngaeprom7861'")
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cur.execute("SELECT table_name FROM information_schema.tables WHERE table_schema = 'public' ORDER BY table_name ASC;")
    tablename=cur.fetchall()
    sqlH = "SELECT * from base130_rpc85 WHERE grid_id={myind}"
    sqlHms = "SELECT * from hist_ms WHERE grid_id={myind}"
    sqlFms = "SELECT * from t07measure_rpc85 WHERE grid_id={myind}"
    
    cal_130 = {}
    for i in range(0,len(eindex)):
           cal_130[eindex[i]]=[]

    for ind in range(1,48324): #48323
        #var='25300'
        value = {}

        cur.execute(sqlH.format(myind=str(ind)))
        H_rows = cur.fetchall()
#        cur.execute(sqlHms.format(myind=str(ind)))
#        Hms_rows = cur.fetchall()
#        cur.execute(sqlFms.format(myind=str(ind)))
#        Fms_rows = cur.fetchall()

        for i in range(0,len(eindex)):
           value[eindex[i]]=[]

        for ey in range(0,len(Sl_eyear)):
            for inx in range(0,len(eindex)):

#                if (inx<18):
                for row in H_rows:
                        #print("inx:" ,inx, "--")
                    value[eindex[inx]].append(row[eindex[inx]][Sl_eyear[ey]])

#                else:
#                    if (int(Sl_eyear[ey])<2006):
#                        for row in Hms_rows:
#                            #o = row[eindex[inx]][Sl_eyear[ey]]
#                            #if(o<-200):
#                            #    print(Sl_eyear[ey]," indx:",eindex[inx])
#                            value[eindex[inx]].append(row[eindex[inx]][Sl_eyear[ey]])
#                    else:
#                        for row in Fms_rows:
#                            o = row[eindex[inx]][Sl_eyear[ey]]
#                            if(int(Sl_eyear[ey])==2086):
#                                #o = (row[eindex[inx]][Sl_eyear[ey-1]]+row[eindex[inx]][Sl_eyear[ey+1]] )/2
#                                print(Sl_eyear[ey]," indx:",eindex[inx]," val:",o)
#                            value[eindex[inx]].append(row[eindex[inx]][Sl_eyear[ey]])



        for i in range(0,len(eindex)):           
#           if (i<19 or i==21):
            cal = np.average(value[eindex[i]])
            cal_130[eindex[i]].append(float("%.2f" % cal))
#            elif (i==19 or i==22):
#                cal = np.max(value[eindex[i]])
#                cal_130[eindex[i]].append(float("%.2f" % cal))
#            elif (i==20 or i==23):
#                cal = np.min(value[eindex[i]])
#                cal_130[eindex[i]].append(float("%.2f" % cal))
         
        if (ind%1000==0):
            print("ind: ", ind)   

    for i in range(0,len(eindex)):
        cal_130[eindex[i]] = json.dumps(cal_130[eindex[i]])        

    cur.execute(sqlInsert,('1','130',  cal_130[eindex[0]], cal_130[eindex[1]], cal_130[eindex[2]],
                          cal_130[eindex[3]], cal_130[eindex[4]], cal_130[eindex[5]], cal_130[eindex[6]],
                          cal_130[eindex[7]], cal_130[eindex[8]], cal_130[eindex[9]], cal_130[eindex[10]],
                          cal_130[eindex[11]], cal_130[eindex[12]], cal_130[eindex[13]], cal_130[eindex[14]],
                          cal_130[eindex[15]], cal_130[eindex[16]], cal_130[eindex[17]] ))
#                          cal_130[eindex[18]], cal_130[eindex[19]], cal_130[eindex[20]],
#                          cal_130[eindex[21]], cal_130[eindex[22]], cal_130[eindex[23]] ))
    conn.commit()
#        for row in H_rows:
#            print ('----------------')
#            for year in H_eyear:
#                print (year,'--',row[eindex[0]][year])



    for x in tablename:
        for y in x:
            if (y=='t04historical'):
#cur.execute("SELECT column1, column2 FROM {mytable} ORDER BY column1 ASC". format(mytable=y))
#mytest = cur.fetchall()
                print (y)
                #cur.execute(sqlH,var)
                #rows = cur.fetchall()
                """for row in rows:
                    print('--',row[eindex[0]][eyear[0]])
                for row in rowsd:
                    print(row[eindex[0]]['2006'])"""

            if (y=='t02future_rpc85'):
                print (y)
                #cur.execute(sqlIPSL85,var)
                #rowsd = cur.fetchall()
                
                
except  (psycopg2.DatabaseError, e):
    print ('Error %s' % e  )



conn.close()
print('---%s seconds---'%(time.time()-start_time))
