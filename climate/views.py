from django.shortcuts import render, redirect
from django.core import serializers
from django.template import loader, Context
from django.http import JsonResponse
from decimal import *


from climate.cal_index import Indexs
from climate.thai_graph import Indexs_thai
from climate.cambo_graph import Indexs_cambo
from climate.vietn_graph import Indexs_vietn
from climate.lao_graph import Indexs_lao
from climate.plot_grap import Indexs_page2


from climate.map_index import Indx_map, Compare_map
#from climate.map_index import Compare_map


import csv
import json
import numpy as np
import datetime as dt

from django.conf import settings
import os

def map(request):

    return render(request, 'About.html')#, {'lon':json.dumps(lat)})

def home_page(request):    
    cli2 = Indexs()
    cli_thai = Indexs_thai()
    cli_cambo = Indexs_cambo()
    cli_lao = Indexs_lao()
    cli_vietn = Indexs_vietn()


    if request.method == 'POST':
        #POST goes here . is_ajax is must to capture ajax requests. Beginner's pit.
        if request.is_ajax():
            data = {}
            Time_start = request.POST.get('time_start')
            type_class = request.POST.get('set_type')
            if (Time_start is not None):
                Time_end = request.POST.get('time_end')
                #print("Time_start--",Time_start,'  Time_end--')
                indx = request.POST.get('indx')  
                rcp = request.POST.get('rcp')
                gd_size = request.POST.get('path_gd')
                s_table = request.POST.get('s_table')
                #map_climate = Indx_map(0)
                map_climate = Indx_map(gd_size)
                index_json,maxValue,minValue = map_climate.check_indx(indx,int(Time_start),int(Time_end)+1,rcp,s_table)
                print("maxValue: ", maxValue,"  minValue: ",minValue)
                data = {"mapValue":index_json} #,"maxValue":maxValue,"minValue":minValue}
                print("indx--",indx)
            elif (type_class is not None):
                print("type_class: ",type_class)
                file1 = request.POST.get('path_file')
                file2 = request.POST.get('path_file2')
                if (type_class == "Thailand"):
                    data = {"path_file":getattr(cli_thai, file1)(),"path_file2":getattr(cli_thai,file2)()} 
                elif (type_class == "Cambodia"):
                    data = {"path_file":getattr(cli_cambo, file1)(),"path_file2":getattr(cli_cambo,file2)()} 
                elif (type_class == "Lao People's Democratic Republic"):
                    data = {"path_file":getattr(cli_lao, file1)(),"path_file2":getattr(cli_lao,file2)()} 
                elif (type_class == "Viet Nam"):
                    data = {"path_file":getattr(cli_vietn, file1)(),"path_file2":getattr(cli_vietn,file2)()}
                else: 
                    data = {"path_file":getattr(cli2, file1)(),"path_file2":getattr(cli2,file2)()}

            else:
                #Always use get on request.POST. Correct way of querying a QueryDict.
                file1 = request.POST.get('path_file')
                file2 = request.POST.get('path_file2')
                gd_size = request.POST.get('path_gd')
                indx = request.POST.get('indx')      
                rcp = request.POST.get('rcp')
                s_table = request.POST.get('s_table') 
                map_climate = Indx_map(gd_size)               
                index_json,maxValue,minValue = map_climate.check_indx(indx,1970,2101,rcp,s_table)
                type_class = request.POST.get('set_type') 
                            
                print("gd_size: ",gd_size)
                
                #if(type_class == "Thailand"):
                #    
                #elif(type_class == "all"):
                data = {"path_file":getattr(cli2, file1)(),"path_file2":getattr(cli2,file2)(),"mapValue":index_json,"maxValue":maxValue,"minValue":minValue} 
            
                  
            #Returning same data back to browser.It is not possible with Normal submit
            #print(file2)
            #print(data["path_file"])
            #print("#------------------------#")
            #print(file2)
            #print("#------------------------#")
            #print(getattr(cli2, file2)())
            return JsonResponse(data)

    eindex=["ts_avg","ts_max","ts_min","pr_avg","pr_max","pr_min"]
    map_climate = Indx_map(0)
    index_json,maxValue,minValue = map_climate.check_indx("avg1",1970,2101,"45","ms")

    # Check select index
    #index_json,maxValue,minValue = map_climate.check_indx("avg1",2084,2086,"85","ms")
    #index_json,maxValue,minValue = map_climate.check_indx("fd1",2087,2088,"45","indx")
    #index_json,maxValue,minValue = map_climate.check_indx("fd1",2084,2086,"85","indx")
    #print("maxValue: ", maxValue,"  minValue: ",minValue)

    max_data = cli2.avg1_45()
    max_data85 = cli2.avg1_85()

    return render(request, 'map_graph.html',
                  {'max_ts45':json.dumps(max_data),'max_ts85':json.dumps(max_data85),
                   'map_data':json.dumps(index_json),
                   'len_min':minValue,'len_max':maxValue})



def conctrat_page(request):

    indexs_cli = Indexs_page2()
    tsmaxhist,tsmax_45,tsmax_85 = indexs_cli.avg1()

    map_plot = Compare_map()

    if request.method == 'POST':

        if request.is_ajax():
            data = {}
            #Always use get on request.POST. Correct way of querying a QueryDict.
            path_file = request.POST.get('path_file')
            gd_size = request.POST.get('path_gd')
           
            hist,rcp_45,rcp_85 = getattr(indexs_cli, path_file)()
            json_hist,json_rcp45,json_rcp85 = map_plot.call_value(path_file,gd_size)

            data = {"hist_data":hist,"rcp45":rcp_45,"rcp85":rcp_85,
                    "map_hist":json_hist,"map_rcp45":json_rcp45, "map_rcp85":json_rcp85}   
            
            # Returning same data back to browser.It is not possible with Normal submit
            return JsonResponse(data)

    json_hist,json_rcp45,json_rcp85 = map_plot.call_value("avg1","0")

    return render(request, 'map_Contrast.html',
                  {'max_ts_hist':json.dumps(tsmaxhist),'max_ts45':json.dumps(tsmax_45),
                   'max_ts85':json.dumps(tsmax_85), 'map_hist':json_hist, 'map_rcp45':json_rcp45,
                   'map_rcp85':json_rcp85})


