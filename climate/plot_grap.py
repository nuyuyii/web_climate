from decimal import *
import csv
import json
import numpy as np
import datetime as dt
from climate.models import cal_mounlyrcp85,cal_mounlyhis,cal_mounlyrcp45

class Indexs_page2(object):
    """docstring for Indexs_page2"""
    def __init__(self):
        super(Indexs_page2, self).__init__()
        self.num_point = 5
        self.str_year = 1970
#----------------------------------rcp45-----------------------------------
    def max1(self):

        grid_dat1 = cal_mounlyhis.objects.get(id=1)
        grid_dat2 = cal_mounlyrcp45.objects.get(id=1)
        grid_dat3 = cal_mounlyrcp85.objects.get(id=1)
        print("--------------------------------------")
        data = grid_dat3.ts_max
        data2 = grid_dat2.ts_max
        for i in range(1,len(data)):
            for j in range(1, 13):
                if j <= 9:
                    if data[i][0] == '2084'+"-0"+str(j)+"-00":
                        data[i][1] = (data[i-24][1] + data[i+48][1])/2
                        #print(data[i+36][0])
                        #print(data[i-10][0])
                    if data[i][0] == '2085'+"-0"+str(j)+"-00":
                        data[i][1] = (data[i-12][1] + data[i+36][1])/2
                        #print(data[i][0])
                        #print(data[i-10][0])
                elif j >= 10:
                    if data[i][0] == '2084'+"-"+str(j)+"-00":
                        data[i][1] = (data[i-24][1] + data[i+48][1])/2
                    if data[i][0] == '2085'+"-0"+str(j)+"-00":
                        data[i][1] = (data[i-12][1] + data[i+36][1])/2
                        #print(data[i][0])
                        #print(data[i-10][0])
                        #print(data[i][1])

        # print(data)
        return grid_dat1.ts_max,data2,data

    def min1(self):

        grid_dat1 = cal_mounlyhis.objects.get(id=1)
        grid_dat2 = cal_mounlyrcp45.objects.get(id=1)
        grid_dat3 = cal_mounlyrcp85.objects.get(id=1)

        return grid_dat1.ts_min,grid_dat2.ts_min,grid_dat3.ts_min

    def avg1(self):

        grid_dat1 = cal_mounlyhis.objects.get(id=1)
        grid_dat2 = cal_mounlyrcp45.objects.get(id=1)
        grid_dat3 = cal_mounlyrcp85.objects.get(id=1)
        data = grid_dat3.ts_avg
        data2 = grid_dat2.ts_avg
        for i in range(1,len(data)):
            for j in range(1, 13):
                if j <= 9:
                    if data[i][0] == '2084'+"-0"+str(j)+"-00":
                        data[i][1] = (data[i-12][1] + data[i+36][1])/2
                    if data[i][0] == '2085'+"-0"+str(j)+"-00":
                        data[i][1] = (data[i-10][1] + data[i+30][1])/2
                elif j >= 10:
                    if data[i][0] == '2084'+"-"+str(j)+"-00":
                        data[i][1] = (data[i-12][1] + data[i+36][1])/2
                    if data[i][0] == '2085'+"-0"+str(j)+"-00":
                        data[i][1] = (data[i-12][1] + data[i+36][1])/2
        for i in range(1,len(data2)):
            for j in range(1, 13):
                if j <= 9:
                    if data2[i][0] == '2086'+"-0"+str(j)+"-00":
                        data2[i][1] = (data2[i-12][1] + data2[i+36][1])/2
                elif j >= 10:
                    if data2[i][0] == '2086'+"-"+str(j)+"-00":
                        data2[i][1] = (data2[i-12][1] + data2[i+36][1])/2
        
        return grid_dat1.ts_avg,data2,data

    def max2(self):

        grid_dat1 = cal_mounlyhis.objects.get(id=1)
        grid_dat2 = cal_mounlyrcp45.objects.get(id=1)
        grid_dat3 = cal_mounlyrcp85.objects.get(id=1)
            
        return grid_dat1.pr_max,grid_dat2.pr_max,grid_dat3.pr_max


    def min2(self):

        grid_dat1 = cal_mounlyhis.objects.get(id=1)
        grid_dat2 = cal_mounlyrcp45.objects.get(id=1)
        grid_dat3 = cal_mounlyrcp85.objects.get(id=1)
            
        return grid_dat1.pr_min,grid_dat2.pr_min,grid_dat3.pr_min

    def avg2(self):

        grid_dat1 = cal_mounlyhis.objects.get(id=1)
        grid_dat2 = cal_mounlyrcp45.objects.get(id=1)
        grid_dat3 = cal_mounlyrcp85.objects.get(id=1)
            
        return grid_dat1.pr_avg,grid_dat2.pr_avg,grid_dat3.pr_avg

