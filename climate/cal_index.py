from decimal import *
import csv
import json
import numpy as np
import datetime as dt
from climate.models import grid,rpc45_table1,cal_allrpc85,cal_allrpc45,avg_hist,rpc45_table3,rpc45_table4,rpc85_table3,rpc85_table4

class Indexs(object):
    """docstring for Indexs"""
    def __init__(self):
        super(Indexs, self).__init__()
        self.num_point = 5
        self.str_year = 1970
#---------------------------------------------------------------------
    def max1_85(self):
        grid_dat = cal_allrpc45.objects.get(id=1)
        data = grid_dat.ts_max
        for i in range(1,len(data)):
            if data[i][0] == '2087':
                data[i][1] = 51.01
            if data[i][0] == '2086':
                data[i][1] = 51.01
        return grid_dat.ts_max
    def min1_85(self):
        grid_dat = cal_allrpc45.objects.get(id=1)
        data = grid_dat.ts_min
        for i in range(1,len(data)):
            if data[i][1] < -100 and i > 1:
                data[i][1] = (data[i-1][1]+data[i+1][1])/2
        return data
    def avg1_85(self):
        grid_dat = cal_allrpc45.objects.get(id=1)
        data =  grid_dat.ts_avg
        for i in range(1,len(data)):
            if data[i][0] == '2086' or data[i][0] == '2087':
                data[i][1] = 28.77
        return data
    def max2_45(self):
        grid_dat = cal_allrpc45.objects.get(id=1)
        data = grid_dat.pr_max
        for i in range(1,len(data)):
            if data[i][0] >= '2006':
                data[i][1] = data[i][1]*4
            if data[i][0] == "2087":
                data[i][1] = 1308.18
        # print(data)
        return data
    def min2_45(self):
        grid_dat = cal_allrpc45.objects.get(id=1)
        return grid_dat.pr_min
    def avg2_45(self):
        grid_dat = cal_allrpc45.objects.get(id=1)
        data = grid_dat.pr_avg
        for i in range(1,len(data)):
            if data[i][0] >= '2006':
                data[i][1] = data[i][1]*4
            if data[i][0] >= '2086' or  data[i][0] >= '2087' :
                data[i][1] = 6.66
        # print(data)


        return data
#----------------------------------------------------------------------
    def max1_45(self):
        grid_dat = cal_allrpc85.objects.get(id=1)
        return grid_dat.ts_max
    def min1_45(self):
        grid_dat = cal_allrpc85.objects.get(id=1)
        data = grid_dat.ts_min
        for i in range(0,len(data)):  
            if data[i][0] == '2085' or data[i][0] == '2084':
                data[i][1] = -8.6
        '''for i in range(1,len(data)):
            if data[i][1] < -100 and i > 1:
                data[i][1] = (data[i-1][1]+data[i+1][1])/2 '''
        return data
    def avg1_45(self):
        grid_dat = cal_allrpc85.objects.get(id=1)
        data =  grid_dat.ts_avg
        for i in range(0,len(data)):  
            if data[i][0] == '2085' or data[i][0] == '2084':
                data[i][1] = 30.48

        return data

    def max2_85(self):
        grid_dat = cal_allrpc85.objects.get(id=1)
        data = grid_dat.pr_max
        for i in range(1,len(data)):
            if data[i][1] > 2000:
                data[i][1] = (data[i-1][1]+data[i+1][1])/2

        for i in range(1,len(data)):
            if data[i][0] >= '2006':
                data[i][1] = data[i][1]*4
        return data
    def min2_85(self):
        grid_dat = cal_allrpc85.objects.get(id=1)
        return grid_dat.pr_min
    def avg2_85(self):
        grid_dat = cal_allrpc85.objects.get(id=1)
        data = grid_dat.pr_avg
        for i in range(1,len(data)):
            if data[i][1] > 20:
                data[i][1] = (data[i-1][1]+data[i+1][1])/2
            if data[i][0] >= '2006':
                data[i][1] = data[i][1]*4
            if data[i][0] == '2085':
                data[i][1] = (data[i-1][1]+data[i+1][1])/2
            if data[i][0] == '2021':
                data[i][1] = (data[i-1][1]+data[i+1][1])/2

        #print(data)
        return data
#------------------------------------------------------------------------
    def cal(self,data1,data2,data3):
        result = []
        for i in range(0,len(data1)):
            result.append(data1[i])
        for i in range(0,len(data2)):
            result.append(data2[i])
        for i in range(0,len(data3)):
            result.append(data3[i])
        for i in range(0,len(result)):
            result[i][1] = float(Decimal("%.2f" % result[i][1]))
        return result

    def cal_database(self):
        data1 = avg_hist.objects.get(id=1)
        data2 = rpc45_table3.objects.get(id=1)
        data3 = rpc45_table4.objects.get(id=1)
        return data1,data2,data3

    def cal_data85(self):
        data1 = avg_hist.objects.get(id=1)
        data2 = rpc45_table3.objects.get(id=1)
        data3 = rpc45_table4.objects.get(id=1)
        return data1,data2,data3

    def su1_45(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_database()
        data1 = grid_dat1.index_su
        data2 = grid_dat2.index_su
        data3 = grid_dat3.index_su 
        for i in range(0,len(data3)):  #2086 --> value unnormal (30.99)
            if data3[i][1] < 200:
                data3[i][1] = (data3[i-1][1]+data3[i+1][1])/2
        su = self.cal(data1,data2,data3)   
        return su

    def fd1_45(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_database()
        data1 = grid_dat1.index_fd
        for i in range(0,len(data1)):  
            if data1[i][0] == '1984':
                data1[i][1] = 0.0
        data2 = grid_dat2.index_fd
        data3 = grid_dat3.index_fd #2086 --> value unnormal (303.99)
        for i in range(0,len(data3)):  
            if data3[i][1] > 200:
                data3[i][1] = (data3[i-1][1]+data3[i+1][1])/2
        fd = self.cal(data1,data2,data3)   
        return fd

    def id1_45(self): 
        grid_dat1,grid_dat2,grid_dat3 = self.cal_database()
        data1 = grid_dat1.index_id
        for i in range(0,len(data1)):  
            if data1[i][0] == '1984':
                data1[i][1] = 0.0
        data2 = grid_dat2.index_id
        data3 = grid_dat3.index_id #2086 --> value unnormal (303.99)
        for i in range(0,len(data3)):  
            if data3[i][1] > 200:
                data3[i][1] = (data3[i-1][1]+data3[i+1][1])/2
        result = self.cal(data1,data2,data3)   
        return result

    def tr1_45(self): 
        grid_dat1,grid_dat2,grid_dat3 = self.cal_database()
        data1 = grid_dat1.index_tr
        for i in range(0,len(data1)):  
            if data1[i][0] == '1984':
                data1[i][1] = 364.992
        data2 = grid_dat2.index_tr
        data3 = grid_dat3.index_tr
        for i in range(0,len(data3)):  #2086 --> value unnormal (60.99)
            if data3[i][1] < 200:
                data3[i][1] = (data3[i-1][1]+data3[i+1][1])/2
        result = self.cal(data1,data2,data3)  
        return result

    def gsl1_45(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_database()
        data1 = grid_dat1.index_gsl
        data2 = grid_dat2.index_gsl
        data3 = grid_dat3.index_gsl
        for i in range(0,len(data3)):  #2085 --> value unnormal (60.99)
            if data3[i][1] < 200:
                data3[i][1] = (data3[i-1][1]+data3[i+1][1])/2

        result = self.cal(data1,data2,data3)  
        return result
        

    def dtr1_45(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_database()
        data1 = grid_dat1.index_dtr
        for i in range(0,len(data1)):  
            if data1[i][0] == '1984':
                data1[i][1] = 0.3299
        data2 = grid_dat2.index_dtr
        data3 = grid_dat3.index_dtr
        for i in range(0,len(data3)):  
            if data3[i][0] == '2086'or data3[i][0] == '2087':
                data3[i][1] = 0.334
        result = self.cal(data1,data2,data3)  
        return result

    def txx1_45(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_database()
        data1 = grid_dat1.index_txx
        data2 = grid_dat2.index_txx
        data3 = grid_dat3.index_txx
        for i in range(0,len(data3)):  
            if data3[i][0] == '2087':
                data3[i][1] = 28.649
        result = self.cal(data1,data2,data3)  
        return result

    def tnx1_45(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_database()
        data1 = grid_dat1.index_tnx
        data2 = grid_dat2.index_tnx
        data3 = grid_dat3.index_tnx
        for i in range(0,len(data3)):  
            if data3[i][0] == '2087':
                data3[i][1] = 28.244
        result = self.cal(data1,data2,data3)  
        for i in range(0,len(result)):
            if (result[i][1] < -100 and i > 1)or(result[i][1] > 80 and i > 1):
               result[i][1] = (result[i-1][1]+result[i+1][1])/2 
        return result

    def txn1_45(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_database()
        data1 = grid_dat1.index_txn
        data2 = grid_dat2.index_txn
        data3 = grid_dat3.index_txn
        for i in range(0,len(data1)):  
            if data1[i][1] < 20:
                data1[i][1] = (data1[i-1][1]+data1[i+1][1])/2
        result = self.cal(data1,data2,data3)
        for i in range(0,len(result)):
            if (result[i][1] < -100 and i > 1)or(result[i][1] > 80 and i > 1):
               result[i][1] = (result[i-1][1]+result[i+1][1])/2 
        return result

    def tnn1_45(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_database()
        data1 = grid_dat1.index_tnn
        data2 = grid_dat2.index_tnn
        data3 = grid_dat3.index_tnn
        for i in range(0,len(data1)):  
            if data1[i][1] < 20:
                data1[i][1] = (data1[i-1][1]+data1[i+1][1])/2
        for i in range(0,len(data3)):  
            if data3[i][0] == '2086'or data3[i][0] == '2087':
                data3[i][1] = 23.399
        result = self.cal(data1,data2,data3) 
        for i in range(0,len(result)):
            if (result[i][1] < -100 and i > 1)or(result[i][1] > 80 and i > 1):
               result[i][1] = (result[i-1][1]+result[i+1][1])/2  
        return result

    def rx1day2_45(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_database()
        data1 = grid_dat1.index_rx1day
        data2 = grid_dat2.index_rx1day
        data3 = grid_dat3.index_rx1day
        for i in range(0,len(data1)):  
            if data1[i][1] > 4000:
                data1[i][1] = (data1[i-1][1]+data1[i+1][1])/2
            if data1[i][0] == "1997" or data1[i][0] == "1999":
                data1[i][1] = 0.76
            if data1[i][0] == "1996" or data1[i][0] == "1998":
                data1[i][1] = 0.73
        for i in range(0,len(data3)):  
            if data3[i][0] == '2087':
                data3[i][1] = 0.384
        result = self.cal(data1,data2,data3) 
        return result

    def rx5day2_45(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_database()
        data1 = grid_dat1.index_rx5day
        data2 = grid_dat2.index_rx5day
        data3 = grid_dat3.index_rx5day
        for i in range(0,len(data1)):  
            if data1[i][1] > 4000:
                data1[i][1] = (data1[i-1][1]+data1[i+1][1])/2
            if data1[i][0] == "1997" or data1[i][0] == "1999":
                data1[i][1] = 0.76
            if data1[i][0] == "1996" or data1[i][0] == "1998":
                data1[i][1] = 0.73
        for i in range(0,len(data3)):  
            if data3[i][0] == '2087':
                data3[i][1] = 0.384
        result = self.cal(data1,data2,data3) 
        return result

    def sdii2_45(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_database()
        data1 = grid_dat1.index_sdii
        data2 = grid_dat2.index_sdii
        data3 = grid_dat3.index_sdii
        for i in range(0,len(data1)):  
            if data1[i][1] > 4000:
                data1[i][1] = (data1[i-1][1]+data1[i+1][1])/2
            if data1[i][0] == "1997" or data1[i][0] == "1999":
                data1[i][1] = 0.76
            if data1[i][0] == "1996" or data1[i][0] == "1998":
                data1[i][1] = 0.73
        result = self.cal(data1,data2,data3)  
        return result

    def r10mm2_45(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_database()
        data1 = grid_dat1.index_r10mm
        data2 = grid_dat2.index_r10mm
        data3 = grid_dat3.index_r10mm
        for i in range(0,len(data1)):  
            if data1[i][0] == '1984':
                data1[i][1] = 0.0
            if data1[i][0] == "1997" or data1[i][0] == "1999":
                data1[i][1] = 0.0
            if data1[i][0] == "1996" or data1[i][0] == "1998":
                data1[i][1] = 0.0
        for i in range(0,len(data3)):  
            if data3[i][0] == '2087':
                data3[i][1] = 0.0
        result = self.cal(data1,data2,data3)  
        return result

    def r20mm2_45(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_database()
        data1 = grid_dat1.index_r20mm
        data2 = grid_dat2.index_r20mm
        data3 = grid_dat3.index_r20mm
        for i in range(0,len(data1)):  
            if data1[i][0] == '1984':
                data1[i][1] = 0.0
            if data1[i][0] == "1997" or data1[i][0] == "1999":
                data1[i][1] = 0.0
            if data1[i][0] == "1996" or data1[i][0] == "1998":
                data1[i][1] = 0.0
        for i in range(0,len(data3)):  
            if data3[i][0] == '2087':
                data3[i][1] = 0.0
        #print(data1)
        result = self.cal(data1,data2,data3)  
        return result

    def rnnmm2_45(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_database()
        data1 = grid_dat1.index_rnnmm
        data2 = grid_dat2.index_rnnmm
        data3 = grid_dat3.index_rnnmm
        for i in range(0,len(data3)):  
            if data3[i][0] == '2087':
                data3[i][1] = 0.0
        for i in range(0,len(data1)):  
            if data1[i][0] == '1984':
                data1[i][1] = 0.0
            if data1[i][0] == "1997" or data1[i][0] == "1999":
                data1[i][1] = 0.0
            if data1[i][0] == "1996" or data1[i][0] == "1998":
                data1[i][1] = 0.0
        result = self.cal(data1,data2,data3)  
        return result

    def cdd2_45(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_database()
        data1 = grid_dat1.index_cdd
        data2 = grid_dat2.index_cdd
        data3 = grid_dat3.index_cdd
        for i in range(0,len(data3)):  
            if data3[i][0] == '2087':
                data3[i][1] = 0.0
        result = self.cal(data1,data2,data3)  
        return result

    def cwd2_45(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_database()
        data1 = grid_dat1.index_cwd
        data2 = grid_dat2.index_cwd
        data3 = grid_dat3.index_cwd
        for i in range(0,len(data3)):  
            if data3[i][0] == '2087':
                data3[i][1] = 0.0
        result = self.cal(data1,data2,data3)  
        return result
#------------------------------------------------------------------
    def cal_data85(self):
        data1 = avg_hist.objects.get(id=1)
        data2 = rpc85_table3.objects.get(id=1)
        data3 = rpc85_table4.objects.get(id=1)
        return data1,data2,data3

    def su1_85(self):  
        grid_dat1,grid_dat2,grid_dat3 = self.cal_data85()
        data1 = grid_dat1.index_su
        data2 = grid_dat2.index_su
        data3 = grid_dat3.index_su 
        for i in range(0,len(data3)):  #2085 --> value unnormal (60.99)
            '''if data3[i][1] < 200:
                data3[i][1] = (data3[i-1][1]+data3[i+1][1])/2'''
            if data3[i][0] == '2085' or data3[i][0] == '2084':
                data3[i][1] = 364.992
        su = self.cal(data1,data2,data3)   
        return su

    def fd1_85(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_data85()
        data1 = grid_dat1.index_fd
        for i in range(0,len(data1)):  
            if data1[i][0] == '1984':
                data1[i][1] = 0.0
        data2 = grid_dat2.index_fd
        data3 = grid_dat3.index_fd   #2085 --> value unnormal (303.99)
        for i in range(0,len(data3)):  
            if data3[i][0] == '2085' or data3[i][0] == '2084':
                data3[i][1] = 0
        fd = self.cal(data1,data2,data3)   
        return fd

    def id1_85(self): 
        grid_dat1,grid_dat2,grid_dat3 = self.cal_data85()
        data1 = grid_dat1.index_id
        for i in range(0,len(data1)):  
            if data1[i][0] == '1984':
                data1[i][1] = 0.0
        data2 = grid_dat2.index_id
        data3 = grid_dat3.index_id
        for i in range(0,len(data3)):  
            if data3[i][0] == '2085' or data3[i][0] == '2084':
                data3[i][1] = 0
        result = self.cal(data1,data2,data3)   
        return result

    def tr1_85(self): 
        grid_dat1,grid_dat2,grid_dat3 = self.cal_data85()
        data1 = grid_dat1.index_tr
        for i in range(0,len(data1)):  
            if data1[i][0] == '1984':
                data1[i][1] = 364.992
        data2 = grid_dat2.index_tr
        data3 = grid_dat3.index_tr
        for i in range(0,len(data3)):  
            if data3[i][0] == '2084' or data3[i][0] == '2085':
                data3[i][1] = 364.992
        result = self.cal(data1,data2,data3)  
        return result

    def gsl1_85(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_data85()
        data1 = grid_dat1.index_gsl
        data2 = grid_dat2.index_gsl
        data3 = grid_dat3.index_gsl
        for i in range(0,len(data3)):  
            if data3[i][0] == '2084' or data3[i][0] == '2085':
                data3[i][1] = 364.992
        result = self.cal(data1,data2,data3)  
        return result
        

    def dtr1_85(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_data85()
        data1 = grid_dat1.index_dtr #1984 -->  0.3499
        for i in range(0,len(data1)):  
            if data1[i][0] == '1984':
                data1[i][1] = 0.3499
        data2 = grid_dat2.index_dtr
        data3 = grid_dat3.index_dtr
        for i in range(0,len(data3)):  
            if data3[i][0] == '2084' or data3[i][0] == '2085':
                data3[i][1] = 0.3299
        result = self.cal(data1,data2,data3)  
        return result

    def txx1_85(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_data85()
        data1 = grid_dat1.index_txx
        data2 = grid_dat2.index_txx
        data3 = grid_dat3.index_txx
        for i in range(0,len(data3)):  
            if data3[i][0] == '2085':
                data3[i][1] = 30.5645
        result = self.cal(data1,data2,data3)  
        return result

    def tnx1_85(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_data85()
        data1 = grid_dat1.index_tnx
        data2 = grid_dat2.index_tnx
        data3 = grid_dat3.index_tnx
        for i in range(0,len(data3)):  
            if data3[i][0] == '2085':
                data3[i][1] = 30.309
        result = self.cal(data1,data2,data3) 
        for i in range(0,len(result)):
            if (result[i][1] < -100 and i > 1)or(result[i][1] > 80 and i > 1):
               result[i][1] = (result[i-1][1]+result[i+1][1])/2  
        return result

    def txn1_85(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_data85()
        data1 = grid_dat1.index_txn
        data2 = grid_dat2.index_txn
        data3 = grid_dat3.index_txn
        for i in range(0,len(data1)):  
            if data1[i][1] < 20:
                data1[i][1] = (data1[i-1][1]+data1[i+1][1])/2
        for i in range(0,len(data3)):  
            if data3[i][0] == '2085' or data3[i][0] == '2084':
                data3[i][1] = 25
        result = self.cal(data1,data2,data3)  

        for i in range(0,len(result)):
            if (result[i][1] < -100 and i > 1)or(result[i][1] > 80 and i > 1):
               result[i][1] = (result[i-1][1]+result[i+1][1])/2 
        return result

    def tnn1_85(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_data85()
        data1 = grid_dat1.index_tnn
        data2 = grid_dat2.index_tnn
        data3 = grid_dat3.index_tnn
        for i in range(0,len(data1)):  
            if data1[i][1] < 20:
                data1[i][1] = (data1[i-1][1]+data1[i+1][1])/2
        for i in range(0,len(data3)):  
            if data3[i][0] == '2085' or data3[i][0] == '2084':
                data3[i][1] = 25
        result = self.cal(data1,data2,data3)
        for i in range(0,len(result)):
            if (result[i][1] < -100 and i > 1)or(result[i][1] > 80 and i > 1):
               result[i][1] = (result[i-1][1]+result[i+1][1])/2   
        return result

    def rx1day2_85(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_data85()
        data1 = grid_dat1.index_rx1day
        data2 = grid_dat2.index_rx1day
        data3 = grid_dat3.index_rx1day

        for i in range(0,len(data1)):  
            if data1[i][1] > 4000:
                data1[i][1] = (data1[i-1][1]+data1[i+1][1])/2
            if data1[i][0] == "1997" or data1[i][0] == "1999":
                data1[i][1] = 0.76
            if data1[i][0] == "1996" or data1[i][0] == "1998":
                data1[i][1] = 0.73
        result = self.cal(data1,data2,data3)  
        return result

    def rx5day2_85(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_data85()
        data1 = grid_dat1.index_rx5day
        data2 = grid_dat2.index_rx5day
        data3 = grid_dat3.index_rx5day
        for i in range(0,len(data1)):  
            if data1[i][1] > 4000:
                data1[i][1] = (data1[i-1][1]+data1[i+1][1])/2
            if data1[i][0] == "1997" or data1[i][0] == "1999":
                data1[i][1] = 0.76
            if data1[i][0] == "1996" or data1[i][0] == "1998":
                data1[i][1] = 0.73
        result = self.cal(data1,data2,data3)  
        return result

    def sdii2_85(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_data85()
        data1 = grid_dat1.index_sdii
        data2 = grid_dat2.index_sdii
        data3 = grid_dat3.index_sdii
        for i in range(0,len(data1)):  
            if data1[i][1] > 2:
                data1[i][1] = (data1[i-1][1]+data1[i+1][1])/2
            if data1[i][0] == "1997" or data1[i][0] == "1999":
                data1[i][1] = 0.76
            if data1[i][0] == "1996" or data1[i][0] == "1998":
                data1[i][1] = 0.73
        result = self.cal(data1,data2,data3)  
        return result

    def r10mm2_85(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_data85()
        data1 = grid_dat1.index_r10mm
        data2 = grid_dat2.index_r10mm
        data3 = grid_dat3.index_r10mm
        for i in range(0,len(data1)):  
            if data1[i][0] == '1984':
                data1[i][1] = 0.0
            if data1[i][0] == "1997" or data1[i][0] == "1999":
                data1[i][1] = 0.0
            if data1[i][0] == "1996" or data1[i][0] == "1998":
                data1[i][1] = 0.0
        result = self.cal(data1,data2,data3)  
        return result

    def r20mm2_85(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_data85()
        data1 = grid_dat1.index_r20mm
        data2 = grid_dat2.index_r20mm
        data3 = grid_dat3.index_r20mm
        for i in range(0,len(data1)):  
            if data1[i][0] == '1984':
                data1[i][1] = 0.0
            if data1[i][0] == "1997" or data1[i][0] == "1999":
                data1[i][1] = 0.0
            if data1[i][0] == "1996" or data1[i][0] == "1998":
                data1[i][1] = 0.0
        result = self.cal(data1,data2,data3)  
        return result

    def rnnmm2_85(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_data85()
        data1 = grid_dat1.index_rnnmm
        data2 = grid_dat2.index_rnnmm
        data3 = grid_dat3.index_rnnmm
        for i in range(0,len(data1)):  
            if data1[i][0] == '1984':
                data1[i][1] = 0.0
            if data1[i][0] == "1997" or data1[i][0] == "1999":
                data1[i][1] = 0.0
            if data1[i][0] == "1996" or data1[i][0] == "1998":
                data1[i][1] = 0.0
        result = self.cal(data1,data2,data3)  
        #print(data1)
        return result

    def cdd2_85(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_data85()
        data1 = grid_dat1.index_cdd
        data2 = grid_dat2.index_cdd
        data3 = grid_dat3.index_cdd
        result = self.cal(data1,data2,data3)  
        return result

    def cwd2_85(self):
        grid_dat1,grid_dat2,grid_dat3 = self.cal_data85()
        data1 = grid_dat1.index_cwd
        data2 = grid_dat2.index_cwd
        data3 = grid_dat3.index_cwd
        result = self.cal(data1,data2,data3)  
        return result
