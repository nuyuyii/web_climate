# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-05-11 04:39
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('climate', '0005_auto_20170511_0431'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='his_rcp85',
            new_name='future_rpc45',
        ),
        migrations.RenameModel(
            old_name='future_rcp85_80',
            new_name='future_rpc85',
        ),
        migrations.DeleteModel(
            name='future_rcp45',
        ),
        migrations.DeleteModel(
            name='future_rcp85',
        ),
        migrations.AlterModelTable(
            name='future_rpc45',
            table='t03future_rpc45',
        ),
    ]
