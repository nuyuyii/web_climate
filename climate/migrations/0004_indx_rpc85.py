# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-05-02 08:45
from __future__ import unicode_literals

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('climate', '0003_auto_20170502_0843'),
    ]

    operations = [
        migrations.CreateModel(
            name='indx_rpc85',
            fields=[
                ('id', models.IntegerField(db_column='year_id', primary_key=True, serialize=False)),
                ('year', models.IntegerField(db_column='year')),
                ('index_su', django.contrib.postgres.fields.jsonb.JSONField()),
                ('index_fd', django.contrib.postgres.fields.jsonb.JSONField()),
                ('index_id', django.contrib.postgres.fields.jsonb.JSONField()),
                ('index_tr', django.contrib.postgres.fields.jsonb.JSONField()),
                ('index_gsl', django.contrib.postgres.fields.jsonb.JSONField()),
                ('index_dtr', django.contrib.postgres.fields.jsonb.JSONField()),
                ('index_txx', django.contrib.postgres.fields.jsonb.JSONField()),
                ('index_tnx', django.contrib.postgres.fields.jsonb.JSONField()),
                ('index_txn', django.contrib.postgres.fields.jsonb.JSONField()),
                ('index_tnn', django.contrib.postgres.fields.jsonb.JSONField()),
                ('index_rx1day', django.contrib.postgres.fields.jsonb.JSONField()),
                ('index_rx5day', django.contrib.postgres.fields.jsonb.JSONField()),
                ('index_sdii', django.contrib.postgres.fields.jsonb.JSONField()),
                ('index_r10mm', django.contrib.postgres.fields.jsonb.JSONField()),
                ('index_r20mm', django.contrib.postgres.fields.jsonb.JSONField()),
                ('index_rnnmm', django.contrib.postgres.fields.jsonb.JSONField()),
                ('index_cdd', django.contrib.postgres.fields.jsonb.JSONField()),
                ('index_cwd', django.contrib.postgres.fields.jsonb.JSONField()),
            ],
            options={
                'db_table': 't12index_rpc85',
            },
        ),
    ]
