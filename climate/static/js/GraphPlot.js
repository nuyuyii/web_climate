

var carsAndModels = {};
    carsAndModels['1'] = ['avg','max','min','-----','txx','tnx','txn','tnn','dtr','su', 'fd', 'id','tr','gsl'];
    carsAndModels['2'] = ['avg','max','min','-----','cdd','cwd','rx1day', 'rx5day', 'sdii', 'r10mm','r20mm'];
//,'r20mm','rnnmm'
function ChangeCarList() {
  var carList = document.getElementById("vars");
        var modelList = document.getElementById("cindex");
        var selCar = carList.options[carList.selectedIndex].value;

        //console.log(selCar);

        while (modelList.options.length) {
            modelList.remove(0);
        }
        var cars = carsAndModels[selCar];
        if (cars) {
            var i;
            for (i = 0; i < cars.length; i++) {
                var car = new Option(cars[i], i);
                modelList.options.add(car);
            }
        }
    } 

/*
function datetime(){
  var currentD = new Date();
  //var time = "Time :"+currentD.getHours()+":"+currentD.getMinutes()+":"+currentD.getSeconds();
  var minut = currentD.getMinutes();
  var second = currentD.getSeconds();
  var milli = currentD.getMilliseconds();
  var time = (minut*60)+second+(milli/1000)
  var startD,stopD;
  startD = datetime();
  stopD = datetime();
  if (gd_size=="0"){
    gd_size="3x3";
  }else if(gd_size=="1"){
    gd_size="2x2";
  }else if(gd_size=="2"){
    gd_size="1x1";
  }
  var formatT = d3.format(".4f");
  //alert("StartTime: "+startD+"  StopTime: "+stopD);
  alert("Grid Size: "+gd_size+" Spend Time: "+formatT((stopD-startD))+" s");
  return time;
}*/


function listY(){
  var listYear = [];
  var list = document.getElementById("list_year");
  for(var i = 1970;i<=2000;i++){
    var st_i = new Option(i);
    list.options.add(st_i);
    listYear.push(i);
  }
  //console.log(listYear);
}
   
///////////////////////////////////////////////////////////////////////////
//////////////////// Plot Graph Average Max Min ///////////////////////////
/////////////////////////////////////////////////////////////////////////// 
        function getCookie(name) {
               var cookieValue = null;
               if (document.cookie && document.cookie != '') {
                 var cookies = document.cookie.split(';');
                 for (var i = 0; i < cookies.length; i++) {
                 var cookie = jQuery.trim(cookies[i]);
                 // Does this cookie string begin with the name we want?
                 if (cookie.substring(0, name.length + 1) == (name + '=')) {
                     cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                     break;
                  }
             }
         }
         return cookieValue;
        }



        var check = 1,check_data1,check_data2,variab,indexs;

        function doalert(checkboxElem) {
             d3.select("#map").selectAll("svg").remove();
             d3.select("#map2").selectAll("svg").remove();
             d3.select("#map3").selectAll("svg").remove();
             document.getElementById("des_map").innerHTML = "";

             /* -- Don't change Brush -- */
             nGraph=1;

                  //console.log(check_data1);
                  //console.log(check_data2);
                    if (checkboxElem.checked) {
                          //alert ("hi");
                          check = 1;
                          select_var(check_data1,check_data2,variab,indexs);
                    } else {
                          //alert ("bye");
                          data2 = [];
                          select_var(check_data1,data2,variab,indexs);
                          check = 2;
                    }
         }
        function data(data,index,vars,data2){
             // document.getElementById("map_p").innerHTML = index+data['path_file'];
             /*d3.select("#map").selectAll("svg").remove();
             d3.select("#map2").selectAll("svg").remove();
             d3.select("#map3").selectAll("svg").remove();

             document.getElementById("des_map").innerHTML = "";*/
             
             check_data1 = data;
             check_data2 = data2;
             variab = vars;
             indexs = index;    
             console.log(check);
             if (check == 1) 
              {
                select_var(check_data1,check_data2,variab,indexs);
              }else if(check == 2){
                data2 = [];
                select_var(check_data1,data2,variab,indexs);
              };
              
             
        }
        
        function select_var(data,data2,x,y){
                       //document.getElementById("demo").innerHTML = z;

                     if(x==1&&y=='txx'){
                        chart_clim(data,data2,"Celsius",' Hottest day (°C)');
                        document.getElementById("des_map").innerHTML = "Monthly maximum value of daily max temperature";
                      }else if(x==1&&y=='tnx'){
                        chart_clim(data,data2,"Celsius",' Warmest night (°C)');
                        document.getElementById("des_map").innerHTML = "Monthly maximum value of daily min temperature";
                      }else if(x==1&&y=='txn'){
                        chart_clim(data,data2,"Celsius",' Clodest day (°C)');
                        document.getElementById("des_map").innerHTML = "Monthly minimum value of daily max temperature";
                      }else if(x==1&&y=='tnn'){
                        chart_clim(data,data2,"Celsius",' Clodest night (°C)');
                        document.getElementById("des_map").innerHTML = "Monthly minimum value of daily min temperature";
                      }else if(x==2&&y=='rx1day'){
                        chart_clim(data,data2," Precip(mm)",' Max 1 day precipitation amount (mm)');
                        document.getElementById("des_map").innerHTML = "Monthly maximum 1-day precipitation";
                      }else if(x==2&&y=='rx5day'){
                        chart_clim(data,data2," Precip(mm)",' Max 5 day precipitation amount (mm)');
                        document.getElementById("des_map").innerHTML = "Monthly maximum consecutive 5-day precipitation";
                      }else if(x==1&&y=='su'){
                        chart_clim(data,data2,"days",' Summer days (days)');
                        document.getElementById("des_map").innerHTML = "Annual count when daily max temperature > 25 °C";
                      }else if(x==1&&y=='fd'){
                        chart_clim(data,data2,"days",' Frost days (days)');
                        document.getElementById("des_map").innerHTML = "Annual count when daily minimum temperature < 0 °C";
                      }else if(x==1&&y=='id'){
                        chart_clim(data,data2,"days",' Ice days (day)');
                        document.getElementById("des_map").innerHTML = "Annual count when daily maximum temperature < 0 °C";
                      }else if(x==1&&y=='tr'){
                        chart_clim(data,data2,"days",' Tropical nights (days)');
                        document.getElementById("des_map").innerHTML = "Annual count when daily max temperature > 20 °C";
                      }else if(x==1&&y=='gsl'){
                        chart_clim(data,data2,"days",' Growing season length (°C)');
                        document.getElementById("des_map").innerHTML = "Annual (1st Jan to 31st Dec in Northern Hemisphere (NH), 1st July to 30th June in Southern Hemisphere (SH)) count between first span of at least 6 days with TG > 5°C and first span after July 1st (Jan 1st in SH) of 6 days with TG < 5°C (where TG is daily mean temperature)";
                      }else if(x==1&&y=='dtr'){
                        chart_clim(data,data2,"Celsius",' Diurnal temperature range (°C)');
                        document.getElementById("des_map").innerHTML = "Monthly mean difference between daily max and min temperature";
                      }else if(x==2&&y=='r10mm'){
                        chart_clim(data,data2,"days",' Number of heavy precipitation days (days)');
                        document.getElementById("des_map").innerHTML = "Annual count when precipitation ≥ 10mm";
                      }else if(x==2&&y=='r20mm'){
                        chart_clim(data,data2,"days",' Number of very heavy precipitation days (days)');
                        document.getElementById("des_map").innerHTML = "Annual count when precipitation ≥ 20mm";
                      }else if(x==2&&y=='rnnmm'){
                        chart_clim(data,data2,"days",' Annual count of days when PRCP ≥ nnmm (days)');
                        document.getElementById("des_map").innerHTML = "Annual count of days when PRCP≥ nnmm, nn is a user defined threshold: Let RRij be the daily precipitation amount on day i in period j. Count the number of days where: RRij ≥ nnmm ";
                      }else if(x==2&&y=='cdd'){
                        chart_clim(data,data2,"days",' Consecutive dry days (days)');
                        document.getElementById("des_map").innerHTML = "Maximum number of consecutive days when precipitation < 1 mm";
                      }else if(x==2&&y=='cwd'){
                        chart_clim(data,data2,"days",' Consecutive wet days (days)');
                        document.getElementById("des_map").innerHTML = "Maximum number of consecutive days when precipitation >= 1 mm";
                      }else if(x==1&&y=='max'){
                        chart_clim(data,data2,"Celsius",' Maximum Temperature');
                        document.getElementById("des_map").innerHTML = "maximum temperature";
                      }else if(x==1&&y=='min'){
                        chart_clim(data,data2,"Celsius",' Minimum Temperature');
                        document.getElementById("des_map").innerHTML = "minimum temperature";
                      }else if(x==1&&y=='avg'){
                        chart_clim(data,data2,"Celsius",' Average Temperature');
                        document.getElementById("des_map").innerHTML = "Average temperature";
                      }else if(x==2&&y=='max'){
                        chart_clim(data,data2,"Precip(mm)",' Maximum precipitation');
                        document.getElementById("des_map").innerHTML = "maximum precipitation";
                      }else if(x==2&&y=='min'){
                        chart_clim(data,data2,"Precip(mm)",' Minimum precipitation');
                        document.getElementById("des_map").innerHTML = "minimum precipitation";
                      }else if(x==2&&y=='avg'){
                        chart_clim(data,data2,"Precip(mm)",' Average precipitation');
                        document.getElementById("des_map").innerHTML = "Average precipitation";
                      }else if(x==2&&y=='sdii'){
                        chart_clim(data,data2,"mm/day",' Simple daily intensity index (mm/day)');
                        document.getElementById("des_map").innerHTML = "The ratio of Annual total precipitation to the number of wet days (>= 1 mm)";
                      };
          }
//var legen_graph = "1";
var name_country = "";
var range_time = "";
var begin = 0,nGraph=0;

        function requestData(name_set) {
          var csrftoken = getCookie('csrftoken');
              //label1Visible()
              var index = $("#cindex").val();
              var variable = $( "#vars" ).val();
              var gd_size = $( "#gridSize" ).val();
              var rcp = $( "#rcp" ).val();
              var ind = carsAndModels[variable][index];
              var new_rcp = "";
              name_country = name_set;
               if(rcp == "45"){
                    new_rcp = "85";
               }else if(rcp == "85"){
                    new_rcp = "45";
               }

               $.ajax({
                     url : window.location.href, // the endpoint,commonly same url
                     type : "POST", // http method
                     data : { csrfmiddlewaretoken : csrftoken, 
                     set_type : name_set ,path_file : ind+''+variable+'_'+rcp, path_file2 : ind+''+variable+'_'+new_rcp,
                   }, // data sent with the post request

                   // handle a successful response
                   success : function(json) {
                      console.log("requestData func. -------");
                      /* -- Don't change Brush -- */
                      nGraph=1;
                                //console.log(json['path_file']); // another sanity check
                                //On success show the data posted to server as
                      data(json['path_file'],ind,variable,json['path_file2']);
                                //legen(variable);
                                
                                //alert('Hi '+json['path_file']);

                   },

                   // handle a non-successful response
                   error : function(xhr,errmsg,err) {
                        console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
                   }
               }); 
        }

        $( "select" ).change(function (e) {
              e.preventDefault();
              console.log("select func. --- ");
              var csrftoken = getCookie('csrftoken');
              //label1Visible()              
              var variable = $( "#vars" ).val();
              var index = $("#cindex").val();
              var gd_size = $( "#gridSize" ).val();
              var rcp = $( "#rcp" ).val();
              var ind = carsAndModels[variable][index];
              name_country = ""
              var new_rcp = " ";
               if(rcp == "45"){
                    new_rcp = "85";
               }else if(rcp == "85"){
                    new_rcp = "45";
               }

               /* select Table */
              var table = " ";
                if(ind == "max" || ind == "min" || ind == "avg"){
                  table = "ms";
               } else{
                  table = "indx";
              }
              //document.getElementById("tool").innerHTML = ind;
              //requestData(function(name_set) {
              $.ajax({
                     url : window.location.href, // the endpoint,commonly same url
                     type : "POST", // http method
                     data : { csrfmiddlewaretoken : csrftoken, 
                     path_file : ind+''+variable+'_'+rcp, path_file2 : ind+''+variable+'_'+new_rcp,
                     path_gd : gd_size,indx:ind+variable,rcp:rcp,s_table:table
                   }, // data sent with the post request

                   // handle a successful response
                   success : function(json) {
                                //console.log(json['path_file']); // another sanity check
                                //On success show the data posted to server as
                                map_data = json["mapValue"];
                                len_min = json["minValue"];
                                len_max = json["maxValue"]; 

                                /* -- Don't change Brush -- */
                                nGraph = 1;

                                /* -- Update Graph and Map -- */
                                data(json['path_file'],ind,variable,json['path_file2']);
                                legen(variable,ind);

                                //console.log(data);
                                //alert('Hi '+json['path_file']);

                   },

                   // handle a non-successful response
                   error : function(xhr,errmsg,err) {
                        console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
                   }
               }); 

             // });      
        });
       /* $( "#compare_rcp" ).change(function (e) {
              e.preventDefault();
              var csrftoken = getCookie('csrftoken');
              //label1Visible()
              var index = $("#cindex").val();
              var variable = $( "#vars" ).val();
              var rcp = $( "#rcp" ).val();
              var ind = carsAndModels[variable][index];
             
              document.getElementById("GraphName").innerHTML = Data_p;
              
              $.ajax({
                     url : window.location.href, // the endpoint,commonly same url
                     type : "POST", // http method
                     data : { csrfmiddlewaretoken : csrftoken, 
                     path_file : ind+''+variable+'_'+rcp
                   }, // data sent with the post request

                   // handle a successful response
                   success : function(json) {
                        console.log(json['path_file']); // another sanity check
                         document.getElementById("des_map").innerHTML = Data_p;
                        //On success show the data posted to server as
                                 data(json['path_file'],ind,variable);
                                 legen(variable);
                                    //alert('Hi '+json['path_file']);
                   },

                   // handle a non-successful response
                   error : function(xhr,errmsg,err) {
                        console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
                   }
               });       
        });*/


function change_brush(start,end,begin,nGraph){
  if (begin==1){
    var gd_size = $( "#gridSize" ).val();
    var csrftoken = getCookie('csrftoken');
    var variable = $( "#vars" ).val();
    var index = $("#cindex").val();
    var rcp = $( "#rcp" ).val();
    var ind = carsAndModels[variable][index];
    /* select Table */
              var table = " ";
                if(ind == "max" || ind == "min" || ind == "avg"){
                  table = "ms";
               } else{
                  table = "indx";
              }
    $.ajax({
      url : window.location.href, // the endpoint,commonly same url
      type : "POST", // http method
      data : { csrfmiddlewaretoken : csrftoken, 
               time_start:start,
               time_end:end,
               rcp:rcp,
               path_gd : gd_size,
               indx:ind+variable,s_table:table
             }, // data sent with the post request
      // handle a successful response
      success : function(json) {
      //On success show the data posted to server as
                  console.log(map_data.features[0].properties.value);
                  map_data = json["mapValue"]
                  //len_min = json["minValue"]
                  //len_max = json["maxValue"]

                  legen(variable,ind);
                  console.log(len_min+"----"+len_max);
                },
      // handle a non-successful response
      error : function(xhr,errmsg,err) {
                  console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
               }
    });
    console.log("change_brush  start:--"+start+"  end:--"+end+'  gd_size '+gd_size) 
  }else{
    console.log("Begin "+begin); 
    this.begin=1;
  }
           
}

function start(){
  //listY();
  $("#vars" ).val("1");
  $("#cindex").val("avg");
  $("#gridSize").val("0");
  
  document.getElementById("compare_rcp").checked = true;
  
  $( "#rcp" ).val("45");
  ChangeCarList();
  //  console.log("check--"+check);
    data(maxts_data,"avg","1",maxts_data85);
  legen("1","avg");
}start();
  
        var customTimeFormat = d3.time.format.multi([
            [".%L", function(d) { return d.getMilliseconds(); }],
            [":%S", function(d) { return d.getSeconds(); }],
            ["%I:%M", function(d) { return d.getMinutes(); }],
            ["%I %p", function(d) { return d.getHours(); }],
            ["%a %d", function(d) { return d.getDay() && d.getDate() != 1; }],
            ["%b %d", function(d) { return d.getDate() != 1; }],
            ["%b", function(d) { return d.getMonth(); }],
            ["%Y", function() { return true; }]
        ]);
         
         var win_width = window.innerWidth;

        function chart_clim(data_plot,data_plot2,name,ind) {
              chart(data_plot,data_plot2,name,ind);  
        }
        function chart(data_plot,data_plot2,name,ind) {
              
             //document.getElementById("GraphName").innerHTML = data_plot;
             d3.select("#map").selectAll("svg").remove();
             //d3.select("#map2").selectAll("svg").remove();
             d3.select("#map3").selectAll("svg").remove();
            //document.getElementById("map_p").innerHTML = window.innerWidth;
            
            //var grap_2 = document.getElementById( "map2" );
            //grap_2.style.backgroundColor = "#FFFFFF";
            //grap_2.style.border = "none";

            var parseD = d3.time.format("%Y").parse;
            var data = data_plot.map(function(d) {
               //document.getElementById("demo").innerHTML = d[0];
               return {
                  date: parseD(d[0]),
                  close: d[1]
               };
              
             });

            var data2 = data_plot2.map(function(d) {
               //document.getElementById("demo").innerHTML = d[0];
               return {
                  date_p: parseD(d[0]),
                  close_p: d[1]
               };
              
             });
          
            var ind_data = [];
            var ind_data2 = [];

            //var mss = data[0];
            data_plot.forEach(function(d,i) {
                ind_data[i] = d[1];
            });
            data_plot2.forEach(function(d,i) {
                ind_data2[i] = d[1];
            });
 
            //document.getElementById("GraphName").innerHTML=Math.max.apply(null,ind_data);
            var str_color = Math.min.apply(null,ind_data)
            var end_color = Math.max.apply(null,ind_data)

            var breakPoint = 930;


            var margin = {},margin2 = {};

            margin.top = 30;
            margin.bottom = 30;
            margin.right =  43;
            margin.left =43;

            margin2 = {top: 10, right: 20, bottom: 15, left: 20};
                 

            //var width_size = (window.innerWidth/3);
                 
            var width =$('#map').width() - margin.left - margin.right;
            var height = $('#map').height()- margin.top - margin.bottom;

            var width2 = $('#map3').width() - margin2.left - margin2.right;
            var height2 = $('#map3').height() - margin2.top - margin2.bottom;
          
            
            var x = d3.time.scale().range([0, width]);
            var y = d3.scale.linear().range([height, 0]);

              

           if (data_plot2.length > 0)
           {
             var y0 = d3.scale.linear().range([height, 0]);
           }else{
             var y0 = d3.scale.linear().range([0, 0]);
           };

            
            
           

            var x2 = d3.time.scale()
            .domain(d3.extent(data, function(d) { return d.date; }))
            .range([0, width2]);

            var y2 = d3.scale.linear().range([height2, 0]);
            var xAxis2 = d3.svg.axis().scale(x2).orient("bottom");
          
            var xAxis = d3.svg.axis().scale(x).orient("bottom").tickFormat(d3.time.format("%Y"));
            xAxis.ticks(d3.time.years, 15) //for 10 years

            var scale_brush = 10; //100 year use 5 it 1 for 10 years
            
            /*if(window.innerWidth <= 1109 && window.innerWidth > 937 ) {
              xAxis.ticks(d3.time.years, 15)
              scale_brush = 15;
            }
            else if( window.innerWidth <= 937){
              xAxis.ticks(d3.time.years, 20)
              scale_brush = 20;
            }else {
              xAxis.ticks(d3.time.years, 10)
              scale_brush = 10;
            }*/


            //var yAxis = d3.svg.axis().scale(y).orient(window.innerWidth < breakPoint ? 'right' : 'left');
            var yAxis = d3.svg.axis().scale(y).orient("left").ticks(6);//auto // for scle use ".ticks(6)"
            //var y0Axis = d3.svg.axis().scale(y0).orient("right");//auto // for scle use ".ticks(6)"

            var brush = d3.svg.brush()
            .x(x2)
            .extent(d3.extent(data, function(d) { return d.date; }))
            .on("brushend", brushended);


            var line = d3.svg.line()
                //.interpolate("basis")
                .x(function(d) { return x(d.date); })
                .y(function(d) { return y(d.close); });

            var line_p = d3.svg.line()
                //.interpolate("basis")
                .x(function(d) { return x(d.date_p); })
                .y(function(d) { return y(d.close_p); });


            var line2 = d3.svg.line()
                //.interpolate("basis")
                .x(function(d) { return x2(d.date); })
                .y(function(d) { return y2(d.close); });


            var svg = d3.select("#map").append("svg")
             .attr("width", width + margin.left + margin.right)
             .attr("height", height + margin.top + margin.bottom);

            var svg2 = d3.select("#map3").append("svg")
             .attr("width", width2 + margin2.left + margin2.right)
             .attr("height", height2 + margin2.top + margin2.bottom);

            svg.append("defs").append("clipPath")
             .attr("id", "clip")
             .append("rect")
             .attr("width", width2)
             .attr("height", height);

            var zoom = d3.behavior.zoom().on("zoom", draw);

           /* var rect = svg.append("svg:rect")
              .attr("class", "pane")
              .attr("id", "clip-rect")
              .attr("width", width)
              .attr("height", height)
              .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
              .call(zoom);*/                         

            var focus = svg.append("g")
             .attr("class", "focus")
             .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
           
            var context = svg2.append("g")
             .attr("class", "context")
             .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");
          
            var clip2 = focus.append("defs").append("svg:clipPath")
              .attr("id", "clip2")
              .append("svg:rect")
              .attr("id", "clip-rect")
              .attr("x", "0")
              .attr("y", "0")
              .attr("width", width)
              .attr("height", height);
                    
             
             x.domain(d3.extent(data, function(d) { return d.date; }));

             var find_max = [Math.max.apply(null,ind_data), Math.max.apply(null,ind_data2)];

             var find_min = [Math.min.apply(null,ind_data), Math.min.apply(null,ind_data2)];

             max_num = Math.max.apply(null,find_max);
             min_num = Math.min.apply(null,find_min);

             y.domain([min_num-10,max_num+10]);
             
               
            if((Math.max.apply(null,ind_data) == Math.min.apply(null,ind_data))||(Math.max.apply(null,ind_data2) == Math.min.apply(null,ind_data2))){
                   if(Math.max.apply(null,ind_data) > Math.max.apply(null,ind_data2)){
                       y.domain([Math.min.apply(null,ind_data)-10,Math.max.apply(null,ind_data)]);

                   }else if(Math.max.apply(null,ind_data) < Math.max.apply(null,ind_data2)){
                       y.domain([Math.min.apply(null,ind_data)-10,Math.max.apply(null,ind_data2)]);

                   }else if(Math.max.apply(null,ind_data) == Math.max.apply(null,ind_data2)){
                       y.domain([Math.min.apply(null,ind_data)-10,Math.max.apply(null,ind_data)]);
                   }
               
               //y0.domain([Math.min.apply(null,ind_data2)-10,Math.max.apply(null,ind_data2)]);
               
             }else{
               //y.domain([0, d3.max(data, function(d) { return Math.max(d.close); })]);
                   if(Math.max.apply(null,ind_data) > Math.max.apply(null,ind_data2)){
                       y.domain([Math.min.apply(null,ind_data),Math.max.apply(null,ind_data)]);

                   }else if(Math.max.apply(null,ind_data) < Math.max.apply(null,ind_data2)){
                       y.domain([Math.min.apply(null,ind_data),Math.max.apply(null,ind_data2)]);

                   }else if(Math.max.apply(null,ind_data) == Math.max.apply(null,ind_data2)){
                       y.domain([Math.min.apply(null,ind_data),Math.max.apply(null,ind_data)]);
                   }
               
              // y0.domain(d3.extent(data2, function(d) { return d.close_p; }));
             }
            
             //y0.domain([0, d3.max(data2, function(d) { return Math.max(d.close_p); })]);

             

             x2.domain(x.domain());
             y2.domain(y.domain());

             var transition = d3.transition();              

             var col_temp = ["#4575b4","#ffffbf","#a50026"]
             var col_day = ["#404040","#b30000","#ffffff","#08519c"]
             var col_percip = ["#ffffe5","#33FFCC","#00441b"]
             var col = []
             var dom = []
             if(name == "days" || name == "mm/day"){
                  col = col_temp;
                  dom = [0,90,180,200]
             }else if(name == "Celsius"){
                  col = col_temp
                  dom = [-10,20,35]
             }else if(name == "Precip(mm)"||name == " Precip(mm)"){
                  col = col_percip
                  dom = [-5, 2,14]
             }

             //dom = [-10,20,35]

             var clo_1 = dom.reduce(function (prev, curr) {
               return (Math.abs(curr - str_color) < Math.abs(prev - str_color) ? curr : prev);
             });
             var clo_2 = dom.reduce(function (prev, curr) {
               return (Math.abs(curr - end_color) < Math.abs(prev - end_color) ? curr : prev);
             });

             ind_str = 0;
             ind_end = 1;

             for (var i = 0; i  < dom.length ; i++) {
                  if (clo_1 == dom[i]) {ind_str = i;};
                  if (clo_2 == dom[i]) {ind_end = i;};
             };
             
             //document.getElementById("GraphName").innerHTML= ind_str + ":" + ind_end;
             /*svg.append("linearGradient")
                .attr("id", "temperature-gradient")
                .attr("gradientUnits", "userSpaceOnUse")
                .attr("x1", 0).attr("y1", y(str_color))
                .attr("x2", 0).attr("y2", y(end_color))
              .selectAll("stop")
                .data([
                  {offset: "0%", color: col[ind_str]},
                  {offset: "100%", color: col[ind_end]}
                ])
              .enter().append("stop")
                .attr("offset", function(d) { return d.offset; })
                .attr("stop-color", function(d) { return d.color; });*/
            /************************line scle color*******************************/


            /*focus.append("path")
              .datum(data)
              .attr({
                'class':'line',
                'd': line(data),
              })
              .each(function(d) { d.totalLength = this.getTotalLength(); })
              .attr("stroke-dasharray", function(d) { return d.totalLength + " " + d.totalLength; })
              .attr("stroke-dashoffset", function(d) { return d.totalLength; })
              .transition()
                  .delay( 3000 )
                  .duration(1500)
              .attr('stroke-dashoffset', 0)
              .attr("clip-path","url(#clip2)");*/

              var color1; //red
              var color2; //blue

            if(Math.max.apply(null,ind_data) > Math.max.apply(null,ind_data2)){
                       color1 = "#800000"; //red
                       color2 = "#333399"; //blue
            }else if(Math.max.apply(null,ind_data) < Math.max.apply(null,ind_data2)){
                            
                       color2 = "#800000"; //red
                       color1 = "#333399"; //blue
            }else if(Math.max.apply(null,ind_data) == Math.max.apply(null,ind_data2)){
                       color2 = "#800000"; //red
                       color1 = "#333399"; //blue
            }
               
            focus.append("path")
              .datum(data)
              .attr("class", "line")
              .attr("d", line(data))
              .style("stroke", color1)
              .attr("clip-path","url(#clip2)");

                              
                             
              if (data_plot2.length > 0)
              {
                  focus.append("path")
                    .datum(data2)
                    .attr("class", "line")
                    .attr("d", line_p(data2))
                    .style("stroke", color2)
                    .attr("clip-path","url(#clip2)");
              };

            focus.append("g")
               .attr("class", "x axis")
               .attr("transform", "translate(0," + height + ")")
               .style("font-weight","bold")
               .call(xAxis);

    
            focus.append("g")
               .attr("class", "y axis")
               .style("font-weight","bold")
               .style("fill", "black")   
               .call(yAxis);

            /*focus.selectAll("dot")
                .data(data)
                .enter().append("circle")
                .attr("r", 2.5)
                .style("fill", "#005580")    
                .style("opacity", .8)      // set the element opacity
                .style("stroke", "#f93")    // set the line colour
                .style("stroke-width", 0.5) 
                .attr("cx", function(d) { return x(d.date); })
                .attr("cy", function(d) { return y(d.close); });*/

            focus.append("text")
               //.attr("transform", "rotate(-90)")
               .attr("x", 1)
               .attr("y",-12 )
               .style("font-family", "sans-serif")
               .style("fill", "black")
               .style("font-size","8px")
               .style("font-weight","bold")
               .style("text-anchor", "middle")
               .text(name);

            focus.append("text")
               .attr("x", width/2)
               .attr("y", height+26)
               .style("font-family", "sans-serif")
               .style("fill", "black")
               .style("font-size","9px")
               .style("font-weight","bold")
               .style("text-anchor", "middle")
               .text("Years");

            

            /*focus.append("text")
               .attr("x", (width/2)+8)
               .attr("y", -12 )
               .style("fill", "#000033")
               .style("font-family", "sans-serif")
               .style("text-anchor", "middle")
               .attr("font-size", "11px")
               .text(ind);*/

            context.append("g")
             .attr("class", "x grid")
             .attr("transform", "translate(0," + height2 + ")")
             .call(d3.svg.axis()
                .scale(x2)
                .orient("bottom")
                .ticks(d3.time.years, 5)
                .tickSize(-height2)
                .tickFormat(""))
             .selectAll(".tick")
             .classed("minor", function(d) { return d.getHours(); });

            context.append("path")
              //.datum(data)
              .attr("class", "brush");
              //.attr("d", line2(data));
            
           /* context.append("text")
               .attr("x", (width2/2)+8)
               .attr("y", -1 )
               .attr("class", "caption")
               .style("font-size","9px")
               //.style("font-weight","bold")
               .text("Years")*/

            context.append("g")
             .attr("class", "x axis")
             .attr("transform", "translate(0," + height2 + ")")
             .call(d3.svg.axis()
               .scale(x2)
               .orient("bottom")
               .ticks(d3.time.years,scale_brush)
               .tickPadding(0))
             .selectAll("text")
             .attr("x", 6)
             .style("font-size","8px")
             .style("font-family", "sans-serif")
             //.style("font-weight","bold")
             .style("text-anchor", "middle")
             .call(xAxis2);

           context.append("g")
            .attr("class", "brush")
            .call(brush)
            .call(brush.event)
            .selectAll("rect")
            .style("fill", "black")
            .attr("height", height2);
            
/*var transition = d3.transition();
var path = svg.append('path')
  .datum(data)
    .attr({
      'd': line(data),
      'stroke-dasharray': '385 385',
      'stroke-dashoffset': 385
    })
    .transition()
        .delay( 2000 )
        .duration(1500)
    .attr('stroke-dashoffset', 0)*/


    function brushended() {
           
            var extent0 = brush.extent(),
                extent1 = extent0.map(d3.time.year.round);
                //cal_year(extent1,data_plot,ind,name);


            var start = extent1[0].getFullYear();
            var end = extent1[1].getFullYear(); 
            range_time = start+" - "+end;

            var checked_rcp = document.getElementById("compare_rcp").checked;// = false;
            console.log("compare_rcp --- "+checked_rcp);

          
            console.log("brushended --- "+nGraph);
            //if (checked_rcp == true && ){}
            /* Check On Brush*/
            if (nGraph==0){
              change_brush(start,end,begin);
              //console.log("begin :"+begin)                
              console.log(range_time);
              nGraph=1;
            }else{
              nGraph=0;
            }       
            document.getElementById("MapName").innerHTML="Map: "+range_time;  

            if(name_country=="Thailand" ||name_country=="Cambodia" || 
               name_country=="Lao People's Democratic Republic" || name_country=="Viet Nam"){
              document.getElementById("GraphName").innerHTML=ind+": "+name_country+" "+range_time;
            }else{
              document.getElementById("GraphName").innerHTML=ind+": "+range_time;
            }         
            
            

            var range = end-start;

              if(range <= 10 ){
                    xAxis.ticks(d3.time.years, 1);
              }if(range <= 20 || range <= 30 ){
                    xAxis.ticks(d3.time.years, 3);
              }if(range <= 40){
                    xAxis.ticks(d3.time.years, 4);
              }if(range <= 50){
                    xAxis.ticks(d3.time.years, 5);
              }else{
                    xAxis.ticks(d3.time.years, 15);
              }
                        
         
            if (!d3.event.sourceEvent){
                //cal_year(extent1,data_plot,ind,name);
              /*focus.append("text")
               .attr("x", (width/2) )
               .attr("y", -10 )
               .style("fill", "#000033")
               .style("font-family", "sans-serif")
               .style("text-anchor", "middle")
               .attr("font-size", "13px")
               .text(ind+" in"+extent1[0].getFullYear()+"-"+extent1[1].getFullYear());*/
             
          return;
            } // only transition after input

            // if empty when rounded, use floor & ceil instead
            if (extent1[0] >= extent1[1]) {
                extent1[0] = d3.time.year.floor(extent0[0]);
                extent1[1] = d3.time.year.ceil(extent0[1]);
             }
            x.domain(brush.empty() ? x2.domain() : brush.extent());
            
            d3.select(this).transition()
              .call(brush.extent(extent1))
              .call(brush.event)
              .call(endAll, function () {
               
              });
             
            zoom.x(x);
          }

          function endAll (transition, callback) {
            var n;

            if (transition.empty()) {
                callback();
            }
            else {
                n = transition.size();
                transition.each("end", function () {
                    n--;
                    focus.select(".x.axis").call(xAxis);
                    if (n === 0) {
                        focus.selectAll(".line").attr("d", line(data));
                        if (data_plot2.length > 0){
                              focus.select(".line").attr("d", line_p(data2));
                        }
                        


                        //focus.select(".dot");
                        callback();
                    }
                });
            }
          }
          function draw() {
          }
        }