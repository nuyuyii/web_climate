/* ******************************************************** */  
/* ------------------- Concapt ---------------------------- */
// 3 Map Global 3 Legend for each Map And 3 slider Time Bar
// each Map comtain by two layers -geoWorld -geoTemp_and_prec_plot


/* ******************************************************** */  
/* ****************** Legend Color Map ******************** */
/* ******************************************************** */
// var nLegend = ["temperature (°C)", "preciptation (mm/day)"]
var nLegend = {"1": {"color":["#4575b4", "#ffffbf", "#a50026"],
"name":"Temperature (°C)","domain":[-10, 20, 35],"scale":[-10, 40]},
"2":{"color":["#fff","#9ecae1","#3182bd"],"name":"preciptation (mm/day)",
"domain":[-5, 2,5],"scale":[0, 5]}};
//"domain":[-10, 20, 35],"scale":[-10, 40]},
//["#543005","#FFFFFF","#2EFEF7","#003c30"]
//["#9F5000","#ffffe5","#0C6944"]
//"#ffffe5","#a6bddb","#2b8cbe"

// prec "color":["#ffffe5","#33FFCC","#00441b"]
//"prec":{"color":["#ffffe5","#33FFCC","#00441b"],"name":"preciptation (mm/day)",
//"domain":[-3, 5,18],"scale":[0, 20]}};




function map_plot(n,ind){ 
    d3.select('#legend_H').selectAll("svg").remove();
    d3.select('#legend_rcp85').selectAll("svg").remove();
    d3.select('#legend_rcp45').selectAll("svg").remove();
    d3.select('#map_past').selectAll("svg").remove();
    d3.select('#map_rcp85').selectAll("svg").remove();
    d3.select('#map_rcp45').selectAll("svg").remove();

    var indL = n;
    var i_ticks = 11;  // max_value is scale/2
  //var legend = d3.select('#legend_H'),
   var   legend_rcp85 = d3.select('#legend_rcp85'),
      legend_rcp45 = d3.select('#legend_rcp45');

  var widthL = parseInt(legend_rcp85.style('width')),
      heightL = parseInt(legend_rcp85.style('height'));
  var maxVal,minVal;
  var format_scal = "+.0f"
var format_scal = "+.0f"
  if (ind == "max" && n == "1"){
    minVal = 20;
    maxVal = 50;
}else if (ind == "min" && n=="1"){
    minVal = -10;
    maxVal = 30;
}else if (ind == "avg" && n=="1"){
    minVal = 15;
    maxVal = 35;
}else if (ind == "max" && n=="2"){
    minVal = 100; // 15.87-1069.53  :: 5.51 - 290.32
    maxVal = 1000;
}else if (ind == "min" && n=="2"){
    minVal = 0; // 0 - 0.18  :: 0 - 0.5
    maxVal = 0.5;
    format_scal = "+.2f"
}else if (ind == "avg" && n=="2"){
    minVal = 0; //0.16 - 97.48  :: 0.08-27.44
    maxVal = 40;
    i_ticks = 8;
}
  //console.log("new "+ind+" minVal "+minVal+" -- maxVal "+ maxVal);

  nLegend[indL]['scale'] = [minVal,maxVal];
  nLegend[indL]['domain'] = [minVal,(minVal+maxVal)/2,maxVal]

    var color = d3.scale.linear()
    .domain(nLegend[indL]['domain'])
    .range(nLegend[indL]['color'])
    .interpolate(d3.interpolateHcl);

  /* ---------------------------------------------------------------------- */
  //////////////////////////////// Legen d3 plot /////////////////////////////
  /* ---------------------------------------------------------------------- */
  draw_legen('#legend_H');
  draw_legen('#legend_rcp85');
  draw_legen('#legend_rcp45');

  function draw_legen(idLegen){
    var legend = d3.select(idLegen)

    var x = d3.scale.linear()
    .domain(nLegend[indL]['scale'])
    .range([0, widthL-20]);

    var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom")
    .tickSize(10)
    .ticks(i_ticks)
    .tickFormat(d3.format(format_scal));

    var svgL = legend.append("svg")
    .attr("width", widthL)
    .attr("height", heightL)
    .attr("class", "legend_compare")
    .append("g")
    .attr("transform", "translate(10,20)");

    svgL.selectAll("rect")
    .data(pair(x.ticks(20)))
    .enter().append("rect")
    .attr("height", 10)
    .attr("x", function(d) { return x(d[0]); })
    .attr("width", function(d) { return x(d[1]) - x(d[0]); })
    .style("fill", function(d) { return color(d[0]); });

    svgL.call(xAxis).append("text")
    .attr("class", "caption_compare")
    .attr("y", -10)
    .attr("x", widthL/2)
    .text(nLegend[indL]['name']);

  };  // end draw_legen
    
  function pair(array) {
       return array.slice(1).map(function(b, i) {
        //console.log(array[i], b);
           return [array[i], b];
       });
  };  // end pair func.


  /* ---------------------------------------------------------------------- */
  ////////////////////////// Map Projection d3 plot //////////////////////////
  /* ---------------------------------------------------------------------- */
  draw_map('#map_past', map_hist.features);
  draw_map('#map_rcp85', map_rcp85.features);
  draw_map('#map_rcp45', map_rcp45.features);
  function draw_map(idMap, features){
    var projection, pathMap, svgMap, gMap, gMap2;
    var mapP = d3.select(idMap);
    var dataTs,dataLand,dataPr;
    //var tooltip = map.append("div").attr("class", "tooltip hidden");
    var width = parseInt(mapP.style('width')),
        height = parseInt(mapP.style('height'));
    var graticule = d3.geo.graticule();

    // --------------------------------------
    // --------------- Zoom and Move map :: Hist
    var zoomMap = d3.behavior.zoom().translate([0,0])//.center([117.9, 6.7])
                  .scale(1).scaleExtent([1, 20]).on("zoom", moveMap);

    function moveMap(){
      var t = d3.event.translate;
      var s = d3.event.scale;
      var w_max = 0;
      var w_min = width * (1 - s);
      var h_max = height < s*width/2 ? s*(width/2-height/2) : (1-s)*height/2;
      var h_min = height < s*width/2 ? -s*(width/2-height/2)-(s-1)*height : (1-s)*height/2;

      t[0] = Math.min(w_max, Math.max(w_min, t[0]));
      t[1] = Math.min(h_max, Math.max(h_min, t[1]));

      zoomMap.translate(t);
      gMap.attr("transform", "translate(" + t + ")scale(" + s + ")");
      gMap.selectAll("path").attr("class","MapColor");
      //console.log(t,s);
      gMap2.attr("transform", "translate(" + t + ")scale(" + s + ")");
      gMap2.selectAll("path").style("stroke-width", .9 / s + "px");
    }

    // --------------------------------------
    // --------------- setup map :: Hist
    setupMap(width,height); 

    function setupMap(width,height){               
      projection = d3.geo.equirectangular()
                  .center([117.9, 5.6]).scale(width) //width/2/Math.PI
                  .translate([width/2, height/2]);

      pathMap = d3.geo.path().projection(projection);
      svgMap = mapP
            .append("svg")
            .attr("width", '100%')
            .attr("height", '100%')
            .attr("class","svgMap")
            .attr("id", "mapHist")
            .call(zoomMap);

      gMap = svgMap.append("g"),
      gMap2 = svgMap.append("g");
      drawMapL0(features);
      //console.log(map_rcp85);
    }

    // --------------------------------------
    // --------------- port layer0 color map :: Hist
    // data temp coordinate
    // d.id 0-252 keep  d.coordinate[0][1] lon -180 to 180 
    // d.id %252 keep d.coordinate[0][0] lat -90 to -90
    // console.log(json[1]);
    function drawMapL0(json){
      gMap.selectAll("path")
          .data(json)
          .enter().append("path")
          .attr("d", pathMap)
          .attr("class","MapColor")
          .style("fill", function(json){ return color(json.properties.value);})
          .style("stroke",function(json){ return color(json.properties.value);});
                    //.style("stroke-width",1)data9T
                    //.style("stroke-opacity",1);
                    //.style("opacity",1);
    }

    // --------------------------------------
    // --------------- Read Data topo json map
    d3.json('../static/data/world-topo-min.json', function(json) {
    dataLand = topojson.feature(json, json.objects.countries).features
        //console.log("Func. Draw map"); 
        drawMapL1(dataLand);   
    });

    // --------------------------------------
    // --------------- Function Plot Grid and Base map :: Hist
    function drawMapL1(json){
        gMap2.append("path")
              .datum(graticule)
              .attr("class","graticule")
              .attr("d", pathMap);

        gMap2.selectAll("path")
            .data(json)
            .enter().append("path")
            .attr("d", pathMap)
           .style("stroke-width",0.9);
    }
    // --------------------------------------
    // --------------- Resize window
    d3.select(window).on('resize', function() {
      width = parseInt(mapP.style('width'));
      height = parseInt(mapP.style('height'));

      projection.scale(width) //width/2/Math.PI
              .translate([width/2, height/2]);
      pathMap = d3.geo.path().projection(projection);
      gMap.selectAll("path")
          .attr("d", pathMap);

      gMap2.selectAll("path")
          .attr("d", pathMap);
      });

    // --------------------------------------
    // --------------- Redraw Map
    function resetMap(newdata){
      width = parseInt(mapP.style('width'));
      height = parseInt(mapP.style('height'));
      console.log("Reset Ok");

      d3.select('.svgMap').remove();
      setupMap(width,height)
      drawMapL0(dataLand);
      drawMapL1(newdata.features);
    };  //end reset Map func.
  };  // end draw map func.
};  // end map_plot func. 
//---------------------------------------------------------
