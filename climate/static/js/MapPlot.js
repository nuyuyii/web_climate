
// -$("#scatter").load("{% url 'home' %}");-->



/* ******************************************************** */  
/* ***************** Onload Body HTML ********************* */
/* ******************************************************** */
//var carsAndModels = {};
/*function makeBase(){
    ChangeCarList();
    //document.getElementById("tool").innerHTML = ind;
    ajaxData();   
}*/

//carsAndModels['1'] = ['su', 'fd', 'id','tr','gsl','wsdi','csdi','dtr','txx','tnx','txn','tnn','tn10p','tx10p','tn90p','tx90p'];
//carsAndModels['2'] = ['rx1day', 'rx5day', 'sdii', 'r10mm','r20mm','rnnmm','cdd','cwd','r95ptot','r99ptot','prcptot'];
/* ******************************************************** */  
/* ****************** Legend Color Map ******************** */
/* ******************************************************** */
// var nLegend = ["temperature (°C)", "preciptation (mm/day)"]
var nLegend = {"1": {"color":["#4575b4", "#ffffbf", "#a50026"], //"#404040","#ffffff","#08519c"
"name":"Temperature (°C)","domain":[-10,20, 35],"scale":[-10, 40]},
"2":{"color":["#fff","#9ecae1","#3182bd"],"name":"preciptation (mm/day)",
"domain":[-5, 2,14],"scale":[0, 15]}};
//["#543005","#FFFFFF","#2EFEF7","#003c30"]
//["#9F5000","#ffffe5","#0C6944"]
//"#543005","#7fcdbb","#2c7fb8"
//"prec":{"color":["#ffffe5","#33FFCC","#00441b"],"name":"preciptation (mm/day)",
//"domain":[-3, 5,18],"scale":[0, 20]}};


function legen(n,ind){
d3.select("#legend_p").selectAll("svg").remove();
d3.select("#map_p").selectAll("svg").remove();
d3.select("#map_p").selectAll("div").remove();

var indL = n;
var maxVal = len_max;
var minVal = len_min;
var i_ticks = 11;  // max_value is scale/2
console.log(ind+" minVal "+minVal+" -- maxVal "+ maxVal);
var i_ticks_rc = 40;
var format_scal = "+.0f"

if (ind == "max" && n == "1"){
    minVal = 20;
    maxVal = 50;
}else if (ind == "min" && n=="1"){
    minVal = -10;
    maxVal = 30;
}else if (ind == "avg" && n=="1"){
    minVal = 15;
    maxVal = 35;
}else if (ind == "su" && n=="1"){
    minVal = 16;
    maxVal = 366;
    i_ticks_rc = 44;
    nLegend[indL]['name'] = "Summer days (days)";
}else if (ind == "fd" && n=="1"){
    minVal = 0;
    maxVal = 330;
    i_ticks_rc = 44;
    nLegend[indL]['name'] = "Frost days (days)";
}else if (ind == "id" && n=="1"){
    format_scal = "+.1f"
    i_ticks = 7; 
    nLegend[indL]['name'] = "Ice days (day)";
}else if (ind == "id" && n=="1"){
    minVal = 0;
    maxVal = 360;
    format_scal = "+.1f"
    nLegend[indL]['name'] = "Tropical nights (days)";
}else if (ind == "gsl" && n=="1"){
    minVal = 350;
    //maxVal = 360;
    //format_scal = ".0f"
    nLegend[indL]['name'] = "Growing season length (°C)";
}else if (ind == "dtr" && n=="1"){
    maxVal = 10;
    nLegend[indL]['name'] = "Diurnal temperature range (°C)";
}else if (ind == "txx" && n=="1"){
    minVal =25;//26-49
    maxVal = 50;
    i_ticks = 11;
    nLegend[indL]['name'] = "Hottest day (°C)";
}else if (ind == "tnx" && n=="1"){
    minVal = 15; //15-35 
    maxVal = 40;
    i_ticks = 11;
    nLegend[indL]['name'] = "Warmest night (°C)";
}else if (ind == "txn" && n=="1"){
    minVal = -5;
    maxVal = 35;
    nLegend[indL]['name'] = "Clodest day (°C)";
}else if (ind == "tnn" && n=="1"){
    minVal = -5;
    maxVal = 35;
    nLegend[indL]['name'] = "Clodest night (°C)";
}else if (ind == "max" && n=="2"){
    minVal = 100; // 15.87-1069.53  :: 5.51 - 290.32
    maxVal = 1000;
}else if (ind == "min" && n=="2"){
    minVal = 0; // 0 - 0.18  :: 0 - 0.5
    maxVal = 0.5;
    format_scal = "+.2f"
}else if (ind == "avg" && n=="2"){
    minVal = 0; //0.16 - 97.48  :: 0.08-27.44
    maxVal = 40;
    i_ticks = 8;
}else if (ind == "rx1day" && n=="2"){
    nLegend[indL]['name'] = "Max 1 day precipitation amount (mm)";
}else if (ind == "rx5day" && n=="2"){
    nLegend[indL]['name'] = "Max 5 day precipitation amount (mm)";
}else if (ind == "sdii" && n=="2"){
    nLegend[indL]['name'] = "Simple daily intensity index (mm/day)";
}else if (ind == "r10mm" && n=="2"){
    nLegend[indL]['name'] = "Number of heavy precipitation days (days)";
}else if (ind == "r20mm" && n=="2"){
    nLegend[indL]['name'] = "Number of very heavy precipitation days (days)";
}else if (ind == "cdd" && n=="2"){
    nLegend[indL]['name'] = "Consecutive dry days (days)";
}else if (ind == "cwd" && n=="2"){
    nLegend[indL]['name'] = "Consecutive wet days (days)";
}
//console.log("new "+ind+" minVal "+minVal+" -- maxVal "+ maxVal);

nLegend[indL]['scale'] = [minVal,maxVal];
nLegend[indL]['domain'] = [minVal,(minVal+maxVal)/2,maxVal]


var legend = d3.select('#legend_p');

var widthL = parseInt(legend.style('width')),
    heightL = parseInt(legend.style('height'));

var color = d3.scale.linear()
    .domain(nLegend[indL]['domain'])
    .range(nLegend[indL]['color'])
    .interpolate(d3.interpolateHcl);

var x = d3.scale.linear()
    .domain(nLegend[indL]['scale'])
    .range([0, widthL-20]);

var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom")
    .tickSize(9)
    .ticks(i_ticks)
    .tickFormat(d3.format(format_scal));

//console.log("legend w/h",widthL,heightL);
//console.log("x(d[1]) "+ x(d[1])+" x(d[0]) "+x(d[0]) );
var svgL = legend.append("svg")
    .attr("width", widthL)
    .attr("height", heightL)
    .attr("class", "legendP")
    .append("g")
    .attr("transform", "translate(10,20)");

svgL.selectAll("rect")
    .data(pair(x.ticks(i_ticks_rc)))
    .enter().append("rect")
    .attr("height", 10)
    .attr("x", function(d) { return x(d[0]); })
    .attr("width", function(d) { return x(d[1]) - x(d[0]); })
    .style("fill", function(d) { return color(d[0]); });

svgL.call(xAxis).append("text")
    .attr("class", "caption")
    .attr("y", -9)
    .attr("x", widthL/2)
    .text(nLegend[indL]['name']);


function pair(array) {
  return array.slice(1).map(function(b, i) {
    //console.log(i+" : "+array[i], b);
    return [array[i], b];
  });
}

/* ******************************************************** */  
/* **************** Map Projection d3 plot **************** */
/* ******************************************************** */
var mapP = d3.select('#map_p');
//var tooltip = map.append("div").attr("class", "tooltip hidden");

var width = parseInt(mapP.style('width')),
height = parseInt(mapP.style('height'));

var graticule = d3.geo.graticule();
var zoomMap = d3.behavior.zoom().translate([0,0])//.center([117.9, 6.7])
                .scale(1).scaleExtent([1, 20]).on("zoom", moveMap);

var tooltip = d3.select("#map_p").append("div").attr("class", "tooltip hidden");

function moveMap(){
    var t = d3.event.translate;
    var s = d3.event.scale;

    var w_max = 0;
    var w_min = width * (1 - s);
    var h_max = height < s*width/2 ? s*(width/2-height/2) : (1-s)*height/2;
    var h_min = height < s*width/2 ? -s*(width/2-height/2)-(s-1)*height : (1-s)*height/2;

    t[0] = Math.min(w_max, Math.max(w_min, t[0]));
    t[1] = Math.min(h_max, Math.max(h_min, t[1]));

    //console.log(t,s)

    zoomMap.translate(t);
    gMap.attr("transform", "translate(" + t + ")scale(" + s + ")");
    gMap.selectAll("path").attr("class","MapColor");
    //console.log(t,s);
    gMap2.attr("transform", "translate(" + t + ")scale(" + s + ")");
    //gMap2.selectAll("path").style("stroke-width", .9 / s + "px");
}

// --------------------------------------
// --------------- setup map
var projection, pathMap, svgMap, gMap, gMap2;
setupMap(width,height);

//console.log(width,height)

function setupMap(width,height){
               
    projection = d3.geo.equirectangular()
                .center([117.9, 5.7]).scale(width) //width/2/Math.PI
                //.center([117.9, 5.7]).scale(width)
                .translate([width/2, height/2]);

    pathMap = d3.geo.path().projection(projection);
    svgMap = mapP
            .append("svg")
            .attr("width", '100%')
            .attr("height", '100%')
            .attr("class","svgMap")
            .call(zoomMap);

    gMap = svgMap.append("g");
    gMap2 = svgMap.append("g");    
}

// --------------------------------------
// --------------- port layer0 color map
var dataTs,dataLand,dataPr;

/*var data9T;
d3.json('../static/data/geoAvgTsPrG9.json', function(json) {
    console.log("FF",json.length);   
    data9T = json.features;
});*/

function drawMapL0(json){
    //data temp coordinate// d.id 0-252 keep  d.coordinate[0][1] lon -180 to 180 
// d.id %252 keep d.coordinate[0][0] lat -90 to -90
    //console.log(json[1]);
    gMap.selectAll("path")
        .data(json)
        .enter().append("path")
        .attr("d", pathMap)
        .attr("class","MapColor")
        .style("fill", function(d){ return color(d.properties.value);})
        .style("stroke",function(d){ return color(d.properties.value);});
                    //.style("stroke-width",1)data9T
                    //.style("stroke-opacity",1);
                    //.style("opacity",1);
}

/*d3.json('../static/data/geoAvgTsPrG9t.json', function(json) {
    dataTs = json.features;
    var dou=[];
    //var tt = dataTs.properties.ts[1]
    console.log(data9T[0].properties.allts)
    dataTs.forEach(function(d, i){
    //dou.push(data9T[i].properties.allts)
        d.properties.allts = +data9T[i].properties.allts;
    })
                //console.log(dou)
    drawMapL0(dataTs);
});*/

            
           /*
            d3.json('../static/data/geoTsPrAvg.json', function(json) {
                console.log(json);
            });*/
// --------------------------------------
// --------------- Base map
d3.json('../static/data/world-topo-min.json', function(json) {
    dataLand = topojson.feature(json, json.objects.countries).features
    drawMapL1(dataLand);
    drawMapL0(map_data.features);
});

function drawMapL1(json){  

    gMap2.append("path")
          .datum(graticule)
          .attr("class","graticule")
          .attr("d", pathMap);

    var country = gMap2.selectAll(".country").data(json);
    country.enter().insert("path")
      .attr("class", "country")
      .attr("d", pathMap)
      .attr("id", function(d,i) { return d.id; })
      .attr("title", function(d,i) { return d.properties.name; });//function(d, i) { return d.properties.color; });

    var offsetL = document.getElementById('map_p').offsetLeft+20;
    var offsetT = document.getElementById('map_p').offsetTop+10;

    country.on("mousemove", function(d,i){
        var mouse = d3.mouse(svgMap.node()).map(function(d){ return parseInt(d); });
        tooltip.classed("hidden", false)
            .attr("style","left:"+(mouse[0]+offsetL)+"px;top:"+(mouse[1]+offsetT)+"px")
            .html(d.properties.name);
        }).on("mouseout", function(d,i){
            tooltip.classed("hidden",true);
        }).on("click", function(d){
            console.log(d.properties.name);
            requestData(d.properties.name);
        });
    /*gMap2.append("path")
   .datum({type: "LineString", coordinates: [[-180, 0], [-90, 0], [0, 0], [90, 0], [180, 0]]})
   .attr("class", "equator")
   .attr("d", pathMap);
*/

    /*gMap2.selectAll("path")
         .data(json)
         .enter().append("path")
         .attr("d", pathMap)
         .style("stroke-width",0.9);
         /**.attr("class","country");
         .style("fill","white");
         .on("mousemove", function(d,i){
                        var mouse = d3.mouse(svgMap.node()).map( function(d) { return parseInt(d); } );
                        var left = Math.min(width-12*d.properties.name.length, (mouse[0]+20));
                        var top = Math.min(height-40, (mouse[1]+20));

                        tooltip.classed("hidden", false)
                            .attr("style", "left:"+left+"px;top:"+top+"px")
                            .html(d.properties.name);
                    })
                    .on("mouseout",  function(d,i) {
                        tooltip.classed("hidden", true);
         });*/
}

// --------------------------------------
// --------------- Resize window
d3.select(window).on('resize', function() {
    width = parseInt(mapP.style('width'));
    height = parseInt(mapP.style('height'));

    projection.scale(width) //width/2/Math.PI
              .translate([width/2, height/2]);
    pathMap = d3.geo.path().projection(projection);

    gMap.selectAll("path")
        .attr("d", pathMap);

    gMap2.selectAll("path")
         .attr("d", pathMap);

    
});

// --------------------------------------
// --------------- Redraw Map
function resetMap(){
    width = parseInt(mapP.style('width'));
    height = parseInt(mapP.style('height'));
    console.log("Reset Ok");
    gMap.transition()
        .duration(500)
        .attr("transform","translater("+width/2+","+height/2+")scale(" +width+ ")");
        //.style("stroke-width", .5 + "px");
    gMap2.transition()
         .duration(500)
         .attr("transform","translater("+width/2+","+height/2+")scale(" +width+ ")")
         .style("stroke-width", .9 + "px");

               /* d3.select('.svgMap').remove();
                setupMap(width,height)
                drawMapL0(dataLand);
                drawMapL1(dataTs);
                setupMap(width,height);*/
};

d3.selectAll("button[resetP]").on("click", resetMap);
/*
// --------------------------------------
// --------------- Zoom button
function zoomed() {
    svgMap.attr("transform",
        "translate(" + zoomMap.translate() + ")" +
        "scale(" + zoomMap.scale() + ")"
    );
}

function interpolateZoom (t, s) {
    var self = this;
    d3.transition().duration(350).tween("zoom", function () {      

        var iTranslate = d3.interpolate(zoomMap.translate(), t),
            iScale = d3.interpolate(zoomMap.scale(), s);
            console.log("call interpolateZoom");
        return function (tr) {
            zoomMap
                .scale(iScale(tr))
                .translate(iTranslate(tr));
            zoomed();
        };
    });
}

function zoomClick() {
    var clicked = d3.event.target,
        direction = 1,
        factor = 0.2,
        target_zoom = 1,
        center = [width / 2, height / 2],
        extent = zoomMap.scaleExtent(),
        translate = zoomMap.translate(),
        translate0 = [],
        l = [],
        view = {x: translate[0], y: translate[1], k: zoomMap.scale()};
    console.log(translate,extent);
    d3.event.preventDefault();
    direction = (this.id === 'zoom_in') ? 1 : -1;
    target_zoom = zoomMap.scale() * (1 + factor * direction);

    if (target_zoom < extent[0] || target_zoom > extent[1]) { return false; }

    translate0 = [(center[0] - view.x) / view.k, (center[1] - view.y) / view.k];
    view.k = target_zoom;
    l = [translate0[0] * view.k + view.x, translate0[1] * view.k + view.y];

    view.x += center[0] - l[0];
    view.y += center[1] - l[1];

    console.log(l,translate0,view);
    //interpolateZoom([view.x, view.y], view.k);
}*/

//d3.selectAll('button[zoomP]').on('click', zoomClick);

}