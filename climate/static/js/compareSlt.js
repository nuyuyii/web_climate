///////////////////////////////////////////////////////////////////////////
//////////////////// Select Temp Or Prec Index ////////////////////////////
/////////////////////////////////////////////////////////////////////////// 
var carsAndModels = {};
    carsAndModels['1'] = ['avg','max','min'];
    carsAndModels['2'] = ['avg','max','min'];

function ChangeCarList() {
        var carList = document.getElementById("varFuture");
        var modelList = document.getElementById("cindexFuture");
        var selCar = carList.options[carList.selectedIndex].value;

        //console.log(selCar);

        while (modelList.options.length) {
            modelList.remove(0);
        }
        var cars = carsAndModels[selCar];
        if (cars) {
            var i;
            for (i = 0; i < cars.length; i++) {
                var car = new Option(cars[i], i);
                modelList.options.add(car);
            }
        }
    } 
function getCookie(name) {
               var cookieValue = null;
               if (document.cookie && document.cookie != '') {
                 var cookies = document.cookie.split(';');
                 for (var i = 0; i < cookies.length; i++) {
                 var cookie = jQuery.trim(cookies[i]);
                 // Does this cookie string begin with the name we want?
                 if (cookie.substring(0, name.length + 1) == (name + '=')) {
                     cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                     break;
                  }
             }
         }
         return cookieValue;
}
/*--------------------------------------------------------------------------------*/
/* ------------------------------Plot Graph---------------------------------------*/

   
//document.getElementById("SlH").innerHTML = maxts_data;
      var customTimeFormat = d3.time.format.multi([
            [".%L", function(d) { return d.getMilliseconds(); }],
            [":%S", function(d) { return d.getSeconds(); }],
            ["%I:%M", function(d) { return d.getMinutes(); }],
            ["%I %p", function(d) { return d.getHours(); }],
            ["%a %d", function(d) { return d.getDay() && d.getDate() != 1; }],
            ["%b %d", function(d) { return d.getDate() != 1; }],
            ["%b", function(d) { return d.getMonth(); }],
            ["%Y", function() { return true; }]
        ]);
        

        function cal_sumMounth (data,variable,ind) {

             var parseD = d3.time.format("%Y-%m-%d").parse;
             start = parseD(data[1][0]).getFullYear();
             end = parseD(data[data.length-1][0]).getFullYear();
             var year = [];
             var val = [];
              var year2 = [];
             //var date = parseD("1970-01");
             var date = parseD(data[1][0]);
            
             for (var i = 1; i < data.length; i++) {
                year[i] =  parseD(data[i][0]).getFullYear();
                val[i] = data[i][1];
                year2[i] = data[i][0];
             };
             e_year = end - start;
             val_cut = [];
             dat_cut = [];
             for(var i = 0;i<val.length;i++){
                  if(year[i]>=start&&year[i]<=end){
                     val_cut.push(val[i-1]);   
                     dat_cut.push(year2[i-1]);
                  }
              }
               //------------------------------------------------------
              val_sum = [];
              for(var j=0;j<val_cut.length;j++){
                   val_cut[j] = val_cut[j] ? val_cut[j] : 0.00;
              }
              for(var j=0;j<12;j++){
                   val_sum[j] = 0;
              }

              k = 0;
              count = 0;

              for(var i = 0;i<e_year;i++){
                   for(var j=0;j<12;j++){
                        val_sum[j] = val_sum[j]+val_cut[j+count];
                   }
                   k = k+1;
                   count = 12*k;
               }

              val_avg = [];
              //----------------------Calculate------------------------------
              for(var j=0;j<12;j++){
                    val_avg[j] = val_sum[j]/e_year;
              }

              val_avg[4] = (val_avg[3]+val_avg[5])/2;
              //----------------------append---------------------------
              var month = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
              var data_pg = new Array(11);
              var set = "1970";
              for(var i = 0;i<12;i++){
                 data_pg[i] = new Array(1);
                 for(var j=0;j<2;j++){
                      if(j==0){
                           //data_pg[i][j] = start+"-"+month[i];//
                           data_pg[i][j] = set+"-"+month[i]; 
                       }else if(j==1){
                           data_pg[i][j] = val_avg[i];
                       }
                 
                 }
              }
            
            return data_pg;
        }

         $( "select" ).change(function (e) {
              e.preventDefault();
              var csrftoken = getCookie('csrftoken');
              var index = $("#cindexFuture").val();
              var variable = $( "#varFuture" ).val();
              var gd_size = $( "#gridSize" ).val();
              var ind = carsAndModels[variable][index];
              $.ajax({
                     url : window.location.href, // the endpoint,commonly same url
                     type : "POST", // http method
                     data : { csrfmiddlewaretoken : csrftoken, 
                     path_file : ind+''+variable,
                     path_gd: gd_size
                     //,path_gd : gd_size,indx:ind+variable,rcp:rcp,s_table:table
                   }, 
                   // data sent with the post request
                   // handle a successful response
                   success : function(json) {
                                map_hist = json["map_hist"];
                                map_rcp45 = json["map_rcp45"];
                                map_rcp85 = json["map_rcp85"];
                                //console.log(json['hist_data']);
                                //console.log("Select: "+map_rcp85.features[0].properties.value);
                                map_plot(variable,ind);  
                                cal_year(json['hist_data'],json['rcp45'],json['rcp85'],variable,ind);


                   },

                   // handle a non-successful response
                   error : function(xhr,errmsg,err) {
                        console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
                   }
               }); 
        });
        function cal_year(data,data2,data3,variable,ind) {

             data_pg =  cal_sumMounth(data,variable,ind);
             //document.getElementById("SlH").innerHTML =data_pg;
             data_pg2 =  cal_sumMounth(data2,variable,ind);

             data_pg3 =  cal_sumMounth(data3,variable,ind);
             var name;
            if (variable == 1 && ind == "max")
             {
                document.getElementById("graph_head").innerHTML = "Graph Compare Hist RCP8.5 and RCP4.5 : Maximum Temperature";
                name = "Maximum Temperature";
             }else if(variable == 1 && ind == "min"){
                document.getElementById("graph_head").innerHTML = "Graph Compare Hist RCP8.5 and RCP4.5 : Minimum Temperature";
                name = "Minimum Temperature";
             }else if(variable == 1 && ind == "avg"){
                document.getElementById("graph_head").innerHTML = "Graph Compare Hist RCP8.5 and RCP4.5 : Average Temperature";
                name = "Average Temperature";
             }else if(variable == 2 && ind == "max"){
                document.getElementById("graph_head").innerHTML = "Graph Compare Hist RCP8.5 and RCP4.5 : Maximum Precipitation";
                name = "Maximum Precipitation";
             }else if(variable == 2 && ind == "avg"){
                document.getElementById("graph_head").innerHTML = "Graph Compare Hist RCP8.5 and RCP4.5 : Average Precipitation";
                name = "Average Precipitation";
             }else if(variable == 2 && ind == "min"){
                document.getElementById("graph_head").innerHTML = "Graph Compare Hist RCP8.5 and RCP4.5 : Minimum Precipitation";
                name = "Minimum Precipitation";
             };

             //document.getElementById("SlF").innerHTML =  data_pg +"|||||||||||"+data_pg2;             
             d3.select("#map_graph").selectAll("svg").remove(); 
             int_chart(data_pg,data_pg2,data_pg3,name);
        }

        function int_chart(mon_plot1,mon_plot2,mon_plot3,name) {
            //d3.selectAll("svg > *").remove();

            var margin3 = {top: 40, right: 40, bottom: 50, left: 50},
            width3 = $('#map_graph').width()- margin3.left - margin3.right,
            height3 = $('#map_graph').height()- margin3.top - margin3.bottom;

            //var graph = d3.select("#map_graph1");
            //var ts = parseInt(graph.style('width'));
            //console.log("width3: ", height3)

            var parseD = d3.time.format("%Y-%b").parse;

            var data1 = mon_plot1.map(function(d) {
               //document.getElementById("demo").innerHTML = d[0];
               return {
                  date: parseD(d[0]),
                  close: d[1],
                  name: "Hist"
               };
              
             });

            var data2 = mon_plot2.map(function(d) {
               //document.getElementById("demo").innerHTML = d[0];
               return {
                  date_p: parseD(d[0]),
                  close_p: d[1],
                  name:"RCP45"
               };
              
             });

            var data3 = mon_plot3.map(function(d) {
               //document.getElementById("demo").innerHTML = d[0];
               return {
                  date_p2: parseD(d[0]),
                  close_p2: d[1],
                  name:"RCP85"
               };
              
             });


          
            var ind_data = [];
            var ind_data2 = [];
            var ind_data3 = [];

            //var mss = data[0];
            mon_plot1.forEach(function(d,i) {
                ind_data[i] = d[1];
            });
            mon_plot2.forEach(function(d,i) {
                ind_data2[i] = d[1];
            });
            mon_plot3.forEach(function(d,i) {
                ind_data3[i] = d[1];
            });
             
            var x3 = d3.time.scale().range([0, width3]);
            var y3 = d3.scale.linear().range([height3, 0]);

            var xAxis3 = d3.svg.axis().scale(x3).orient("bottom").ticks(d3.time.months, 1).tickFormat(d3.time.format("%b"));
            var yAxis3 = d3.svg.axis().scale(y3).orient("left").ticks(5);;
            

            var line_p = d3.svg.line()
                //.interpolate("basis")
                .x(function(d) { return x3(d.date_p); })
                .y(function(d) { return y3(d.close_p); });

            var line = d3.svg.line()
                //.interpolate("basis")
                .x(function(d) { return x3(d.date); })
                .y(function(d) { return y3(d.close); });

            var line3 = d3.svg.line()
                //.interpolate("basis")
                .x(function(d) { return x3(d.date_p2); })
                .y(function(d) { return y3(d.close_p2); });


            var svg = d3.select("#map_graph").append("svg")
             .attr("width", width3 + margin3.left + margin3.right)
             .attr("height", height3 + margin3.top + margin3.bottom)
             .append("g")
             .attr("transform", "translate(" + margin3.left + "," + margin3.top + ")");

            svg.append("rect")
             .attr("width", width3)
             .attr("height", height3)
             .attr("fill", "#F4F4F4");

       
             x3.domain(d3.extent(data1, function(d) { return d.date; }));

             var find_max = [Math.max.apply(null,ind_data), Math.max.apply(null,ind_data2), Math.max.apply(null,ind_data3)];

             var find_min = [Math.min.apply(null,ind_data), Math.min.apply(null,ind_data2), Math.min.apply(null,ind_data3)];

             max_num = Math.max.apply(null,find_max);
             min_num = Math.min.apply(null,find_min);

             y3.domain([min_num-2,max_num+2]);
             
         
            svg.append("path")
              .attr("class", "line")
              .style("stroke", "#000000")
              .attr("d", line(data1));
            

            svg.append("g")
               .attr("class", "x axis")
               .attr("transform", "translate(0," + height3 + ")")
               .call(xAxis3);

            
            svg.append("path")
              .attr("class", "line")
              .style("stroke", "#333399")
              .attr("d", line_p(data2));

            svg.append("path")
              .attr("class", "line")
              .style("stroke", "#800000")
              .attr("d", line3(data3));

            svg.append("g")
               .attr("class", "y axis")
               .call(yAxis3);

            svg.selectAll("dot")
                .data(data1)
                .enter().append("circle")
                .style("fill", "#000000")    
                .attr("r", 2.0)   
                .style("opacity", .9)      // set the element opacity
                .style("stroke", "#f93")    // set the line colour
                //.style("stroke-width", 0.1) 
                .attr("cx", function(d) { return x3(d.date); })
                .attr("cy", function(d) { return y3(d.close); });

            svg.selectAll("dot")
                .data(data2)
                .enter().append("circle")
                .style("fill", "#333399")    
                .attr("r", 2.0)
                .style("opacity", .9)      // set the element opacity
                .style("stroke", "#f93")    // set the line colour
                //.style("stroke-width", 0.1) 
                .attr("cx", function(d) { return x3(d.date_p); })
                .attr("cy", function(d) { return y3(d.close_p); });

            svg.selectAll("dot")
                .data(data3)
                .enter().append("circle")
                .attr("r", 2.0)
                .style("fill", "#800000")    
                .style("opacity", .9)      // set the element opacity
                .style("stroke", "#f93")    // set the line colour
                //.style("stroke-width", 0.1) 
                .attr("cx", function(d) { return x3(d.date_p2); })
                .attr("cy", function(d) { return y3(d.close_p2); });

            svg.append("text")
               .attr("x", 30)
               .attr("y",-12 )
               .style("font-family", "sans-serif")
               .style("fill", "black")
               .style("font-size","9px")
               .style("font-weight","bold")
               .style("text-anchor", "middle")
               .text(name);

            svg.append("text")
               .attr("x", width3+20)
               .attr("y", height3+5 )
               .style("font-family", "sans-serif")
               .style("fill", "black")
               .style("font-size","9px")
               .style("font-weight","bold")
               .style("text-anchor", "middle")
               .text("Month");

           /* svg.append("text")
               .attr("x", (width3/2)-9 )
               .attr("y", -10 )
               .style("fill", "#000033")
               .style("font-family", "sans-serif")
               .style("text-anchor", "middle")
               .attr("font-size", "11px")
               .text(ind+" in Jan-Dec ");*/
           //---------------------- Hist Legned ------------------
            var rectangle = svg.append("rect")
              .attr("x", 80)
              .attr("y", height3 + 30)
              .attr("width", 10)
              .attr("height", 10);

            svg.append("text")
               .attr("x", 120 )
               .attr("y", height3 + 40 )
               .style("font-family", "sans-serif")
               .style("text-anchor", "middle")
               .attr("font-size", "11px")
               .text("Hist");
            //---------------------- RCP45 Legned ------------------
            var rectangle = svg.append("rect")
              .attr("x", 160)
              .attr("y", height3 + 30)
              .style("fill", "#333399")
              .attr("width", 10)
              .attr("height", 10);

            svg.append("text")
               .attr("x", 200 )
               .attr("y", height3 + 40 )
               .style("font-family", "sans-serif")
               .style("text-anchor", "middle")
               .attr("font-size", "11px")
               .text("RCP45");

            //---------------------- RCP85 Legned ------------------
            var rectangle = svg.append("rect")
              .attr("x", 240)
              .attr("y", height3 + 30)
              .style("fill", "#800000")
              .attr("width", 10)
              .attr("height", 10);

            svg.append("text")
               .attr("x", 280 )
               .attr("y", height3 + 40 )
               .style("font-family", "sans-serif")
               .style("text-anchor", "middle")
               .attr("font-size", "11px")
               .text("RCP85");

        }



function start(){
  ChangeCarList();
  $("#cindexHist").val("avg");
  $( "#varFuture" ).val(1);
  $( "#gridSize" ).val("0");
  cal_year(graph_max_hist,graph_max_rcp45,graph_max_rcp85,1,"avg");
  document.getElementById("graph_head").innerHTML = "Graph Compare Hist RCP8.5 and RCP4.5 : Average Temperature";
  map_plot("1","avg");     
}
start();
  
