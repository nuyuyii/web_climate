    

/* ******************************************************** */  
/* ****************** Legend Color Map ******************** */
/* ******************************************************** */
// var nLegend = ["temperature (°C)", "preciptation (mm/day)"]
var nLegend = {"1": {"color":["#4575b4", "#ffffbf", "#a50026"],
"name":"Temperature (°C)","domain":[-10, 20, 35],"scale":[-10, 40]},
"2":{"color":["#ffffe5","#33FFCC","#00441b"],"name":"preciptation (mm/day)",
"domain":[-5, 2,14],"scale":[0, 15]}};
//["#543005","#FFFFFF","#2EFEF7","#003c30"]
//["#9F5000","#ffffe5","#0C6944"]
//"#543005","#7fcdbb","#2c7fb8"
//"prec":{"color":["#ffffe5","#33FFCC","#00441b"],"name":"preciptation (mm/day)",
//"domain":[-3, 5,18],"scale":[0, 20]}};

function len(n){
    d3.select('#legend_p').selectAll("svg").remove();
    d3.select('#map_past').selectAll("svg").remove();
    d3.select('#map_future').selectAll("svg").remove();

var indL = n;
var i_ticks = 15;  // max_value is scale/2

var legend = d3.select('#legend_H');

var widthL = parseInt(legend.style('width')),
    heightL = parseInt(legend.style('height'));

var color = d3.scale.linear()
    .domain(nLegend[indL]['domain'])
    .range(nLegend[indL]['color'])
    .interpolate(d3.interpolateHcl);

var x = d3.scale.linear()
    .domain(nLegend[indL]['scale'])
    .range([0, widthL-20]);

var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom")
    .tickSize(10)
    .ticks(i_ticks)
    .tickFormat(d3.format("+.0f"));

//console.log("legend w/h",widthL,heightL);

var svgL = legend.append("svg")
    .attr("width", widthL)
    .attr("height", heightL)
    .attr("class", "legend_txt")
  .append("g")
    .attr("transform", "translate(10,20)");

svgL.selectAll("rect")
    .data(pair(x.ticks(i_ticks)))
  .enter().append("rect")
    .attr("height", 10)
    .attr("x", function(d) { return x(d[0]); })
    .attr("width", function(d) { return x(d[1]) - x(d[0]); })
    .style("fill", function(d) { return color(d[0]); });

svgL.call(xAxis).append("text")
    .attr("class", "caption")
    .attr("y", -10)
    .attr("x", widthL/2)
    .text(nLegend[indL]['name']);


function pair(array) {
  return array.slice(1).map(function(b, i) {
    //console.log(array[i], b);
    return [array[i], b];
  });
}

/* ******************************************************** */  
/* **************** Map Projection d3 plot **************** */
/* ******************************************************** */
var mapP = d3.select('#map_past');

//var tooltip = map.append("div").attr("class", "tooltip hidden");

var width = parseInt(mapP.style('width')),
height = parseInt(mapP.style('height'));

var graticule = d3.geo.graticule();
var zoomMap = d3.behavior.zoom().translate([0,0])//.center([117.9, 6.7])
                .scale(1).scaleExtent([1, 20]).on("zoom", moveMap);

function moveMap(){
    var t = d3.event.translate;
    var s = d3.event.scale;

    var w_max = 0;
    var w_min = width * (1 - s);
    var h_max = height < s*width/2 ? s*(width/2-height)/2 : (1-s)*height/2;
    var h_min = height < s*width/2 ? -s*(width/2-height)/2-(s-1)*height : (1-s)*height/2;

    t[0] = Math.min(w_max, Math.max(w_min, t[0]));
    t[1] = Math.min(h_max, Math.max(h_min, t[1]));

    console.log(t,s)

    zoomMap.translate(t);
    gMap.attr("transform", "translate(" + t + ")scale(" + s + ")");
    gMap.selectAll("path").attr("class","MapColor");
    //console.log(t,s);
    gMap2.attr("transform", "translate(" + t + ")scale(" + s + ")");
    gMap2.selectAll("path").style("stroke-width", .9 / s + "px");
}

/* Future */
var mapFt = d3.select('#map_future');
var zoomMapFt = d3.behavior.zoom().translate([0,0])//.center([117.9, 6.7])
                .scale(1).scaleExtent([1, 20]).on("zoom", moveMap);
function moveMapFt(){
    var t = d3.event.translate;
    var s = d3.event.scale;

    var w_max = 0;
    var w_min = width * (1 - s);
    var h_max = height < s*width/2 ? s*(width/2-height)/2 : (1-s)*height/2;
    var h_min = height < s*width/2 ? -s*(width/2-height)/2-(s-1)*height : (1-s)*height/2;

    t[0] = Math.min(w_max, Math.max(w_min, t[0]));
    t[1] = Math.min(h_max, Math.max(h_min, t[1]));

    console.log(t,s)

    zoomMapFt.translate(t);
    gMapFt.attr("transform", "translate(" + t + ")scale(" + s + ")");
    gMapFt.selectAll("path").attr("class","MapColor");
    //console.log(t,s);
    gMap2Ft.attr("transform", "translate(" + t + ")scale(" + s + ")");
    gMap2Ft.selectAll("path").style("stroke-width", .9 / s + "px");
}
// --------------------------------------
// --------------- setup map
var projection, pathMap, svgMap, gMap, gMap2;

setupMap(width,height);  // console.log(width,height)

function setupMap(width,height){
               
    projection = d3.geo.equirectangular()
                .center([117.9, 5.6]).scale(width) //width/2/Math.PI
                .translate([width/2, height/2]);

    pathMap = d3.geo.path().projection(projection);
    svgMap = mapP
            .append("svg")
            .attr("width", '100%')
            .attr("height", '100%')
            .attr("class","svgMap")
            .call(zoomMap);

    gMap = svgMap.append("g"),
    gMap2 = svgMap.append("g");
    drawMapL0(maxts_data.features);
}

/* Future */
var projectionFt, pathMapFt, svgMapFt, gMapFt, gMap2Ft;
var widthFt = parseInt(mapFt.style('width')),
heightFt = parseInt(mapFt.style('height'));
setupMap_Ft(widthFt,heightFt);
console.log("width: "+width);  

function setupMap_Ft(width,height){
    console.log("widthFt: "+width);           
    projectionFt = d3.geo.equirectangular()
                .center([117.9, 6.7]).scale(width) //width/2/Math.PI
                .translate([width/2, height/2]);

    pathMapFt = d3.geo.path().projection(projection);
    svgMapFt = mapFt
            .append("svg")
            .attr("width", '100%')
            .attr("height", '100%')
            .attr("class","svgMap")
            .call(zoomMapFt);

    gMapFt = svgMapFt.append("g"),
    gMap2Ft = svgMapFt.append("g");
    drawMapL0_Ft(maxts_data.features);
}
// --------------------------------------
// --------------- port layer0 color map
var dataTs,dataLand,dataPr;

/*var data9T;
d3.json('../static/data/geoAvgTsPrG9.json', function(json) {
    console.log("FF",json.length);   
    data9T = json.features;
});*/

function drawMapL0(json){
    // data temp coordinate
    // d.id 0-252 keep  d.coordinate[0][1] lon -180 to 180 
    // d.id %252 keep d.coordinate[0][0] lat -90 to -90
    // console.log(json[1]);
    gMap.selectAll("path")
        .data(json)
        .enter().append("path")
        .attr("d", pathMap)
        .attr("class","MapColor")
        .style("fill", function(json){ return color(json.properties.ts[0]);})
        .style("stroke",function(json){ return color(json.properties.ts[0]);});
                    //.style("stroke-width",1)data9T
                    //.style("stroke-opacity",1);
                    //.style("opacity",1);
}
/* Future */
function drawMapL0_Ft(json){
    console.log("drawMapL0_Ft");

    gMapFt.selectAll("path")
        .data(json)
        .enter().append("path")
        .attr("d", pathMapFt)
        .attr("class","MapColor")
        .style("fill", function(data){ return color(data.properties.ts[2]);})
        .style("stroke",function(data){ return color(data.properties.ts[2]);});
}
console.log(maxts_data);
/*d3.json(maxts_data, function(json) {
    dataTs = json.features;
    //var dou=[];
    //var tt = dataTs.properties.ts[1]
    console.log("msp: "+dataTs[0].properties.allts)
    //dataTs.forEach(function(d, i){
    //dou.push(data9T[i].properties.allts)
    //    d.properties.allts = +data9T[i].properties.allts;
    //})
                //console.log(dou)
    drawMapL0(maxts_data.features);
});*/

            
           /*
            d3.json('../static/data/geoTsPrAvg.json', function(json) {
                console.log(json);
            });*/
// --------------------------------------
// --------------- Base map
d3.json('../static/data/world-topo-min.json', function(json) {
    dataLand = topojson.feature(json, json.objects.countries).features
    drawMapL1(dataLand);
    drawMapL1_Ft(dataLand);
});

function drawMapL1(json){
    gMap2.append("path")
          .datum(graticule)
          .attr("class","graticule")
          .attr("d", pathMap);

    gMap2.selectAll("path")
         .data(json)
         .enter().append("path")
         .attr("d", pathMap)
         .style("stroke-width",0.9);
                    //.attr("class","country");
                    //.style("fill","white");
                    /*.on("mousemove", function(d,i){
                        var mouse = d3.mouse(svgMap.node()).map( function(d) { return parseInt(d); } );
                        var left = Math.min(width-12*d.properties.name.length, (mouse[0]+20));
                        var top = Math.min(height-40, (mouse[1]+20));

                        tooltip.classed("hidden", false)
                            .attr("style", "left:"+left+"px;top:"+top+"px")
                            .html(d.properties.name);
                    })
                    .on("mouseout",  function(d,i) {
                        tooltip.classed("hidden", true);
                    });*/
}

function drawMapL1_Ft(json){
    gMap2Ft.append("path")
          .datum(graticule)
          .attr("class","graticule")
          .attr("d", pathMap);

    gMap2Ft.selectAll("path")
         .data(json)
         .enter().append("path")
         .attr("d", pathMapFt)
         .style("stroke-width",0.9);
}

// --------------------------------------
// --------------- Resize window
d3.select(window).on('resize', function() {
    width = parseInt(mapP.style('width'));
    height = parseInt(mapP.style('height'));

    projection.scale(width) //width/2/Math.PI
              .translate([width/2, height/2]);
    pathMap = d3.geo.path().projection(projection);
    gMap.selectAll("path")
        .attr("d", pathMap);

    gMap2.selectAll("path")
         .attr("d", pathMap);
});

// --------------------------------------
// --------------- Redraw Map
function resetMap(newdata){
    width = parseInt(mapP.style('width'));
    height = parseInt(mapP.style('height'));
    console.log("Reset Ok");
    /*gMap.transition()
        .duration(500)
        .attr("transform","translater("+width/2+","+height/2+")scale(" +width+ ")");
        //.style("stroke-width", .5 + "px");
    gMap2.transition()
         .duration(500)
         .attr("transform","translater("+width/2+","+height/2+")scale(" +width+ ")")
         .style("stroke-width", .9 + "px");*/

    d3.select('.svgMap').remove();
    setupMap(width,height)
    drawMapL0(dataLand);
    drawMapL1(newdata.features);
    //setupMap(width,height);
};



d3.selectAll("button[resetP]").on("click", resetMap);

  
/* ---------------------------------------------------------------------- */
///////////////////////////// Future Map Plot //////////////////////////////
/* ---------------------------------------------------------------------- */
  function resetMap_Ft(newdata){
    width = parseInt(mapFt.style('width'));
    height = parseInt(mapFt.style('height'));
    console.log("Reset Ok");
    d3.select('.svgMapFt').remove();
    setupMap_Ft(width,height)
    drawMapL0_Ft(dataLand);
    drawMapL1_Ft(newdata.features);
    //setupMap(width,height);
  };
};

//---------------------------------------------------------



///////////////////////////////////////////////////////////////////////////
//////////////////// Select Temp Or Prec Index ////////////////////////////
/////////////////////////////////////////////////////////////////////////// 

var temp="1"; 
$("#vars").val(temp);

var carsAndModels = {};
    carsAndModels['1'] = ['max','min','avg','-----','su', 'fd', 'id','tr','gsl','wsdi','csdi','dtr','txx','tnx','txn','tnn','tn10p','tx10p','tn90p','tx90p'];
    carsAndModels['2'] = ['max','min','avg','-----','rx1day', 'rx5day', 'sdii', 'r10mm','r20mm','rnnmm','cdd','cwd','r95ptot','r99ptot','prcptot'];

/* ------------------------------------------------------*/
/* --------------- onchang Historycal -------------------*/
function ChangeCarList() {
  var carList = document.getElementById("varHist");
  var modelList = document.getElementById("cindexHist");

  var selCar = carList.options[carList.selectedIndex].value;

  console.log("Hist: "+selCar);

  while (modelList.options.length) {
    modelList.remove(0);
  }
  var cars = carsAndModels[selCar];
  if (cars) {
    var i;
    for (i = 0; i < cars.length; i++) {
      var car = new Option(cars[i], i);
      modelList.options.add(car);
    }
  }
} 
/* ------------------------------------------------------*/
/* --------------- onchang Future -----------------------*/    
function ChangeFf() {
  var FutureVar = document.getElementById("varFuture");
  var FutureInx = document.getElementById("cindexFuture");
  var FutureSlt = FutureVar.options[FutureVar.selectedIndex].value;

  console.log("Future: "+FutureSlt);

  while (FutureInx.options.length) {
    FutureInx.remove(0);
  }
  var IndxF = carsAndModels[FutureSlt];
  if (IndxF) {
    var i;
    for (i = 0; i < IndxF.length; i++) {
      var future = new Option(IndxF[i], i);
      FutureInx.options.add(future);
    }
  }
} 

///////////////////////////////////////////////////////////////////////////
//////////////////// Select Temp Or Prec Index ////////////////////////////
/////////////////////////////////////////////////////////////////////////// 
function getCookie(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie != '') {
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
      var cookie = jQuery.trim(cookies[i]);
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) == (name + '=')) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}


/*-------------------------------------------------------------*/      
/*---------------- Select Index Hist Using Ajax ---------------*/      
$( "#SlHist" ).change(function (e) {
  e.preventDefault();
  var csrftoken = getCookie('csrftoken');
  //label1Visible()
  var index = $("#cindexHist").val();
  var variable = $( "#varHist" ).val();
  var ind = carsAndModels[variable][index];
  document.getElementById("SlH").innerHTML=ind+''+variable;
  len(variable);

  //document.getElementById("tool").innerHTML = ind;
  $.ajax({
    url : window.location.href, // the endpoint,commonly same url
    type : "POST", // http method
    data : { csrfmiddlewaretoken : csrftoken, 
             path_file : ind //+''+variable
            }, // data sent with the post request
               // handle a successful response
    success : function(json) {
        console.log(json); // another sanity check
        //resetMap(json);
        //On success show the data posted to server as
        //data(json['path_file'],ind,variable);
        //alert('Hi '+json['path_file']);
    },
        // handle a non-successful response
    error : function(xhr,errmsg,err) {
        console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
    }
  });
});

/*-------------------------------------------------------------*/      
/*---------------- Select Index Ft Using Ajax -----------------*/ 
$( "#SlFuture" ).change(function (e) {
  e.preventDefault();
  var csrftoken = getCookie('csrftoken');
  //label1Visible()
  var index = $("#cindexFuture").val();
  var variable = $( "#varFuture" ).val();
  var ind = carsAndModels[variable][index];
  document.getElementById("SlF").innerHTML=ind+''+variable;
  console.log("Future: "+variable);
  //document.getElementById("tool").innerHTML = ind;
  $.ajax({
    url : window.location.href, // the endpoint,commonly same url
    type : "POST", // http method
    data : { csrfmiddlewaretoken : csrftoken, 
             path_file : ind //+''+variable
            }, // data sent with the post request
               // handle a successful response
    success : function(json) {
        console.log(json); // another sanity check
        //resetMap_Ft(json);
    },
        // handle a non-successful response
    error : function(xhr,errmsg,err) {
        console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
    }
  });
});


///////////////////////////////////////////////////////////////////////////
//////////////////// Select Slider Bar Time ///////////////////////////////
/////////////////////////////////////////////////////////////////////////// 

console.log("rangeSlider");
var slide = d3.select('.range-slider');
var width = parseInt(slide.style('width'));

//$(".range-slider__range::-moz-range-thumb").css("width","10px");

var rangeSlider = function(){

/*  var tradio = d3.select('input[name="radioHist"]:checked').node().value;
  console.log(tradio);
  var selectTime = tradio;
  var dio = $('input[name="radioHist"]');
  dio.on('input', function(){    
    selectTime = this.value;
    var maxYear = 2006 - parseInt(this.value);
    console.log(selectTime);
    $("#slideHist").prop({
        max:maxYear
      }).slider(refresh);
    
  });*/
  
 /* var radioFt = d3.select('input[name="radioFuture"]:checked').node().value;
  console.log(radioFt);
  var selectTimeFt = radioFt;
  var dioFt = $('input[name="radioFuture"]');
  dioFt.on('input', function(){    
    selectTimeFt = this.value;
    var maxYearFt = 2101 - parseInt(this.value);
    console.log(selectTimeFt);
    $("#slideFuture").prop({
        max:maxYearFt
      }).slider(refresh);
    
  });
  */
  var slider = $('.range-slider'),
      range = $('#slideHist'),
      value = $('#valueHist'),
      rangeFt = $('#slideFuture'),
      valueFt = $('#valueFuture');
    
 // slider.each(function(){
      //var maxYear = 2006 - parseInt(selectTime);
      //console.log("bg"+ maxYear+" time:"+selectTime);
  //    $(".range-slider__range").css("background-color", "#1abc9c");
  /*--- History Slider Bar ---*/
    value.each(function(){      
      //var endY =  parseInt(value) + parseInt(selectTime);
      var value = $(this).prev().attr('value');
      //var endY =  parseInt(value) + parseInt(selectTime);
      //console.log(endY);    
      $(this).html(value);
    });

    range.on('input', function(){
      //var endY =  parseInt(this.value) + parseInt(selectTime);
      //console.log(endY);     

      $(this).next(value).html(this.value);
    });

  /*--- Future Slider Bar ---*/
    valueFt.each(function(){      
      //var endY =  parseInt(value) + parseInt(selectTime);
      var value = $(this).prev().attr('value');
      //var endY =  parseInt(value) + parseInt(selectTimeFt);
     // console.log(endY);    
      $(this).html(value);
    });

    rangeFt.on('input', function(){
      //var endY =  parseInt(this.value) + parseInt(selectTimeFt);
      $(this).next(value).html(this.value);
    });
 // });
};

rangeSlider();
     
function start(){
  ChangeCarList();
  ChangeFf();
  len("1");
}
start();

