///////////////////////////////////////////////////////////////
// Concept
/*  - Convert the SVG string to a Blob
 *  - Get an URL for Blob
 *  - Create an image element and set the URL as src
 *  - When loaded(onload) you can draw the SVG as image on canvas
 *  - Use toDataURL() to get the PNG file from canvas
 *  -----------------------------------------------------------
 ******** document.createElement( "canvas" ) 
 ******** set .width, .height, .style.border
 ******** set canvas to .getContext( "2d" )
 ******** url of canvas keep to dataUri
 ******** create img use document.createElement( "img" )
 ******** load SVG as image on canvas use img.src = dataUri
*/
// console.log("--------------Save: "+nLegend[1]['name'])
// document.getElementById('Demo').innerHTML = "Ok"+parseInt(mapP.attr("width"));

    var exportPNG = function() {
    	
    /*
    Based off  gustavohenke's svg2png.js
    gist.github.com/gustavohenke/9073132
    */
 
    var svg = document.querySelector( "#mapRCP45" );
    //console.log("ok :")
    console.log("--------------Save: "+parseInt(mapP.style('width')))

    var svgData = new XMLSerializer().serializeToString( svg );
    /* svgData is string (picture) */

    var canvas = document.createElement( "canvas" );
    canvas.width = 464;
    canvas.height = 464;

    var ctx = canvas.getContext( "2d" );
 
    var dataUri = '';
    try {
        dataUri = 'data:image/svg+xml;base64,' + btoa(svgData);
        /* canvas to string (text) */
    } catch (ex) {
 
        // For browsers that don't have a btoa() method, send the text off to a webservice for encoding
        /* Uncomment if needed (requires jQuery)
        $.ajax({
            url: "http://www.mysite.com/webservice/encodeString",
            data: { svg: svgData },
            type: "POST",
            async: false,
            success: function(encodedSVG) {
                dataUri = 'data:image/svg+xml;base64,' + encodedSVG;
            }
        })
        */
 
    }
 
    var img = document.createElement( "img" );
 
    img.onload = function() {
        ctx.drawImage( img, 0, 0 );
 
        try {
 
            // Try to initiate a download of the image
            var a = document.createElement("a");
            a.download = "MDMS_Graph_Export.png";
            a.href = canvas.toDataURL("image/png");
            document.querySelector("body").appendChild(a);
            a.click();
            document.querySelector("body").removeChild(a);
 
        } catch (ex) {
 
            // If downloading not possible (as in IE due to canvas.toDataURL() security issue)
            // then display image for saving via right-click
 
            var imgPreview = document.createElement("div");
            imgPreview.appendChild(img);
            document.querySelector("body").appendChild(imgPreview);
 
        }
    };
 
    img.src = dataUri; 
}