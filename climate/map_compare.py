from decimal import *
import csv
import json
import numpy as np
import datetime as dt
from climate.models import t_compare


from django.conf import settings
import os

class Compare_map(object):
    #def __init__(self):
    #    self.gd_size = int(gd_size)
        #super(Indexs, self).__init__()

    def file_grid(self, gd_size):
        files = ["geoG9Cliamte", "geoG4Cliamte", "geoG1Cliamte"]
        urls = os.path.join(settings.BASE_DIR,'climate/static/data/'+files[gd_size]+'.json')
        with open(urls) as data_file:    
            index_json = json.load(data_file)          
        return index_json    

    def check_indx(self, clim_index, gd_size):        
        indxValue = check_file(gd_size)
        json_hist,maxValue,minValue = check_inx(clim_index, "hist", gd_size)
        json_rcp45 = check_inx(clim_index, "rcp45", gd_size)
        json_rcp85 = check_inx(clim_index, "rcp85", gd_size)
        return json_hist,json_rcp45,json_rcp85

    def check_inx(self, clim_index, file, gd_size):
        index_json = file_grid(gd_size)
        indxCal = []
        if (clim_index=="max1"): 
            indxCal = indxValue[file].ts_max
        elif (clim_index=="min1"): 
            indxCal = indxValue[file].ts_min
        elif (clim_index=="avg1"): 
            indxCal = indxValue[file].ts_avg
        elif (clim_index=="max2"): 
            indxCal = indxValue[file].pr_max
        elif (clim_index=="min2"): 
            indxCal = indxValue[file].pr_min
        elif (clim_index=="avg2"): 
            indxCal = indxValue[file].pr_avg

        cnt_gd = 0
        for gd in var:
            gd['properties']['value'] = indxCal[cnt_gd]
            cnt_gd = cnt_gd + 1
        maxValue = np.max(indxCal)
        minValue = np.min(indxCal)

        return index_json

    def check_file(self, gd_size):
        print("check: ",time_start,' - ',time_end,' rcp: ',rcp,' table: ',s_table)
        listYear = range(time_start,time_end)
        indxValue = {}
        if (gd_size==0):
            indxValue["hist"] = t_compare.objects.get(g_size="3x3")
            indxValue["rcp45"] = t_compare.objects.get(g_size="3x3")
            indxValue["rcp85"] = t_compare.objects.get(g_size="3x3")
        elif (gd_size==1):
            indxValue["hist"] = t_compare.objects.get(g_size="3x3")
            indxValue["rcp45"] = t_compare.objects.get(g_size="3x3")
            indxValue["rcp85"] = t_compare.objects.get(g_size="3x3")
        elif (gd_size==2):
            indxValue["hist"] = t_compare.objects.get(g_size="3x3")
            indxValue["rcp45"] = t_compare.objects.get(g_size="3x3")
            indxValue["rcp85"] = t_compare.objects.get(g_size="3x3")
        return indxValue