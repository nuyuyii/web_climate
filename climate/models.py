from django.db import models
from django.contrib.postgres.fields import JSONField

# ---------------------------------------------#
# ----------------- Table 1 -------------------#
class grid(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    point = JSONField()
    coordinates = JSONField()
    class Meta:
        db_table = 't01grid'

# ---------------------------------------------#
# ----------------- Table 9 -------------------#
class ms_hist(models.Model):
    id = models.IntegerField(db_column='year_id',primary_key=True)
    year = models.IntegerField(db_column='year')
    ts_avg = JSONField()
    ts_max = JSONField()
    ts_min = JSONField()
    pr_avg = JSONField()
    pr_max = JSONField()
    pr_min = JSONField()
    class Meta:
        db_table = 't1_measure_hist'

# ---------------------------------------------#
# ----------------- Table 10 ------------------#
class ms_rcp85(models.Model):
    id = models.IntegerField(db_column='year_id',primary_key=True)
    year = models.IntegerField(db_column='year')
    ts_avg = JSONField()
    ts_max = JSONField()
    ts_min = JSONField()
    pr_avg = JSONField()
    pr_max = JSONField()
    pr_min = JSONField()
    class Meta:
        db_table = 't1_measure_rcp85'

# ---------------------------------------------#
# ----------------- Table 11 -------------------#
class ms_rcp45(models.Model):
    id = models.IntegerField(db_column='year_id',primary_key=True)
    year = models.IntegerField(db_column='year')
    ts_avg = JSONField()
    ts_max = JSONField()
    ts_min = JSONField()
    pr_avg = JSONField()
    pr_max = JSONField()
    pr_min = JSONField()
    class Meta:
        db_table = 't1_measure_rcp45'

# ---------------------------------------------#
# ----------------- Table 12 -------------------#
class indx_rpc85(models.Model):
    id = models.IntegerField(db_column='year_id',primary_key=True)
    year = models.IntegerField(db_column='year')
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 't1_index_rcp85'

# ---------------------------------------------#
# ----------------- Table 13 -------------------#
class indx_rpc45(models.Model):
    id = models.IntegerField(db_column='year_id',primary_key=True)
    year = models.IntegerField(db_column='year')
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 't1_index_rcp45'

# ---------------------------------------------#
# ----------------- Table 14 -------------------#
class indx_hist(models.Model):
    id = models.IntegerField(db_column='year_id',primary_key=True)
    year = models.IntegerField(db_column='year')
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 't1_index_hist'

#--------------------------------------------------------------------------------------------------#
# ---------------------------------------------#
# ---------------- MAP GRID 3x3 ---------------#
# ---------------------------------------------#
# ---------------------------------------------#
# ----------- Table 3x3 indx_hist -------------#
class t9_indx_hist(models.Model):
    id = models.IntegerField(db_column='year_id',primary_key=True)
    year = models.IntegerField(db_column='year')
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 't9_index_hist'

# ---------------------------------------------#
# ----------- Table 3x3 indx_rpc45 ------------#
class t9_indx_rcp45(models.Model):
    id = models.IntegerField(db_column='year_id',primary_key=True)
    year = models.IntegerField(db_column='year')
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 't9_index_rcp45'

# ---------------------------------------------#
# ----------- Table 3x3 indx_rpc85 ------------#
class t9_indx_rcp85(models.Model):
    id = models.IntegerField(db_column='year_id',primary_key=True)
    year = models.IntegerField(db_column='year')
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 't9_index_rcp85'

# --------------------------------------------#
# ------------ Table 3x3 ms_hist -------------#
class t9_ms_hist(models.Model):
    id = models.IntegerField(db_column='year_id',primary_key=True)
    year = models.IntegerField(db_column='year')
    ts_avg = JSONField()
    ts_max = JSONField()
    ts_min = JSONField()
    pr_avg = JSONField()
    pr_max = JSONField()
    pr_min = JSONField()
    class Meta:
        db_table = 't9_measure_hist'

# --------------------------------------------#
# ----------- Table 3x3 ms_rpc45 -------------#
class t9_ms_rcp45(models.Model):
    id = models.IntegerField(db_column='year_id',primary_key=True)
    year = models.IntegerField(db_column='year')
    ts_avg = JSONField()
    ts_max = JSONField()
    ts_min = JSONField()
    pr_avg = JSONField()
    pr_max = JSONField()
    pr_min = JSONField()
    class Meta:
        db_table = 't9_measure_rcp45'


# --------------------------------------------#
# ----------- Table 3x3 ms_rpc85 -------------#
class t9_ms_rcp85(models.Model):
    id = models.IntegerField(db_column='year_id',primary_key=True)
    year = models.IntegerField(db_column='year')
    ts_avg = JSONField()
    ts_max = JSONField()
    ts_min = JSONField()
    pr_avg = JSONField()
    pr_max = JSONField()
    pr_min = JSONField()
    class Meta:
        db_table = 't9_measure_rcp85'


#--------------------------------------------------------------------------------------------------#
# ---------------------------------------------#
# ---------------- MAP GRID 2x2 ---------------#
# ---------------------------------------------#
# ---------------------------------------------#
# ----------- Table 2x2 indx_hist -------------#
class t4_indx_hist(models.Model):
    id = models.IntegerField(db_column='year_id',primary_key=True)
    year = models.IntegerField(db_column='year')
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 't4_index_hist'

# ---------------------------------------------#
# ----------- Table 2x2 indx_rpc45 ------------#
class t4_indx_rcp45(models.Model):
    id = models.IntegerField(db_column='year_id',primary_key=True)
    year = models.IntegerField(db_column='year')
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 't4_index_rcp45'

# ---------------------------------------------#
# ----------- Table 2x2 indx_rpc85 ------------#
class t4_indx_rcp85(models.Model):
    id = models.IntegerField(db_column='year_id',primary_key=True)
    year = models.IntegerField(db_column='year')
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 't4_index_rcp85'

# --------------------------------------------#
# ------------ Table 2x2 ms_hist -------------#
class t4_ms_hist(models.Model):
    id = models.IntegerField(db_column='year_id',primary_key=True)
    year = models.IntegerField(db_column='year')
    ts_avg = JSONField()
    ts_max = JSONField()
    ts_min = JSONField()
    pr_avg = JSONField()
    pr_max = JSONField()
    pr_min = JSONField()
    class Meta:
        db_table = 't4_measure_hist'

# --------------------------------------------#
# ----------- Table 2x2 ms_rpc45 -------------#
class t4_ms_rcp45(models.Model):
    id = models.IntegerField(db_column='year_id',primary_key=True)
    year = models.IntegerField(db_column='year')
    ts_avg = JSONField()
    ts_max = JSONField()
    ts_min = JSONField()
    pr_avg = JSONField()
    pr_max = JSONField()
    pr_min = JSONField()
    class Meta:
        db_table = 't4_measure_rcp45'


# --------------------------------------------#
# ----------- Table 2x2 ms_rpc85 -------------#
class t4_ms_rcp85(models.Model):
    id = models.IntegerField(db_column='year_id',primary_key=True)
    year = models.IntegerField(db_column='year')
    ts_avg = JSONField()
    ts_max = JSONField()
    ts_min = JSONField()
    pr_avg = JSONField()
    pr_max = JSONField()
    pr_min = JSONField()
    class Meta:
        db_table = 't4_measure_rcp85'


# --------------------------------------------#
# ----------- Table Compare page -------------#
class t_compare(models.Model):
    id = models.IntegerField(db_column='year_id',primary_key=True)
    ts_avg = JSONField()
    ts_max = JSONField()
    ts_min = JSONField()
    pr_avg = JSONField()
    pr_max = JSONField()
    pr_min = JSONField()
    g_size = models.CharField(max_length=20,db_column='g_size')
    ipsl_file = models.CharField(max_length=20,db_column='ipsl_file')
    class Meta:
        db_table = 'y130allgrid'

#--------------------------------------------------------------------------------------------------#
# ---------------------------------------------#
# ----------------- Hist Graph ----------------#
# ---------------------------------------------#
#-----------------Average Future RPC85---------#
class avg_hist(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 'all_avg_hist'


class cal_maxmin(models.Model):
    id = models.IntegerField(db_column='cal_id',primary_key=True)
    pr_avg = JSONField()
    pr_max = JSONField()
    pr_min = JSONField()
    ts_avg = JSONField()
    ts_max = JSONField()
    ts_min = JSONField()
    class Meta:
        db_table = 'cal_rcp85' #cal max min avg for histrorical 1970-2005 ind all

class cal_allrpc85(models.Model):
    id = models.IntegerField(db_column='cal_id',primary_key=True)
    pr_avg = JSONField()
    pr_max = JSONField()
    pr_min = JSONField()
    ts_avg = JSONField()
    ts_max = JSONField()
    ts_min = JSONField()
    class Meta:
        db_table = 'calall_rcp85' #cal max min avg for histrorical 1970-2100 ind all

class cal_allrpc45(models.Model):
    id = models.IntegerField(db_column='cal_id',primary_key=True)
    pr_avg = JSONField()
    pr_max = JSONField()
    pr_min = JSONField()
    ts_avg = JSONField()
    ts_max = JSONField()
    ts_min = JSONField()
    class Meta:
        db_table = 'calall_rcp45' #cal max min avg for histrorical 1970-2100 ind all

# ----------------- Table 2 -------------------#
class rpc45_table1(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 'exindex_rpc45_01' #index in year 2006-2080 ind 48323

# ----------------- Table 2 -------------------#
class rpc45_table2(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 'exindex_rpc45_02' #index in year 2080-2100 ind 48323

# ----------------- Table 2 -------------------#
class rpc45_table3(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 'exindex_rpc45_03' #index in year 2006-2079 ind allind

# ----------------- Table 2 -------------------#
class rpc45_table4(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 'exindex_rpc45_04' #index in year 2080-2100 ind allind

class rpc85_table3(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 'exindex_rpc85_03' #index in year 2006-2079 ind allind

# ----------------- Table 2 -------------------#
class rpc85_table4(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 'exindex_rpc85_04' #index in year 2080-2100 ind allind

class cal_mounlyhis(models.Model):
    id = models.IntegerField(db_column='cal_id',primary_key=True)
    pr_avg = JSONField()
    pr_max = JSONField()
    pr_min = JSONField()
    ts_avg = JSONField()
    ts_max = JSONField()
    ts_min = JSONField()
    class Meta:
        db_table = 'mounly_his' #cal max min avg for histrorical 1970-2100 ind all

class cal_mounlyrcp45(models.Model):
    id = models.IntegerField(db_column='cal_id',primary_key=True)
    pr_avg = JSONField()
    pr_max = JSONField()
    pr_min = JSONField()
    ts_avg = JSONField()
    ts_max = JSONField()
    ts_min = JSONField()
    class Meta:
        db_table = 'mounly_rcp45' #cal max min avg for histrorical 1970-2100 ind all

class cal_mounlyrcp85(models.Model):
    id = models.IntegerField(db_column='cal_id',primary_key=True)
    pr_avg = JSONField()
    pr_max = JSONField()
    pr_min = JSONField()
    ts_avg = JSONField()
    ts_max = JSONField()
    ts_min = JSONField()
    class Meta:
        db_table = 'mounly_rcp85' #cal max min avg for histrorical 1970-2100 ind all

#--------------------------------------------------------------------------------------------------#
# ----------------- Thailand -------------------#
class thai_hist(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 'hist_thai' #index in year 1970-2005 ind allind

class thai_rcp85_t01(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 'rcp85_thai_t01' #index in year 2006-2079 ind allind


class thai_rcp85_t02(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 'rcp85_thai_t02' #index in year 2080-2100 ind allind

class thai_rcp45_t01(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 'rcp45_thai_t01' #index in year 2006-2079 ind allind

class thai_rcp45_t02(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 'rcp45_thai_t02' #index in year 2080-2100 ind allind


class all_rcp45_thai(models.Model):
    id = models.IntegerField(db_column='cal_id',primary_key=True)
    pr_avg = JSONField()
    pr_max = JSONField()
    pr_min = JSONField()
    ts_avg = JSONField()
    ts_max = JSONField()
    ts_min = JSONField()
    class Meta:
        db_table = 'rcp45_all_thai' #cal max min avg for histrorical 1970-2005 ind all

class all_rcp85_thai(models.Model):
    id = models.IntegerField(db_column='cal_id',primary_key=True)
    pr_avg = JSONField()
    pr_max = JSONField()
    pr_min = JSONField()
    ts_avg = JSONField()
    ts_max = JSONField()
    ts_min = JSONField()
    class Meta:
        db_table = 'rcp85_all_thai' #cal max min avg for histrorical 1970-2100 ind all

class all_rcp45_thai_02(models.Model):
    id = models.IntegerField(db_column='cal_id',primary_key=True)
    pr_avg = JSONField()
    pr_max = JSONField()
    pr_min = JSONField()
    ts_avg = JSONField()
    ts_max = JSONField()
    ts_min = JSONField()
    class Meta:
        db_table = 'rcp45_all_thai_2'

#--------------------------------------------------------------------------------------------------#
# ----------------- Cambodia -------------------#
class cambo_hist(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 'hist_cambo' #index in year 1970-2005 ind allind

class cambo_rcp85_t01(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 'rcp85_cambo_t01' #index in year 2006-2079 ind allind


class cambo_rcp85_t02(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 'rcp85_cambo_t02' #index in year 2080-2100 ind allind

class cambo_rcp45_t01(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 'rcp45_cambo_t01' #index in year 2006-2079 ind allind

class cambo_rcp45_t02(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 'rcp45_cambo_t02' #index in year 2080-2100 ind allind


class all_rcp45_cambo(models.Model):
    id = models.IntegerField(db_column='cal_id',primary_key=True)
    pr_avg = JSONField()
    pr_max = JSONField()
    pr_min = JSONField()
    ts_avg = JSONField()
    ts_max = JSONField()
    ts_min = JSONField()
    class Meta:
        db_table = 'rcp45_all_cambo' #cal max min avg for histrorical 1970-2005 ind all

class all_rcp85_cambo(models.Model):
    id = models.IntegerField(db_column='cal_id',primary_key=True)
    pr_avg = JSONField()
    pr_max = JSONField()
    pr_min = JSONField()
    ts_avg = JSONField()
    ts_max = JSONField()
    ts_min = JSONField()
    class Meta:
        db_table = 'rcp85_all_cambo' #cal max min avg for histrorical 1970-2100 ind all

# ----------------- Lao -------------------#
class lao_hist(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 'hist_lao' #index in year 1970-2005 ind allind

class lao_rcp85_t01(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 'rcp85_lao_t01' #index in year 2006-2079 ind allind


class lao_rcp85_t02(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 'rcp85_lao_t02' #index in year 2080-2100 ind allind

class lao_rcp45_t01(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 'rcp45_lao_t01' #index in year 2006-2079 ind allind

class lao_rcp45_t02(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 'rcp45_lao_t02' #index in year 2080-2100 ind allind


class all_rcp45_lao(models.Model):
    id = models.IntegerField(db_column='cal_id',primary_key=True)
    pr_avg = JSONField()
    pr_max = JSONField()
    pr_min = JSONField()
    ts_avg = JSONField()
    ts_max = JSONField()
    ts_min = JSONField()
    class Meta:
        db_table = 'rcp45_all_lao' #cal max min avg for histrorical 1970-2005 ind all

class all_rcp85_lao(models.Model):
    id = models.IntegerField(db_column='cal_id',primary_key=True)
    pr_avg = JSONField()
    pr_max = JSONField()
    pr_min = JSONField()
    ts_avg = JSONField()
    ts_max = JSONField()
    ts_min = JSONField()
    class Meta:
        db_table = 'rcp85_all_lao' #cal max min avg for histrorical 1970-2100 ind all

# ----------------- Vietn -------------------#
class vietn_hist(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 'hist_vietn' #index in year 1970-2005 ind allind

class vietn_rcp85_t01(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 'rcp85_vietn_t01' #index in year 2006-2079 ind allind


class vietn_rcp85_t02(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 'rcp85_vietn_t02' #index in year 2080-2100 ind allind

class vietn_rcp45_t01(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 'rcp45_vietn_t01' #index in year 2006-2079 ind allind

class vietn_rcp45_t02(models.Model):
    id = models.IntegerField(db_column='grid_id',primary_key=True)
    index_su = JSONField()
    index_fd = JSONField()
    index_id = JSONField()
    index_tr = JSONField()
    index_gsl = JSONField()
    index_dtr = JSONField()
    index_txx = JSONField()
    index_tnx = JSONField()
    index_txn = JSONField()
    index_tnn = JSONField()
    index_rx1day = JSONField()
    index_rx5day = JSONField()
    index_sdii = JSONField()
    index_r10mm = JSONField()
    index_r20mm = JSONField()
    index_rnnmm = JSONField()
    index_cdd = JSONField()
    index_cwd = JSONField()
    class Meta:
        db_table = 'rcp45_vietn_t02' #index in year 2080-2100 ind allind


class all_rcp45_vietn(models.Model):
    id = models.IntegerField(db_column='cal_id',primary_key=True)
    pr_avg = JSONField()
    pr_max = JSONField()
    pr_min = JSONField()
    ts_avg = JSONField()
    ts_max = JSONField()
    ts_min = JSONField()
    class Meta:
        db_table = 'rcp45_all_vietn' #cal max min avg for histrorical 1970-2005 ind all

class all_rcp85_vietn(models.Model):
    id = models.IntegerField(db_column='cal_id',primary_key=True)
    pr_avg = JSONField()
    pr_max = JSONField()
    pr_min = JSONField()
    ts_avg = JSONField()
    ts_max = JSONField()
    ts_min = JSONField()
    class Meta:
        db_table = 'rcp85_all_vietn' #cal max min avg for histrorical 1970-2100 ind all

