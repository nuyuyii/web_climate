from decimal import *
import csv
import json
import numpy as np
import datetime as dt
from climate.models import t9_ms_rcp85,t9_ms_rcp45,t9_ms_hist,t9_indx_rcp85,t9_indx_rcp45,t9_indx_hist
from climate.models import t4_ms_rcp85,t4_ms_rcp45,t4_ms_hist,t4_indx_rcp85,t4_indx_rcp45,t4_indx_hist
from climate.models import ms_rcp85,ms_rcp45,ms_hist,indx_hist,indx_rpc45,indx_rpc85

from climate.models import t_compare

from django.conf import settings
import os

class Indx_map(object):
    def __init__(self,gd_size):
        self.gd_size = int(gd_size)
        self.index_json = self.file_grid()        
        #super(Indexs, self).__init__()

    def file_grid(self):
        files = ["geoG9Cliamte", "geoG4Cliamte", "geoG1Cliamte"]
        urls = os.path.join(settings.BASE_DIR,'climate/static/data/'+files[self.gd_size]+'.json')

        with open(urls) as data_file:    
            index_json = json.load(data_file)            

        return index_json    


    def check_indx(self, clim_index,time_start,time_end,rcp,s_table):
        var = self.index_json['features']
        #  print(self.index_json['features'][1]['geometry']['coordinates'])
        indxValue = self.check_time(time_start,time_end,rcp,s_table)
        listYear = range(time_start,time_end)
#        eyear=[format(x,'04d') for x in listYear]
#        indxValue = {}
        #indxCal = []

#        if (self.gd_size==0):
#            for y in listYear: 
#                if (y<2006):           
#                    indxValue[str(y)] = t9_ms_hist.objects.get(year=y)
#                elif (rcp=="45"):
#                    indxValue[str(y)] = t9_ms_rcp45.objects.get(year=y)
#                elif (rcp=="85"):
#                    indxValue[str(y)] = t9_ms_rcp85.objects.get(year=y)
        #    indxValue = t9_ms_hist.objects.get(year=time_start)
        #elif (self.gd_size==1):
        #    indxValue = t9_ms_hist.objects.get(year__in=(time_start))
#        elif (self.gd_size==2):
#            for y in listYear: 
#                if (y<2006):           
#                    indxValue[str(y)] = ms_hist.objects.get(year=y)
#                elif (rcp=="45"):
#                    indxValue[str(y)] = ms_rcp45.objects.get(year=y)
#                elif (rcp=="85"):
#                    indxValue[str(y)] = ms_rcp85.objects.get(year=y)

        temFt = []  
        for i in range(0,len(var)):
            indxCal = []
            for y in listYear:
                if (clim_index=="max1"): 
                    indxCal.append(indxValue[str(y)].ts_max[i])
                elif (clim_index=="min1"): 
                    indxCal.append(indxValue[str(y)].ts_min[i])
                elif (clim_index=="avg1"): 
                    indxCal.append(indxValue[str(y)].ts_avg[i])
                elif (clim_index=="max2"): 
                    indxCal.append(indxValue[str(y)].pr_max[i])
                elif (clim_index=="min2"): 
                    indxCal.append(indxValue[str(y)].pr_min[i])
                elif (clim_index=="avg2"): 
                    indxCal.append(indxValue[str(y)].pr_avg[i])
                elif (clim_index=="su1"): 
                    indxCal.append(indxValue[str(y)].index_su[i])
                elif (clim_index=="fd1"): 
                    indxCal.append(indxValue[str(y)].index_fd[i])
                elif (clim_index=="id1"): 
                    indxCal.append(indxValue[str(y)].index_id[i])
                elif (clim_index=="tr1"): 
                    indxCal.append(indxValue[str(y)].index_tr[i])
                elif (clim_index=="gsl1"): 
                    indxCal.append(indxValue[str(y)].index_gsl[i])
                elif (clim_index=="dtr1"): 
                    indxCal.append(indxValue[str(y)].index_dtr[i])
                elif (clim_index=="txx1"): 
                    indxCal.append(indxValue[str(y)].index_txx[i])
                elif (clim_index=="tnx1"): 
                    indxCal.append(indxValue[str(y)].index_tnx[i])
                elif (clim_index=="txn1"): 
                    indxCal.append(indxValue[str(y)].index_txn[i])
                elif (clim_index=="tnn1"): 
                    indxCal.append(indxValue[str(y)].index_tnn[i])
                elif (clim_index=="rx1day2"): 
                    indxCal.append(indxValue[str(y)].index_rx1day[i])
                elif (clim_index=="rx5day2"): 
                    indxCal.append(indxValue[str(y)].index_rx5day[i])
                elif (clim_index=="sdii2"): 
                    indxCal.append(indxValue[str(y)].index_sdii[i])
                elif (clim_index=="r10mm2"): 
                    indxCal.append(indxValue[str(y)].index_r10mm[i])
                elif (clim_index=="rnnmm2"): 
                    indxCal.append(indxValue[str(y)].index_rnnmm[i])
                elif (clim_index=="cdd2"): 
                    indxCal.append(indxValue[str(y)].index_cdd[i])
                elif (clim_index=="cwd2"): 
                    indxCal.append(indxValue[str(y)].index_cwd[i])
                elif (clim_index=="su1"): 
                    indxCal.append(indxValue[str(y)].index_cwd[i])
                elif (clim_index=="su1"): 
                    indxCal.append(indxValue[str(y)].index_su[i])
                else: 
                    print("clim_index",clim_index)
                    indxCal.append(indxValue[str(y)].index_su[i])



            Value = float("%.4f" % np.average(indxCal) )        
            temFt.append(Value)
            #elif (clim_index=="ts_min"):
            #    temFt.append(indxValue.ts_min[i])
            #elif (clim_index=="ts_avg"):
            #    temFt.append(indxValue.ts_avg[i])
            #elif (clim_index=="pr_max"):
            #    temFt.append(indxValue.ts_avg[i])

            #elif (clim_index=="pr_min"):
            #    temFt.append(indxValue.ts_avg[i])
            #elif (clim_index=="pr_avg"):
            #    temFt.append(indxValue.ts_avg[i])

        cnt_gd = 0
        for gd in var:
            gd['properties']['value'] = temFt[cnt_gd]
            cnt_gd = cnt_gd + 1
        maxValue = np.max(temFt)
        minValue = np.min(temFt)
        return self.index_json,maxValue,minValue

    def check_time(self,time_start,time_end,rcp,s_table):
        print("check: ",time_start,' - ',time_end,' rcp: ',rcp,' table: ',s_table)
        listYear = range(time_start,time_end)
        indxValue = {}
        if (s_table=="ms"):
            if (self.gd_size==0):
                for y in listYear: 
                    if (y<2006):           
                        indxValue[str(y)] = t9_ms_hist.objects.get(year=y)
                    elif (rcp=="45"):
                        indxValue[str(y)] = t9_ms_rcp45.objects.get(year=y)
                    elif (rcp=="85"):
                        indxValue[str(y)] = t9_ms_rcp85.objects.get(year=y)
            elif (self.gd_size==1):
                for y in listYear: 
                    if (y<2006):           
                        indxValue[str(y)] = t4_ms_hist.objects.get(year=y)
                    elif (rcp=="45"):
                        indxValue[str(y)] = t4_ms_rcp45.objects.get(year=y)
                    elif (rcp=="85"):
                        indxValue[str(y)] = t4_ms_rcp85.objects.get(year=y)
        #    indxValue = t9_ms_hist.objects.get(year=time_start)
        #elif (self.gd_size==1):
        #    indxValue = t9_ms_hist.objects.get(year__in=(time_start))
            elif (self.gd_size==2):
                for y in listYear: 
                    if (y<2006):           
                        indxValue[str(y)] = ms_hist.objects.get(year=y)
                    elif (rcp=="45"):
                        indxValue[str(y)] = ms_rcp45.objects.get(year=y)
                    elif (rcp=="85"):
                        indxValue[str(y)] = ms_rcp85.objects.get(year=y)
            return indxValue
        elif (s_table=="indx"):
            if (self.gd_size==0):
                for y in listYear: 
                    if (y<2006):           
                        indxValue[str(y)] = t9_indx_hist.objects.get(year=y)
                    elif (rcp=="45"):
                        indxValue[str(y)] = t9_indx_rcp45.objects.get(year=y)
                    elif (rcp=="85"):
                        indxValue[str(y)] = t9_indx_rcp85.objects.get(year=y)
            elif (self.gd_size==1):
                for y in listYear: 
                    if (y<2006):           
                        indxValue[str(y)] = t4_indx_hist.objects.get(year=y)
                    elif (rcp=="45"):
                        indxValue[str(y)] = t4_indx_rcp45.objects.get(year=y)
                    elif (rcp=="85"):
                        indxValue[str(y)] = t4_indx_rcp85.objects.get(year=y)
            elif (self.gd_size==2):
                for y in listYear: 
                    if (y<2006):           
                        indxValue[str(y)] = indx_hist.objects.get(year=y)
                    elif (rcp=="45"):
                        indxValue[str(y)] = indx_rpc45.objects.get(year=y)
                    elif (rcp=="85"):
                        indxValue[str(y)] = indx_rpc85.objects.get(year=y)
            return indxValue

class Compare_map(object):

    def file_grid(self, gd_size):
        files = ["geoG9Cliamte", "geoG4Cliamte", "geoG1Cliamte"]
        urls = os.path.join(settings.BASE_DIR,'climate/static/data/'+files[gd_size]+'.json')
        with open(urls) as data_file:    
            index_json = json.load(data_file)          
        return index_json    

    def call_value(self, clim_index, size):  
        gd_size = int(size)      
        indxValue = self.check_files(gd_size)

        json_hist = self.check_inx(clim_index, gd_size, indxValue["hist"])
        json_rcp45 = self.check_inx(clim_index, gd_size, indxValue["rcp45"])
        json_rcp85 = self.check_inx(clim_index, gd_size, indxValue["rcp85"])

        # print("json: ",clim_index," : ",json_hist['features'][0]['properties']['value'])

        return json_hist,json_rcp45,json_rcp85

    def check_inx(self, clim_index, gd_size, indxValue):
        index_json = self.file_grid(gd_size)
        var = index_json['features']
        indxCal = []
        if (clim_index=="max1"): 
            indxCal = indxValue.ts_max
        elif (clim_index=="min1"): 
            indxCal = indxValue.ts_min
        elif (clim_index=="avg1"): 
            indxCal = indxValue.ts_avg
        elif (clim_index=="max2"): 
            indxCal = indxValue.pr_max
        elif (clim_index=="min2"): 
            indxCal = indxValue.pr_min
        elif (clim_index=="avg2"): 
            indxCal = indxValue.pr_avg

        cnt_gd = 0
        for gd in var:
            gd['properties']['value'] = indxCal[cnt_gd]
            cnt_gd = cnt_gd + 1

        maxValue = np.max(indxCal)
        minValue = np.min(indxCal)
        
        return index_json

    def check_files(self, gd_size):
        indxValue = {}
        if (gd_size==0):
            indxValue["hist"] = t_compare.objects.get(g_size="3x3", ipsl_file="hist")
            indxValue["rcp45"] = t_compare.objects.get(g_size="3x3", ipsl_file="rcp45")
            indxValue["rcp85"] = t_compare.objects.get(g_size="3x3", ipsl_file="rcp85")
        elif (gd_size==1):
            indxValue["hist"] = t_compare.objects.get(g_size="2x2", ipsl_file="hist")
            indxValue["rcp45"] = t_compare.objects.get(g_size="2x2", ipsl_file="rcp45")
            indxValue["rcp85"] = t_compare.objects.get(g_size="2x2", ipsl_file="rcp85")
        elif (gd_size==2):
            indxValue["hist"] = t_compare.objects.get(g_size="1x1", ipsl_file="hist")
            indxValue["rcp45"] = t_compare.objects.get(g_size="1x1", ipsl_file="rcp45")
            indxValue["rcp85"] = t_compare.objects.get(g_size="1x1", ipsl_file="rcp85")
        return indxValue
