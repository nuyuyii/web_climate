import json
import os
import numpy as np
import math

from netCDF4 import Dataset, MFDataset
import datetime as dt 
from decimal import *
#-------------------Database Psql----------------------
import random, string, psycopg2,time
from string import ascii_lowercase
from random import randint



class climatecal(object):

    def __init__(self, stryear,endyear):
        self.stryear = stryear
        self.endyear = endyear
        self.lisYear = range(self.stryear,self.endyear)
        self.eyear = [format(x,'04d') for x in self.lisYear]
        #self.var_pr, self.var_tsmax, self.var_tsmin = self.read_nc()

    def read_nc(self):
        print("Reading data from a multi-file NetCDF dataset")
        var_pr = {}
        var_tsmax = {}
        var_tsmin = {}
        for y in self.eyear: #self.stryear,self.endyear):
            nc_file = MFDataset("/media/nuyuyii/DATA/seaclid/IPSL/RCP45/ipsl_rcp45_dd_T2m_Pr."+y+"*nc")
            var_pr[y] = nc_file.variables['pr']
            var_tsmax[y] = nc_file.variables['tasmax']
            var_tsmin[y] = nc_file.variables['tasmin']
        # print("Time:",len(nc_file.dimensions['time']))
        return var_pr, var_tsmax, var_tsmin

    def val_ltln(self, lat, lon):
        cut_sol1 = []
        cut_sol2 = []
        cut_sol3 = []

        for y in self.eyear:
            sol1 = self.var_pr[y][:,lat,lon]
            sol2 = self.var_tsmax[y][:,lat,lon]
            sol3 = self.var_tsmin[y][:,lat,lon]
            for z in range(0,len(sol1)):
                cut_sol1.append(sol1[z]*86400)
                cut_sol2.append(sol2[z]-273.15)
                cut_sol3.append(sol3[z]-273.15)

        #print("Time:",len(cut_sol1))
        #c = np.array(cut_sol)
        cut_sol1 = [float("%.2f" % e) for e in cut_sol1]
        cut_sol2 = [float("%.2f" % e) for e in cut_sol2]
        cut_sol3 = [float("%.2f" % e) for e in cut_sol3]
        return cut_sol1, cut_sol2, cut_sol3 

    def values(self, lat, lon):
        cut_sol1 = []
        cut_sol2 = []
        cut_sol3 = []
        for y in self.eyear: #self.stryear,self.endyear):
            # print("Reading data from a multi-file NetCDF dataset: %s " %y)
            nc_file = MFDataset("/media/nuyuyii/DATA/seaclid/IPSL/RCP45/ipsl_rcp45_dd_T2m_Pr."+y+"*nc")
            val_pr = nc_file.variables['pr']
            val_tsmax = nc_file.variables['tasmax']
            val_tsmin = nc_file.variables['tasmin']
            sol1 = val_pr[:,lat,lon]
            sol2 = val_tsmax[:,lat,lon]
            sol3 = val_tsmin[:,lat,lon]
            for z in range(0,len(sol1)):
                cut_sol1.append(sol1[z]*86400)
                cut_sol2.append(sol2[z]-273.15)
                cut_sol3.append(sol3[z]-273.15)

        print("Time:",len(cut_sol1))
        #c = np.array(cut_sol)
        cut_sol1 = [float("%.2f" % e) for e in cut_sol1]
        cut_sol2 = [float("%.2f" % e) for e in cut_sol2]
        cut_sol3 = [float("%.2f" % e) for e in cut_sol3]
        return cut_sol1, cut_sol2, cut_sol3 

    def LtLn(self):
        m = Dataset('/media/nuyuyii/DATA/seaclid/IPSL/RCP45/ipsl_rcp45_dd_T2m_Pr.200601.nc', 'r')
        lons = m.variables['lon']
        lats = m.variables['lat']
        return lons, lats

    def measure(self, lat, lon):
        cut_sol1 = []
        cut_sol2 = []
        cut_sol3 = []

        cut_pr = []
        cut_prmax = []
        cut_prmin = []
        cut_ts = []
        cut_tsmax = []
        cut_tsmin = []
        for y in self.eyear: #self.stryear,self.endyear):
            # print("Reading data from a multi-file NetCDF dataset: %s " %y)
            nc_file = MFDataset("/media/nuyuyii/DATA/seaclid/IPSL/RCP45/ipsl_rcp45_dd_T2m_Pr."+y+"*nc")
            val_pr = nc_file.variables['pr']
            val_ts = nc_file.variables['tas']
            val_tsmax = nc_file.variables['tasmax']
            val_tsmin = nc_file.variables['tasmin']


            sol0 = val_pr[:,lat,lon]
            sol1 = val_ts[:,lat,lon]
            sol2 = val_tsmax[:,lat,lon]
            sol3 = val_tsmin[:,lat,lon]

            #cut_pr.append( np.average(sol0)*86400 )
            #cut_prmax.append(np.max(sol0)*86400)
            #cut_prmin.append(np.min(sol0)*86400)
            #cut_ts.append(np.average(sol1)-273.15)
            #cut_tsmax.append(np.max(sol2)-273.15)
            #cut_tsmin.append(np.min(sol3)-273.15)

            fpr = float("%.2f" % (np.average(sol0)*86400)) 
            fprmax = float("%.2f" % (np.max(sol0)*86400)) 
            fprmin = float("%.2f" % (np.min(sol0)*86400)) 

            fts = float("%.2f" % (np.average(sol1)-273.15)) 
            ftsmax = float("%.2f" % (np.max(sol2)-273.15)) 
            ftsmin = float("%.2f" % (np.min(sol3)-273.15)) 


            cut_pr.append(fpr)
            cut_prmax.append(fprmax)
            cut_prmin.append(fprmin)
            cut_ts.append(fts)
            cut_tsmax.append(ftsmax)
            cut_tsmin.append(ftsmin)
        #cut_pr = [float("%.2f" % e) for e in cut_pr]
        #cut_prmax = [float("%.2f" % e) for e in cut_prmax]
        #cut_prmin = [float("%.2f" % e) for e in cut_prmin]
        #cut_ts = [float("%.2f" % e) for e in cut_ts]
        #cut_tsmax = [float("%.2f" % e) for e in cut_tsmax]
        #cut_tsmin = [float("%.2f" % e) for e in cut_tsmin]

           # print("cut_pr:",cut_pr," --cut_prmax:",cut_prmax," --cut_prmin:",cut_prmin)
           # print("cut_ts:",cut_ts," --cut_tsmax:",cut_tsmax," --cut_tsmin:",cut_tsmin)
        #print("Time:",len(cut_pr))
        return cut_pr, cut_prmax, cut_prmin, cut_ts,  cut_tsmax, cut_tsmin

# ---- Global Attribute ----- #
start_time = time.time()
eindex=["ts_avg","ts_max","ts_min","pr_avg","pr_max","pr_min"]
lisYear=range(2006,2101)
eyear=[format(x,'04d') for x in lisYear]
#eyear=["1970","1971"]
#print('"',eyear[0],'"',u[1])
year=[2006,2101]
str_date = "2005-12-31"

#print(year[0],year[1], len(eindex))


clim = climatecal(year[0],year[1])
lons, lats = clim.LtLn()
# print("pr:%s" % pr, "\nmax:%s" % tmax, "\nmin:%s" % tmin)

# ------------------ calInsert -----------------------#
# ----------------------------------------------------#

def calInsert():
    sql1 ="""INSERT INTO t08measure_rpc45
         VALUES (%s, %s, %s, %s, %s, %s, %s)"""
    try:
        conn=psycopg2.connect("dbname='db_ipsl' user='projectuser' password='ngaeprom7861'")
        cur = conn.cursor()
        ind = 1
        for x in range(0,191):#len(lats)):
            feature=[]
            # Note 21499 cdd Na Lat 191 : Lon 253
            for y in range(0,253):#len(lons)):
                cut_pr, cut_prmax, cut_prmin, cut_ts,  cut_tsmax, cut_tsmin = clim.measure(x,y)
                value={}
                value['grid_id'] = ind
                for i in range(0,len(eindex)):
                    value[eindex[i]]={}

                for ey in range(0,len(eyear)):
                    value[eindex[0]][eyear[ey]] = cut_ts[ey]
                    value[eindex[1]][eyear[ey]] = cut_tsmax[ey]
                    value[eindex[2]][eyear[ey]] = cut_tsmin[ey]
                    value[eindex[3]][eyear[ey]] = cut_pr[ey]
                    value[eindex[4]][eyear[ey]] = cut_prmax[ey]
                    value[eindex[5]][eyear[ey]] = cut_prmin[ey]

                for i in range(0,len(eindex)):
                    value[eindex[i]] = json.dumps(value[eindex[i]])


                feature.append(value)
                ind=ind+1
  
            print(lats[x],'--',lons[y],";x ",x,"-y ",y,";ind ",ind-1)

            for fea in feature:            
                cur.execute(sql1,(fea['grid_id'], fea[eindex[0]], fea[eindex[1]], fea[eindex[2]], fea[eindex[3]], fea[eindex[4]],
                                  fea[eindex[5]]))
            
            conn.commit() # commit 
            #print(feature[0]['pr_avg'])
            #print(fea['index_tnn'])
            #print(ind)

        cur.close() # close communication with thte PostgreSQL database server
    #except Exception as e:
    #    print ("I am unable to connect to the database.")

    finally:
        if conn is not None:
            conn.close()

# ------------------ calIn()  ------------------------#
# ----------------------------------------------------#
def calIn(ind,x):
    feature=[]
  # Note 21499 cdd Na Lat 191 : Lon 253
    for y in range(0,253):#len(lons)):
        cut_pr, cut_prmax, cut_prmin, cut_ts,  cut_tsmax, cut_tsmin = clim.measure(x,y)
        value={}
        value['grid_id'] = ind
            #print (value[eindex[0]])

         
            #print (str(value[eindex[0]][0]))
        for i in range(0,len(eindex)):
           value[eindex[i]]={}

        for ey in range(0,len(eyear)):
           value[eindex[0]][eyear[ey]] = cut_ts[ey]
           value[eindex[1]][eyear[ey]] = cut_tsmax[ey]
           value[eindex[2]][eyear[ey]] = cut_tsmin[ey]
           value[eindex[3]][eyear[ey]] = cut_pr[ey]
           value[eindex[4]][eyear[ey]] = cut_prmax[ey]
           value[eindex[5]][eyear[ey]] = cut_prmin[ey]

        for i in range(0,len(eindex)):
           value[eindex[i]] = json.dumps(value[eindex[i]])


        feature.append(value)
        ind=ind+1

    print(lats[x],'--',lons[y],";x ",x,"-y ",y,";ind ",ind-1)
    return ind, feature

if __name__ == '__main__':
    calInsert()

print('---%s seconds---'%(time.time()-start_time))
