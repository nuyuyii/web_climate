#from climate.models import Grid
import json
import numpy as np

from netCDF4 import Dataset
import datetime as dt 
from decimal import *
#-------------------Database Psql----------------------
import random, string, psycopg2,time
from string import ascii_lowercase
from random import randint


# load the psycopg extras module
import psycopg2.extras

start_time = time.time()


cnt=1
Sl_lisYear=range(1970,2101)
Sl_eyear=[format(x,'04d') for x in Sl_lisYear]

H_lisYear=range(1970,2006)
H_eyear=[format(x,'04d') for x in H_lisYear]

F1_lisYear=range(2006,2080)
F1_eyear=[format(x,'04d') for x in F1_lisYear]

F2_lisYear=range(2080,2101)
F2_eyear=[format(x,'04d') for x in F2_lisYear]

eindex=["index_su","index_fd","index_id",
    "index_tr","index_gsl","index_dtr",
    "index_txx","index_tnx","index_txn",
    "index_tnn","index_rx1day","index_rx5day",
    "index_sdii","index_r10mm","index_r20mm",
    "index_rnnmm","index_cdd","index_cwd"]

sqlInsert = """INSERT INTO base130_rpc85
         VALUES (%s, %s, %s, %s, %s, %s, %s, %s,
          %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""

try:
    conn = psycopg2.connect("dbname='db_ipsl' user='projectuser' password='ngaeprom7861'")
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cur.execute("SELECT table_name FROM information_schema.tables WHERE table_schema = 'public' ORDER BY table_name ASC;")
    tablename=cur.fetchall()
    sqlH = "SELECT * from t04historical WHERE grid_id={myind}"
    F1_sql = "SELECT * from t02future_rpc85 WHERE grid_id={myind}"
    F2_sql = "SELECT * from ipsl85_30 WHERE grid_id={myind}"
    
    cal_130 = []
    for ind in range(1,48324): #48323
        #var='25300'
        value = {}
        value['grid_id'] = ind

        cur.execute(sqlH.format(myind=str(ind)))
        H_rows = cur.fetchall()
        cur.execute(F1_sql.format(myind=str(ind)))
        F1_rows = cur.fetchall()
        cur.execute(F2_sql.format(myind=str(ind)))
        F2_rows = cur.fetchall()

        for i in range(0,len(eindex)):
           value[eindex[i]]={}

        for ey in range(0,len(Sl_eyear)):
            for inx in range(0,len(eindex)):
                syear = int(Sl_eyear[ey])
                if syear<2006:
                    #print(H_rows[eindex[inx]]['1970'])
                    for row in H_rows:
                        value[eindex[inx]][Sl_eyear[ey]] = row[eindex[inx]][Sl_eyear[ey]]                
                elif syear<2080:
                    for row in F1_rows:
                        value[eindex[inx]][Sl_eyear[ey]] = row[eindex[inx]][Sl_eyear[ey]]
                elif (syear==2084 or syear==2085 ):
                    for row in F1_rows:
                        value[eindex[inx]][Sl_eyear[ey]] = row[eindex[inx]][Sl_eyear[ey]]
                elif syear<2101:
                    for row in F2_rows:
                        value[eindex[inx]][Sl_eyear[ey]] = row[eindex[inx]][Sl_eyear[ey]]

        # print(value[eindex[0]]['1970'])   

        for i in range(0,len(eindex)):
            value[eindex[i]] = json.dumps(value[eindex[i]])        

        cur.execute(sqlInsert,(value['grid_id'],  value[eindex[0]], value[eindex[1]],
                          value[eindex[2]], value[eindex[3]], value[eindex[4]],
                          value[eindex[5]], value[eindex[6]],
                          value[eindex[7]], value[eindex[8]], value[eindex[9]], value[eindex[10]],
                          value[eindex[11]], value[eindex[12]], value[eindex[13]], value[eindex[14]],
                          value[eindex[15]], value[eindex[16]], value[eindex[17]] ))
        cnt = cnt+1
        if (ind%253==0):
            print('ind: ',ind)
            conn.commit()
#        for row in H_rows:
#            print ('----------------')
#            for year in H_eyear:
#                print (year,'--',row[eindex[0]][year])



    for x in tablename:
        for y in x:
            if (y=='t04historical'):
#cur.execute("SELECT column1, column2 FROM {mytable} ORDER BY column1 ASC". format(mytable=y))
#mytest = cur.fetchall()
                print (y)
                #cur.execute(sqlH,var)
                #rows = cur.fetchall()
                """for row in rows:
                    print('--',row[eindex[0]][eyear[0]])
                for row in rowsd:
                    print(row[eindex[0]]['2006'])"""

            if (y=='t02future_rpc85'):
                print (y)
                #cur.execute(sqlIPSL85,var)
                #rowsd = cur.fetchall()
                
                
except  (psycopg2.DatabaseError, e):
    print ('Error %s' % e  )



conn.close()
print('---%s seconds---'%(time.time()-start_time))
