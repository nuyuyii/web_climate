#from climate.models import Grid
import json
import numpy as np

from netCDF4 import Dataset
import datetime as dt 
from decimal import *
#-------------------Database Psql----------------------
import random, string, psycopg2,time
from string import ascii_lowercase
from random import randint


# load the psycopg extras module
import psycopg2.extras

# dbproject -- ClimePro
# dbclimate --Climproject
# climatedb -- webCli

start_time = time.time()
#m = Dataset('/media/nuyuyii/DATA/CSIRO_MK/RCP45/csiromk36-rcp45-dd-pr-tas-t2m.200701.nc', 'r')
#lon = m.variables['lon']
#lat = m.variables['lat']
# pg_dump -d  climatedb -a -t climate_grid > grid.sql
# psql --set ON_ERROR_STOP=on dbproject < grid.sql
try:
    conn = psycopg2.connect("dbname='db_ipsl' user='projectuser' password='ngaeprom7861'")
except:
    print ("I am unable to connect to the database.")

cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
try:
    cur.execute("""SELECT * from t08measure_rpc45""")
except:
    print ("I can't SELECT t08measure_rpc45")

feature=[]
rows = cur.fetchall()
cnt=1

lisYear=range(2006,2101)
eyear=[format(x,'04d') for x in lisYear]

eindex=["ts_avg","ts_max","ts_min","pr_avg","pr_max","pr_min"]


sql1 ="""INSERT INTO t11measure_rpc45
         VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"""

for year in lisYear:
    va={}

    for i in range(0,len(eindex)):
        va[eindex[i]]=[]

    for row in rows:
        va['years_id'] = cnt
        va['years'] = year
        for i in range(0,len(eindex)):
            va[eindex[i]].append(row[eindex[i]][eyear[cnt-1]])
    #print(va['years'] , va['ts_avg'],len(va['ts_avg']))
    #print(va['years'] , va['ts_max'],len(va['ts_avg']))
    #va['ts_max'] = row['climdex'][ystr]
    #va['ts_min'] = row['climdex'][ystr]
    #va['pr_avg'] = row['climdex'][ystr]
    #va['pr_max'] = row['climdex'][ystr]
    #va['pr_min'] = row['climdex'][ystr]
    #feature.append(va)



    cur.execute(sql1,(va['years_id'], va['years'], str(va[eindex[0]]), str(va[eindex[1]]), str(va[eindex[2]]), str(va[eindex[3]]), str(va[eindex[4]]), str(va[eindex[5]])))
    conn.commit()

    print ("insert OK : year",year," id:",cnt)
    cnt=cnt+1

#select grid_id,ts_avg  from t08measure_rpc45 where ts_avg #> '{2006}'='20';
#print(va['ts_avg'],len(va['ts_avg']))
#print(feature[0]['years'],feature[0]['ts_avg'])
#print(feature[1]['years'],feature[1]['ts_avg'])
conn.close()
print('---%s seconds---'%(time.time()-start_time))
