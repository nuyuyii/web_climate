# load the adapter
import psycopg2

# load the psycopg extras module
import psycopg2.extras

import json
# Try to connect


def create_table(sqls):
    conn = None
    try:
        conn = psycopg2.connect("dbname='db_ipsl' user='projectuser' password='ngaeprom7861'")
        cur = conn.cursor()

		# create table one 
        for sql in sqls:
            cur.execute(sql)
            print('create Table',sql)
		# close communication with thte PostgreSQL database server
        cur.close()
		# commit 
        conn.commit()

    #except Exception as e:
    #    print ("I am unable to connect to the database.")

    finally:
        if conn is not None:
            conn.close()

sqls = (
    """
    CREATE TABLE t9_grid_3x3 (
        grid_id INTEGER PRIMARY KEY,
        point jsonb,
        coordinates jsonb
    );
    """,
    """
    CREATE TABLE t9_index_rcp85 (
        year_id INTEGER PRIMARY KEY,
        year integer NOT NULL,
        index_su jsonb,
        index_fd jsonb,
        index_id jsonb,
        index_tr jsonb,
        index_gsl jsonb,
        index_dtr jsonb,
        index_txx jsonb,
        index_tnx jsonb,
        index_txn jsonb,
        index_tnn jsonb,
        index_rx1day jsonb,
        index_rx5day jsonb,
        index_sdii jsonb,
        index_r10mm jsonb,
        index_r20mm jsonb,
        index_rnnmm jsonb,
        index_cdd jsonb,
        index_cwd jsonb
    );
    """,
    """
    CREATE TABLE t9_index_rcp45 (
        year_id INTEGER PRIMARY KEY,
        year integer NOT NULL,
        index_su jsonb,
        index_fd jsonb,
        index_id jsonb,
        index_tr jsonb,
        index_gsl jsonb,
        index_dtr jsonb,
        index_txx jsonb,
        index_tnx jsonb,
        index_txn jsonb,
        index_tnn jsonb,
        index_rx1day jsonb,
        index_rx5day jsonb,
        index_sdii jsonb,
        index_r10mm jsonb,
        index_r20mm jsonb,
        index_rnnmm jsonb,
        index_cdd jsonb,
        index_cwd jsonb
    );
    """,
    """
    CREATE TABLE t9_index_hist (
        year_id INTEGER PRIMARY KEY,
        year integer NOT NULL,
        index_su jsonb,
        index_fd jsonb,
        index_id jsonb,
        index_tr jsonb,
        index_gsl jsonb,
        index_dtr jsonb,
        index_txx jsonb,
        index_tnx jsonb,
        index_txn jsonb,
        index_tnn jsonb,
        index_rx1day jsonb,
        index_rx5day jsonb,
        index_sdii jsonb,
        index_r10mm jsonb,
        index_r20mm jsonb,
        index_rnnmm jsonb,
        index_cdd jsonb,
        index_cwd jsonb
    );
    """,
    """
    CREATE TABLE t9_measure_rcp85 (
        year_id INTEGER PRIMARY KEY,
        year integer NOT NULL,
        ts_avg jsonb,
        ts_max jsonb,
        ts_min jsonb,
        pr_avg jsonb,
        pr_max jsonb,
        pr_min jsonb
    );
    """,
    """
    CREATE TABLE t9_measure_rcp45 (
        year_id INTEGER PRIMARY KEY,
        year integer NOT NULL,
        ts_avg jsonb,
        ts_max jsonb,
        ts_min jsonb,
        pr_avg jsonb,
        pr_max jsonb,
        pr_min jsonb
    );
    """,
    """
    CREATE TABLE t9_measure_hist (
        year_id INTEGER PRIMARY KEY,
        year integer NOT NULL,
        ts_avg jsonb,
        ts_max jsonb,
        ts_min jsonb,
        pr_avg jsonb,
        pr_max jsonb,
        pr_min jsonb
    );
    """
    )

sqls4 = (
    """
    CREATE TABLE t4_index_rcp85 (
        year_id INTEGER PRIMARY KEY,
        year integer NOT NULL,
        index_su jsonb,
        index_fd jsonb,
        index_id jsonb,
        index_tr jsonb,
        index_gsl jsonb,
        index_dtr jsonb,
        index_txx jsonb,
        index_tnx jsonb,
        index_txn jsonb,
        index_tnn jsonb,
        index_rx1day jsonb,
        index_rx5day jsonb,
        index_sdii jsonb,
        index_r10mm jsonb,
        index_r20mm jsonb,
        index_rnnmm jsonb,
        index_cdd jsonb,
        index_cwd jsonb
    );
    """,
    """
    CREATE TABLE t4_index_rcp45 (
        year_id INTEGER PRIMARY KEY,
        year integer NOT NULL,
        index_su jsonb,
        index_fd jsonb,
        index_id jsonb,
        index_tr jsonb,
        index_gsl jsonb,
        index_dtr jsonb,
        index_txx jsonb,
        index_tnx jsonb,
        index_txn jsonb,
        index_tnn jsonb,
        index_rx1day jsonb,
        index_rx5day jsonb,
        index_sdii jsonb,
        index_r10mm jsonb,
        index_r20mm jsonb,
        index_rnnmm jsonb,
        index_cdd jsonb,
        index_cwd jsonb
    );
    """,
    """
    CREATE TABLE t4_index_hist (
        year_id INTEGER PRIMARY KEY,
        year integer NOT NULL,
        index_su jsonb,
        index_fd jsonb,
        index_id jsonb,
        index_tr jsonb,
        index_gsl jsonb,
        index_dtr jsonb,
        index_txx jsonb,
        index_tnx jsonb,
        index_txn jsonb,
        index_tnn jsonb,
        index_rx1day jsonb,
        index_rx5day jsonb,
        index_sdii jsonb,
        index_r10mm jsonb,
        index_r20mm jsonb,
        index_rnnmm jsonb,
        index_cdd jsonb,
        index_cwd jsonb
    );
    """,
    """
    CREATE TABLE t4_measure_rcp85 (
        year_id INTEGER PRIMARY KEY,
        year integer NOT NULL,
        ts_avg jsonb,
        ts_max jsonb,
        ts_min jsonb,
        pr_avg jsonb,
        pr_max jsonb,
        pr_min jsonb
    );
    """,
    """
    CREATE TABLE t4_measure_rcp45 (
        year_id INTEGER PRIMARY KEY,
        year integer NOT NULL,
        ts_avg jsonb,
        ts_max jsonb,
        ts_min jsonb,
        pr_avg jsonb,
        pr_max jsonb,
        pr_min jsonb
    );
    """,
    """
    CREATE TABLE t4_measure_hist (
        year_id INTEGER PRIMARY KEY,
        year integer NOT NULL,
        ts_avg jsonb,
        ts_max jsonb,
        ts_min jsonb,
        pr_avg jsonb,
        pr_max jsonb,
        pr_min jsonb
    );
    """
    )
sql130 = (
    """
    CREATE TABLE y130allGrid (
        year_id INTEGER PRIMARY KEY,
        ts_avg jsonb,
        ts_max jsonb,
        ts_min jsonb,
        pr_avg jsonb,
        pr_max jsonb,
        pr_min jsonb,
        g_size text,
        ipsl_file text
    );
    """
)
create_table(sqls4)
