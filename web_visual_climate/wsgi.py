"""
WSGI config for web_visual_climate project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/
"""

import os,sys

from django.core.wsgi import get_wsgi_application
sys.path.append('/home/ubuntu')
sys.path.append('/home/ubuntu/web_visual_climate')
sys.path.append('/home/ubuntu/web_visual_climate/climate')
sys.path.append('/home/ubuntu/web_visual_climate/web_visual_climate')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "web_visual_climate.settings")

application = get_wsgi_application()
