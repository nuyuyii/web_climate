#from climate.models import Grid
import json
import numpy as np

from netCDF4 import Dataset
import datetime as dt 
from decimal import *
#-------------------Database Psql----------------------
import random, string, psycopg2,time
from string import ascii_lowercase
from random import randint


# load the psycopg extras module
import psycopg2.extras

start_time = time.time()

# 1970 - 2006
# 2006 - 2101
lisYear=range(2006,2101)
eyear=[format(x,'04d') for x in lisYear]
eindex=["ts_avg","ts_max","ts_min","pr_avg","pr_max","pr_min"]

eindex2=["index_su","index_fd","index_id",
    "index_tr","index_gsl","index_dtr",
    "index_txx","index_tnx","index_txn",
    "index_tnn","index_rx1day","index_rx5day",
    "index_sdii","index_r10mm","index_r20mm",
    "index_rnnmm","index_cdd","index_cwd"]

sql1 ="""INSERT INTO t4_measure_rcp85
         VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"""

sql2 = """INSERT INTO t4_index_rcp85
         VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s,
          %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""


# t4_measure_hist   -- t1_measure_hist
# t4_measure_rcp45  -- t1_measure_rcp45
# t4_measure_rcp85  -- t1_measure_rcp85

# t4_index_hist  -- t1_index_hist
# t4_index_rcp45  -- t1_index_rcp45
# t4_index_rcp85  -- t1_index_rcp85

try:
    conn = psycopg2.connect("dbname='db_ipsl' user='projectuser' password='ngaeprom7861'")
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    sql_hist = "SELECT * from t1_index_rcp85 where year={myind}"

    cnt=1

    for year in lisYear:
        va={}

        cur.execute(sql_hist.format(myind=str(year)))
        rows = cur.fetchall()

        for i in range(0,len(eindex2)):
            va[eindex2[i]]=[]

        for row in rows:
            va['years_id'] = cnt
            va['years'] = year
            for i in range(0,191):
                for j in range(0,253):
                    if (i%2==0 and j%2==0): 
                        #print('---i:',i,'-----j:',j)
                        r_lon1 = (i*253)+j
                        r_lon2 = ((i+1)*253)+j
                        #print('---i:',i,'-----j:',j)
                        
                        for ind in range(0,len(eindex2)):
                            tmValue = []
                            if (i==0 and j<252):
                                tmValue.append(row[eindex2[ind]][r_lon1])
                                tmValue.append(row[eindex2[ind]][r_lon2])
                                tmValue.append(row[eindex2[ind]][r_lon1+1])
                                tmValue.append(row[eindex2[ind]][r_lon2+1])
                            elif (i==190 and j<252):  
                                tmValue.append(row[eindex2[ind]][r_lon1])
                                tmValue.append(row[eindex2[ind]][r_lon1+1])
                            elif (i==190 and j==252): 
                                tmValue.append(row[eindex2[ind]][r_lon1])
                            #elif (j==0): 
                            #    tmValue.append(row[eindex[ind]][r_lon1])
                            #    tmValue.append(row[eindex[ind]][r_lon2])
                            #    tmValue.append(row[eindex[ind]][r_lon1+1])
                            #    tmValue.append(row[eindex[ind]][r_lon2+1])
                            elif (j%252==0):
                                tmValue.append(row[eindex2[ind]][r_lon1])
                                tmValue.append(row[eindex2[ind]][r_lon2])
                            else:
                                tmValue.append(row[eindex2[ind]][r_lon1])
                                tmValue.append(row[eindex2[ind]][r_lon2])
                                tmValue.append(row[eindex2[ind]][r_lon1+1])
                                tmValue.append(row[eindex2[ind]][r_lon2+1])

                            #print("tmValue---",tmValue)
                            #if (ind==0 or ind==3):
                            #    Value = float("%.2f" % (np.average(tmValue)))
                            #elif (ind==1 or ind==4):
                            #	Value = float("%.2f" % (np.max(tmValue)))
                            #elif (ind==2 or ind==5):
                            #	Value = float("%.2f" % (np.min(tmValue)))
                            #print("VALUE------",Value)
                            Value = float("%.2f" % (np.average(tmValue)))
                            va[eindex2[ind]].append(Value)



            #print("LEN---",len(va[eindex[0]]))
            #for i in range(0,len(eindex)):
           #     va[eindex[i]].append(row[eindex[i]][eyear[cnt-1]])
        #cur.execute(sql1,(va['years_id'], va['years'], str(va[eindex[0]]), str(va[eindex[1]]), str(va[eindex[2]]), str(va[eindex[3]]), str(va[eindex[4]]), str(va[eindex[5]])))
        cur.execute(sql2,(va['years_id'], va['years'], str(va[eindex2[0]]), str(va[eindex2[1]]),
                      str(va[eindex2[2]]), str(va[eindex2[3]]), str(va[eindex2[4]]), str(va[eindex2[5]]), 
                      str(va[eindex2[6]]), str(va[eindex2[7]]), str(va[eindex2[8]]), str(va[eindex2[9]]),
                      str(va[eindex2[10]]), str(va[eindex2[11]]), str(va[eindex2[12]]),
                      str(va[eindex2[13]]), str(va[eindex2[14]]), str(va[eindex2[15]]),
                      str(va[eindex2[16]]), str(va[eindex2[17]]) ))

        conn.commit()

        print ("insert OK : year",year," id:",cnt)
        cnt=cnt+1

except (psycopg2.DatabaseError, e):
    print ("I am unable to connect to the database.")
    print ('Error %s' % e )

