#from climate.models import Grid
import json
import numpy as np

from netCDF4 import Dataset
import datetime as dt 
from decimal import *
#-------------------Database Psql----------------------
import random, string, psycopg2,time
from string import ascii_lowercase
from random import randint

#-------------------import R---------------------------
import math, datetime
import rpy2.robjects as ro
from rpy2.robjects.packages import importr

start_time = time.time()
m = Dataset('/media/nuyuyii/DATA/CSIRO_MK/RCP45/csiromk36-rcp45-dd-pr-tas-t2m.200701.nc', 'r')
lon = m.variables['lon']
lat = m.variables['lat']

coord = []
grid = []

# -------------------------------------------------------
# Creat grid and make grid map point, ts and pr
for i in range(0,len(lat)+1):
    for j in range(0,len(lon)+1):
        if (i==0 and j==0):  # begin lon at lat=0
            grid.append([lon[j]-0.11,lat[i]-0.11])
        elif (i==0 and j<len(lon)):
            grid.append([(lon[j-1]+lon[j])/2,lat[i]-0.11])
        elif (i==0 and j==len(lon)): # limit lat at lon=0
            grid.append([lon[j-1]+0.11,lat[i]-0.11])

        elif (i==191 and j==0):  # limit lon at lat=0
            grid.append([lon[j]-0.11,lat[i-1]+0.11])
        elif (i==191 and j<len(lon)):
            grid.append([(lon[j-1]+lon[j])/2,lat[i-1]+0.11])
        elif (i==191 and j==len(lon)): # limit lat at lon=252
            grid.append([lon[j-1]+0.11,lat[i-1]+0.11])

        elif (j==0):
            grid.append([lon[j]-0.11,(lat[i-1]+lat[i])/2])
        elif (j%253==0):
            grid.append([lon[j-1]+0.11,(lat[i-1]+lat[i])/2])
        else:
            grid.append([(lon[j-1]+lon[j])/2,(lat[i-1]+lat[i])/2]) 

        if (i<len(lat) and j<len(lon)):
            lo = float("%.3f" % lon[j])
            la = float("%.3f" % lat[i])
            coord.append([lo,la])

print(len(coord))
#print(lat[190],lon[252])
# -------------------------------------------------------
# short cut float grid
for i in range(0,len(grid)):
    grid[i][0] = float("%.3f" %  grid[i][0])
    grid[i][1] = float("%.3f" %  grid[i][1])

'''
features = {
    "type": 'FeatureCollection',
    "": [],
}'''


'''
create table ars(
  id integer,
  point jsonb,
  coordinates jsonb,
  primary key (id)
)
'''
try:
    conn=psycopg2.connect("dbname='seaclid_ipsl' user='projectuser' password='ngaeprom7861'")
except:
    print ("I am unable to connect to the database.")

cnt = 0
in_id = 1
feature = []
for i in range(0,len(coord)):
    va={'id':[],'point':{},'coordinates':{}}
    va['id'] = in_id
    
    va['point'] = str(coord[i])

    '''
    if (i%48323==48322):
        print(i,cnt,'------------||')'''
    if (i%253==0):
        #print(cnt,'----------**')
        cnt = cnt+1
    #coordinates=[[grid[cnt-1],grid[cnt-1]]]
    va['coordinates']=str([[grid[cnt-1],grid[cnt+253],grid[cnt+254],grid[cnt],grid[cnt-1]]])
    #p=feature(point=coord[i],climdex={},coordinates=coordinates)
    #Grid.objects.create(id=i,point=coord[i],coordinates=coordinates)
    feature.append(va)
    in_id += 1
    cnt = cnt+1

cur = conn.cursor()
sql0 ="""INSERT INTO t01grid(grid_id,point,coordinates)
 VALUES (%(id)s, %(point)s, %(coordinates)s)"""

#Grid.objects.create(id=1,point=[20000, 25000],coordinates=[20000, 25000, 25000, 25000])
'''
CREATE TABLE emp (
  id integer,
  point float[1],
  coordinates float[][][],
  primary key (id)
);
'''
sql1 ="""INSERT INTO climate_grid(id,point,coordinates) VALUES (1,'[20000, 25000]','[20000, 25000, 25000, 25000]')"""

sql3 ="""INSERT INTO emp(id,point,coordinates) VALUES (2,ARRAY[20000, 25000],ARRAY[[[20000, 25000, 25000, 25000]]]"""

# >>print(feature[0])
# {'coordinates': '[[[89.379, -14.924], [89.379, -14.705], [89.602, -14.705], [89.602, -14.924], [89.379, -14.924]]]', 'point': '[89.489, -14.814]', 'id': 1}

cur.executemany(sql0,feature)

conn.commit()
conn.close()
print('---%s seconds---'%(time.time()-start_time))

