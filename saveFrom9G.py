import json
from readNetcdf import ReedNCFile, GridColor
import numpy as np

read = ReedNCFile()
lon = read.lon_data(1)
lat = read.lat_data(1)
lon_max = np.max(lon)
lon_min = np.min(lon)
lat_max = np.max(lat)
lat_min = np.min(lat)

coord = []
grid = []
allValue = []

# -------------------------------------------------------
# Creat grid and make grid map point, ts and pr
for i in range(0, len(lat)-1):  #len(lat)+1): 
    for j in range(0, len(lon)):  #len(lon)+1): 
        if (i%3==1 and j%3==1):  # begin lon at lat=0
            coord.append([lon[j],lat[i]])
            #grid.append([lon[j-1],lat[i-1]])
            #grid.append([lon[j-1],lat[i+1]])
            #grid.append([lon[j+1],lat[i+1]])
            #grid.append([lon[j+1],lat[i-1]])
            print('---i:',i,'-----j:',j)

            if (i==1 and j==1):  # limit lat and lon at 1
                grid.append([lon[j-1]-0.11, lat[i-1]-0.11 ])  # Bottom left
                grid.append([lon[j-1]-0.11, (lat[i+1]+lat[i+2])/2 ])  # Top left
                grid.append([(lon[j+1]+lon[j+2])/2, (lat[i+1]+lat[i+2])/2 ])  # Top right
                grid.append([(lon[j+1]+lon[j+2])/2, lat[i-1]-0.11 ])  # Bottom right
            elif (i==1 and j<251):  # limit lat=1 when lon less than 253 :: horizontal
                grid.append([(lon[j-1]+lon[j-2])/2, lat[i-1]-0.11])  # Bottom left
                grid.append([(lon[j-1]+lon[j-2])/2, (lat[i+1]+lat[i+2])/2 ])  # Top left
                grid.append([(lon[j+1]+lon[j+2])/2, (lat[i+1]+lat[i+2])/2 ])  # Top right
                grid.append([(lon[j+1]+lon[j+2])/2, lat[i-1]-0.11])  # Bottom right

            elif (i==190 and j==1): # limit lat=189 when lon at 1
                grid.append([lon[j-1]-0.11, (lat[i-1]+lat[i-2])/2 ])  # Bottom left
                grid.append([lon[j-1]-0.11, lat[i+1]+0.11 ])  # Top left
                grid.append([(lon[j+1]+lon[j+2])/2, lat[i+1]+0.11])  # Top right
                grid.append([(lon[j+1]+lon[j+2])/2, (lat[i-1]+lat[i-2])/2])  # Bottom right

            elif (i==190 and j<251):  #limit lat=189 when lon less then 253 :: horizontal
                grid.append([(lon[j-1]+lon[j-2])/2, (lat[i-1]+lat[i-2])/2 ])  # Bottom left
                grid.append([(lon[j-1]+lon[j-2])/2, lat[i+1]+0.11 ])  # Top left
                grid.append([(lon[j+1]+lon[j+2])/2, lat[i+1]+0.11])  # Top right
                grid.append([(lon[j+1]+lon[j+2])/2, (lat[i-1]+lat[i-2])/2])  # Bottom right


            elif (i==190 and j==251):  # final limit lat and lon 
                grid.append([(lon[j-1]+lon[j-2])/2, (lat[i-1]+lat[i-2])/2 ])  # Bottom left
                grid.append([(lon[j-1]+lon[j-2])/2, lat[i+1]+0.11 ])  # Top left
                grid.append([lon[j+1]+0.11, lat[i+1]+0.11])  # Top right
                grid.append([lon[j+1]+0.11, (lat[i-1]+lat[i-2])/2])  # Bottom right

            elif (j==1):  # Right grid row
                print("--------------- new row")
                grid.append([lon[j-1]-0.11, (lat[i-1]+lat[i-2])/2 ])  # Bottom left
                grid.append([lon[j-1]-0.11, (lat[i+1]+lat[i+2])/2 ])  # Top left
                grid.append([(lon[j+1]+lon[j+2])/2, (lat[i+1]+lat[i+2])/2 ])  # Top right
                grid.append([(lon[j+1]+lon[j+2])/2, (lat[i-1]+lat[i-2])/2])  # Bottom right

            elif (j%251==0):  # left grid row
                grid.append([(lon[j-1]+lon[j-2])/2, (lat[i-1]+lat[i-2])/2 ])  # Bottom left
                grid.append([(lon[j-1]+lon[j-2])/2, (lat[i+1]+lat[i+2])/2 ])  # Top left
                grid.append([lon[j+1]+0.11, (lat[i+1]+lat[i+2])/2])  # Top right
                grid.append([lon[j+1]+0.11, (lat[i-1]+lat[i-2])/2])  # Bottom right

            else:
            	#print("--------------- new error")
                grid.append([(lon[j-1]+lon[j-2])/2, (lat[i-1]+lat[i-2])/2 ])  # Bottom left
                grid.append([(lon[j-1]+lon[j-2])/2, (lat[i+1]+lat[i+2])/2 ])  # Top left
                grid.append([(lon[j+1]+lon[j+2])/2, (lat[i+1]+lat[i+2])/2 ])  # Top right
                grid.append([(lon[j+1]+lon[j+2])/2, (lat[i-1]+lat[i-2])/2 ])  # Bottom right

            allValue.append(0)

print(len(coord), len(grid))


# -------------------------------------------------------
# short cut float grid
for i in range(0,len(grid)):
    grid[i][0] = float("%.3f" %  grid[i][0])
    grid[i][1] = float("%.3f" %  grid[i][1])



# -------------------------------------------------------
# map grid,point,Ts and Pr to geojson 
geojson = {
    "type": 'FeatureCollection',
    "features": [],
}

cnt = 0
for i in range(0,len(coord)):
    feature = {
               "type": 'Feature',
               "properties":{"point":[],
                             "value":[]},
               "geometry": {
                            "type": 'Polygon',
                            "coordinates": []
              }
    }
    # assign type ts color from float32 to float64
    # tsColor[i] = tsColor[i].astype('float64')
    # c = np.array(sum_var)


    #feature['properties']['allts'] = alltsValue[i]
    #feature['properties']['allpr'] = allprValue[i]

    feature['properties']['value'] = allValue[i]

    feature['properties']['point'] = coord[i]

    if (i%11970==11969):
        print(i,cnt,'------------||')
    #if (i%253==0):
    #    print(cnt,'----------**')

    feature['geometry']['coordinates']=[[grid[cnt],grid[cnt+1],grid[cnt+2],grid[cnt+3],grid[cnt]]]
    cnt = cnt+4
    geojson['features'].append(feature)

out_file = open("geoG9Cliamte.json","w")
json.dump(geojson,out_file)
