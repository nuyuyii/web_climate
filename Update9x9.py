# 2084,2085

#from climate.models import Grid
import json
import numpy as np

from netCDF4 import Dataset
import datetime as dt 
from decimal import *
#-------------------Database Psql----------------------
import random, string, psycopg2,time
from string import ascii_lowercase
from random import randint


# load the psycopg extras module
import psycopg2.extras

start_time = time.time()

# 1970 - 2006
# 2006 - 2101
lisYear=range(2086,2087)
eyear=[format(x,'04d') for x in lisYear]
eindex=["ts_avg","ts_max","ts_min","pr_avg","pr_max","pr_min"]

eindex2=["index_su","index_fd","index_id",
    "index_tr","index_gsl","index_dtr",
    "index_txx","index_tnx","index_txn",
    "index_tnn","index_rx1day","index_rx5day",
    "index_sdii","index_r10mm","index_r20mm",
    "index_rnnmm","index_cdd","index_cwd"]

sql1 ="""INSERT INTO t9_measure_rpc85
         VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"""

sql2 = """INSERT INTO t9_index_rpc45
         VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s,
          %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""

sql3 = """UPDATE  t9_measure_rcp45
         SET ts_avg=%s, ts_max=%s, ts_min=%s, pr_avg=%s, pr_max=%s, pr_min=%s
         WHERE year=%s"""

sql4 = """UPDATE  t9_index_rcp45
         SET index_su=%s, index_fd=%s, index_id=%s,
          index_tr=%s, index_gsl=%s, index_dtr=%s,
          index_txx=%s, index_tnx=%s, index_txn=%s,    
          index_tnn=%s, index_rx1day=%s, index_rx5day=%s,
          index_sdii=%s, index_r10mm=%s, index_r20mm=%s,
          index_rnnmm=%s, index_cdd=%s, index_cwd=%s
         WHERE year=%s"""
# t9_measure_hist   -- t1_9measure_hist
# t9_measure_rcp45  -- t1_measure_rcp45
# t9_measure_rcp85  -- t1_measure_rcp85

# t9_index_hist  -- t1_index_hist
# t9_index_rcp45  -- t1_index_rcp45
# t9_index_rcp85  -- t1_index_rcp85

# t4_measure_hist   -- t1_measure_hist
# t4_measure_rcp45  -- t1_measure_rcp45
# t4_measure_rcp85  -- t1_measure_rcp85

# t4_index_hist  -- t1_index_hist
# t4_index_rcp45  -- t1_index_rcp45
# t4_index_rcp85  -- t1_index_rcp85

# 2086 ts_min missing -273.15 rcp45
# 2085 ts_min missing -273.15 rcp85
# 2084 ts_min missing -273.15 rcp85


try:
    conn = psycopg2.connect("dbname='db_ipsl' user='projectuser' password='ngaeprom7861'")
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    sql_hist = "SELECT * from t9_measure_rcp45 where year={myind}"

    cnt=1

    for year in lisYear:
        va={}
        print("year:  ",year)

        cur.execute(sql_hist.format(myind=str(2085)))
        row1 = cur.fetchall()

        cur.execute(sql_hist.format(myind=str(2088)))
        row2 = cur.fetchall()

        for i in range(0,len(eindex)):
            va[eindex[i]]=[]

#        print(np.min(row1[0]["ts_min"]))
#        print(np.min(row1[0]["ts_avg"]))

        for i in range(0,len(row1[0]["ts_max"])):
           for ind in range(0,len(eindex)):
                tmValue = []
                tmValue.append(row1[0][eindex[ind]][i])
                tmValue.append(row2[0][eindex[ind]][i])

                #if (ind==0 or ind==3):
                Value = float("%.2f" % (np.average(tmValue)))
                #elif (ind==1 or ind==4):
                #    Value = float("%.2f" % (np.max(tmValue)))
                #elif (ind==2 or ind==5):
                #    Value = float("%.2f" % (np.min(tmValue)))
        #        if (Value>0):
        #            print("Value: ",Value)
                va[eindex[ind]].append(Value)
            



        print("LEN---",len(va[eindex[0]]))
            #for i in range(0,len(eindex)):
           #     va[eindex[i]].append(row[eindex[i]][eyear[cnt-1]])
        #cur.execute(sql1,(va['years_id'], va['years'], str(va[eindex[0]]), str(va[eindex[1]]), str(va[eindex[2]]), str(va[eindex[3]]), str(va[eindex[4]]), str(va[eindex[5]])))
        #cur.execute(sql2,(va['years_id'], va['years'], str(va[eindex2[0]]), str(va[eindex2[1]]),
        #              str(va[eindex2[2]]), str(va[eindex2[3]]), str(va[eindex2[4]]), str(va[eindex2[5]]), 
        #              str(va[eindex2[6]]), str(va[eindex2[7]]), str(va[eindex2[8]]), str(va[eindex2[9]]),
        #              str(va[eindex2[10]]), str(va[eindex2[11]]), str(va[eindex2[12]]),
        #              str(va[eindex2[13]]), str(va[eindex2[14]]), str(va[eindex2[15]]),
        #              str(va[eindex2[16]]), str(va[eindex2[17]]) ))
#        cur.execute(sql4,(str(va[eindex2[0]]), str(va[eindex2[1]]),
#                      str(va[eindex2[2]]), str(va[eindex2[3]]), str(va[eindex2[4]]), str(va[eindex2[5]]), 
#                      str(va[eindex2[6]]), str(va[eindex2[7]]), str(va[eindex2[8]]), str(va[eindex2[9]]),
#                      str(va[eindex2[10]]), str(va[eindex2[11]]), str(va[eindex2[12]]),
#                      str(va[eindex2[13]]), str(va[eindex2[14]]), str(va[eindex2[15]]),
#                      str(va[eindex2[16]]), str(va[eindex2[17]]), str(year) ))        
        cur.execute(sql3,(str(va[eindex[0]]), str(va[eindex[1]]), str(va[eindex[2]]), str(va[eindex[3]]), str(va[eindex[4]]), str(va[eindex[5]]), str(year) ))
#        cur.execute(sql3,(str(va[eindex[0]]), str(va[eindex[1]]), str(va[eindex[2]]), str(va[eindex[3]]), str(va[eindex[4]]), str(va[eindex[5]]), "2085"))

        conn.commit()

        print ("insert OK : year",year," id:",cnt)
        cnt=cnt+1

except (psycopg2.DatabaseError, e):
    print ("I am unable to connect to the database.")
    print ('Error %s' % e )

#  pg_dump -t 't1_measure_r*' -t 't4_measure_r*' -t 't9_measure_r*' -t 'y130*' db_ipsl > t_graph.sql 

