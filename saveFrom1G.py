import json
from readNetcdf import ReedNCFile, GridColor
import numpy as np

read = ReedNCFile()
lon = read.lon_data(1)
lat = read.lat_data(1)
lon_max = np.max(lon)
lon_min = np.min(lon)
lat_max = np.max(lat)
lat_min = np.min(lat)
coord = []
grid = []
allValue = []

# -------------------------------------------------------
# Creat grid and make grid map point, ts and pr
for i in range(0,len(lat)+1):
    for j in range(0,len(lon)+1):
        if (i==0 and j==0):  # begin lon at lat=0
            grid.append([lon[j]-0.11,lat[i]-0.11])
        elif (i==0 and j<len(lon)):
            grid.append([(lon[j-1]+lon[j])/2,lat[i]-0.11])
        elif (i==0 and j==len(lon)): # limit lat at lon=0
            grid.append([lon[j-1]+0.11,lat[i]-0.11])

        elif (i==191 and j==0):  # limit lon at lat=0
            grid.append([lon[j]-0.11,lat[i-1]+0.11])
        elif (i==191 and j<len(lon)):
            grid.append([(lon[j-1]+lon[j])/2,lat[i-1]+0.11])
        elif (i==191 and j==len(lon)): # limit lat at lon=252
            grid.append([lon[j-1]+0.11,lat[i-1]+0.11])

        elif (j==0):
            grid.append([lon[j]-0.11,(lat[i-1]+lat[i])/2])
        elif (j%253==0):
            grid.append([lon[j-1]+0.11,(lat[i-1]+lat[i])/2])
        else:
            #print (i)
            #print (j)
            grid.append([(lon[j-1]+lon[j])/2,(lat[i-1]+lat[i])/2]) 

        if (i<len(lat) and j<len(lon)):
            coord.append([lon[j],lat[i]])
            allValue.append(0)

print(len(coord), len(grid))
print(lon_max,lat_max,coord[48322],"-----",allValue[48322])
print(lon_min,lat_min,coord[0],"-----",allValue[0])

# -------------------------------------------------------
# short cut float grid
for i in range(0,len(grid)):
    grid[i][0] = float("%.3f" %  grid[i][0])
    grid[i][1] = float("%.3f" %  grid[i][1])

# -------------------------------------------------------
# map grid,point,Ts and Pr to geojson 
geojson = {
    "type": 'FeatureCollection',
    "features": [],
}
cnt = 0
for i in range(0,len(coord)):
    feature = {
               "type": 'Feature',
               "properties":{"point":[],
                             "value":[]},
               "geometry": {
                            "type": 'Polygon',
                            "coordinates": []
              }
    }
    #allValue[i] = float("%.3f" % allValue[i])

    feature['properties']['value'] = allValue[i]
    feature['properties']['point'] = coord[i]

    if (i%48323==48322):
        print(i,cnt,'------------||')
    if (i%253==0):
        #print(cnt,'-----------**')
        cnt = cnt+1  #increase cnt when raw's end.     
    feature['geometry']['coordinates']=[[grid[cnt-1],grid[cnt+253],grid[cnt+254],grid[cnt],grid[cnt-1]]]
    geojson['features'].append(feature)

    cnt = cnt+1


out_file = open("geoG1Cliamte.json","w")
json.dump(geojson,out_file)