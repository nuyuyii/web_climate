import json
import os
import numpy as np
import math

from netCDF4 import Dataset, MFDataset
import datetime as dt 
from decimal import *
#-------------------Database Psql----------------------
import random, string, psycopg2,time
from string import ascii_lowercase
from random import randint

#-------------------import R---------------------------

from rpy2.robjects.packages import importr
import rpy2.robjects as robjects
from rpy2.robjects.packages import SignatureTranslatedAnonymousPackage

importr("climdex.pcic")
importr("PCICt")

string = """
library("climdex.pcic")
library(PCICt)

cal_date <- function(numday,str_date){
  cal <- "365_day"
  origin <- str_date
  seconds.per.day <- 86400
  ts.dat.days <- 1:numday
  origin.pcict <- as.PCICt(origin, cal)
  ts.dat.pcict <- origin.pcict + (ts.dat.days * seconds.per.day)
  return(ts.dat.pcict)
}

mdata <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  c_date <- cal_date(numday,str_date)
  ## Load the data in.
  ci_mydata <- climdexInput.raw(tmax,tmin,pr,c_date,c_date,c_date,base.range=c(str_year, end_year))
  return(ci_mydata)
}

su <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  tmax <- as.numeric(tmax)
  tmin <- as.numeric(tmin)
  pr <- as.numeric(pr)
  ci_mydata <- mdata(str_year,end_year,numday,str_date,tmax,tmin,pr)
  su_mydata <- climdex.su(ci_mydata)
  su_mydata <- round(su_mydata, digits=2)
  return(su_mydata)
}

fd <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  tmax <- as.numeric(tmax)
  tmin <- as.numeric(tmin)
  pr <- as.numeric(pr)
  ci_mydata <- mdata(str_year,end_year,numday,str_date,tmax,tmin,pr)
  fd_mydata <- climdex.fd(ci_mydata)
  fd_mydata <- round(fd_mydata, digits=2)
  return(fd_mydata)
}
id <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  tmax <- as.numeric(tmax)
  tmin <- as.numeric(tmin)
  pr <- as.numeric(pr)
  ci_mydata <- mdata(str_year,end_year,numday,str_date,tmax,tmin,pr)
  id_mydata <- climdex.id(ci_mydata)
  id_mydata <- round(id_mydata, digits=2)
  return(id_mydata)
}
tr <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  tmax <- as.numeric(tmax)
  tmin <- as.numeric(tmin)
  pr <- as.numeric(pr)
  ci_mydata <- mdata(str_year,end_year,numday,str_date,tmax,tmin,pr)
  tr_mydata <- climdex.tr(ci_mydata)
  tr_mydata <- round(tr_mydata, digits=2)
  return(tr_mydata)
}
gsl <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  tmax <- as.numeric(tmax)
  tmin <- as.numeric(tmin)
  pr <- as.numeric(pr)
  ci_mydata <- mdata(str_year,end_year,numday,str_date,tmax,tmin,pr)
  gsl_mydata <- climdex.gsl(ci_mydata)
  gsl_mydata <- round(gsl_mydata, digits=2)
  return(gsl_mydata)
}
wsdi <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  tmax <- as.numeric(tmax)
  tmin <- as.numeric(tmin)
  pr <- as.numeric(pr)
  ci_mydata <- mdata(str_year,end_year,numday,str_date,tmax,tmin,pr)
  wsdi_mydata <- climdex.wsdi(ci_mydata)
  wsdi_mydata <- round(wsdi_mydata, digits=2)
  return(wsdi_mydata)
}
csdi <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  tmax <- as.numeric(tmax)
  tmin <- as.numeric(tmin)
  pr <- as.numeric(pr)
  ci_mydata <- mdata(str_year,end_year,numday,str_date,tmax,tmin,pr)
  csdi_mydata <- climdex.csdi(ci_mydata)
  csdi_mydata <- round(csdi_mydata, digits=2)
  return(csdi_mydata)
}
dtr <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  tmax <- as.numeric(tmax)
  tmin <- as.numeric(tmin)
  pr <- as.numeric(pr)
  ci_mydata <- mdata(str_year,end_year,numday,str_date,tmax,tmin,pr)
  dtr_mydata <- climdex.dtr(ci_mydata,freq = c("annual"))
  dtr_mydata <- round(dtr_mydata, digits=2)
  return(dtr_mydata)
}
sdii <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  tmax <- as.numeric(tmax)
  tmin <- as.numeric(tmin)
  pr <- as.numeric(pr)
  ci_mydata <- mdata(str_year,end_year,numday,str_date,tmax,tmin,pr)
  sdii_mydata <- climdex.sdii(ci_mydata)
  sdii_mydata <- round(sdii_mydata, digits=2)
  return(sdii_mydata)
}
r10mm <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  tmax <- as.numeric(tmax)
  tmin <- as.numeric(tmin)
  pr <- as.numeric(pr)
  ci_mydata <- mdata(str_year,end_year,numday,str_date,tmax,tmin,pr)
  r10mm_mydata <- climdex.r10mm(ci_mydata)
  r10mm_mydata <- round(r10mm_mydata, digits=2)
  return(r10mm_mydata)
}
r20mm <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  tmax <- as.numeric(tmax)
  tmin <- as.numeric(tmin)
  pr <- as.numeric(pr)
  ci_mydata <- mdata(str_year,end_year,numday,str_date,tmax,tmin,pr)
  r20mm_mydata <- climdex.r20mm(ci_mydata)
  r20mm_mydata <- round(r20mm_mydata, digits=2)
  return(r20mm_mydata)
}
rnnmm <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  tmax <- as.numeric(tmax)
  tmin <- as.numeric(tmin)
  pr <- as.numeric(pr)
  ci_mydata <- mdata(str_year,end_year,numday,str_date,tmax,tmin,pr)
  rnnmm_mydata <- climdex.rnnmm(ci_mydata)
  rnnmm_mydata <- round(rnnmm_mydata, digits=2)
  return(rnnmm_mydata)
}
cdd <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  tmax <- as.numeric(tmax)
  tmin <- as.numeric(tmin)
  pr <- as.numeric(pr)
  ci_mydata <- mdata(str_year,end_year,numday,str_date,tmax,tmin,pr)
  cdd_mydata <- climdex.cdd(ci_mydata,spells.can.span.years = FALSE)
  cdd_mydata[is.na(cdd_mydata)] <- 0
  cdd_mydata <- round(cdd_mydata, digits=2)
  return(cdd_mydata)
}
cwd <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  tmax <- as.numeric(tmax)
  tmin <- as.numeric(tmin)
  pr <- as.numeric(pr)
  ci_mydata <- mdata(str_year,end_year,numday,str_date,tmax,tmin,pr)
  cwd_mydata <- climdex.cwd(ci_mydata,spells.can.span.years = FALSE)
  cwd_mydata[is.na(cwd_mydata)] <- 0
  cwd_mydata <- round(cwd_mydata, digits=2)
  return(cwd_mydata)
}
r95ptot <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  tmax <- as.numeric(tmax)
  tmin <- as.numeric(tmin)
  pr <- as.numeric(pr)
  ci_mydata <- mdata(str_year,end_year,numday,str_date,tmax,tmin,pr)
  r95ptot_mydata <- climdex.r95ptot(ci_mydata)
  r95ptot_mydata <- round(r95ptot_mydata, digits=2)
  return(r95ptot_mydata)
}
r99ptot <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  tmax <- as.numeric(tmax)
  tmin <- as.numeric(tmin)
  pr <- as.numeric(pr)
  ci_mydata <- mdata(str_year,end_year,numday,str_date,tmax,tmin,pr)
  r99ptot_mydata <- climdex.r99ptot(ci_mydata)
  r99ptot_mydata <- round(r99ptot_mydata, digits=2)
  return(r99ptot_mydata)
}
prcptot <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  tmax <- as.numeric(tmax)
  tmin <- as.numeric(tmin)
  pr <- as.numeric(pr)
  ci_mydata <- mdata(str_year,end_year,numday,str_date,tmax,tmin,pr)
  prcptot_mydata <- climdex.prcptot(ci_mydata)
  prcptot_mydata <- round(prcptot_mydata, digits=2)
  return(prcptot_mydata)
}
txx <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  tmax <- as.numeric(tmax)
  tmin <- as.numeric(tmin)
  pr <- as.numeric(pr)
  ci_mydata <- mdata(str_year,end_year,numday,str_date,tmax,tmin,pr)
  txx_mydata <- climdex.txx(ci_mydata,freq = c("annual"))
  txx_mydata <- round(txx_mydata, digits=2)
  return(txx_mydata)
}
tnx <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  tmax <- as.numeric(tmax)
  tmin <- as.numeric(tmin)
  pr <- as.numeric(pr)
  ci_mydata <- mdata(str_year,end_year,numday,str_date,tmax,tmin,pr)
  tnx_mydata <- climdex.tnx(ci_mydata,freq = c("annual"))
  tnx_mydata <- round(tnx_mydata, digits=2)
  return(tnx_mydata)
}
txn <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  tmax <- as.numeric(tmax)
  tmin <- as.numeric(tmin)
  pr <- as.numeric(pr)
  ci_mydata <- mdata(str_year,end_year,numday,str_date,tmax,tmin,pr)
  txn_mydata <- climdex.txn(ci_mydata,freq = c("annual"))
  txn_mydata <- round(txn_mydata, digits=2)
  return(txn_mydata)
}
tnn <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  tmax <- as.numeric(tmax)
  tmin <- as.numeric(tmin)
  pr <- as.numeric(pr)
  ci_mydata <- mdata(str_year,end_year,numday,str_date,tmax,tmin,pr)
  tnn_mydata <- climdex.tnn(ci_mydata,freq = c("annual"))
  tnn_mydata <- round(tnn_mydata, digits=2)
  return(tnn_mydata)
}
tn10p <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  tmax <- as.numeric(tmax)
  tmin <- as.numeric(tmin)
  pr <- as.numeric(pr)
  ci_mydata <- mdata(str_year,end_year,numday,str_date,tmax,tmin,pr)
  tn10p_mydata <- climdex.tn10p(ci_mydata,freq = c("annual"))
  tn10p_mydata <- round(tn10p_mydata, digits=2)
  return(tn10p_mydata)
}
tx10p <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  tmax <- as.numeric(tmax)
  tmin <- as.numeric(tmin)
  pr <- as.numeric(pr)
  ci_mydata <- mdata(str_year,end_year,numday,str_date,tmax,tmin,pr)
  tx10p_mydata <- climdex.tx10p(ci_mydata,freq = c("annual"))
  tx10p_mydata <- round(tx10p_mydata, digits=2)
  return(tx10p_mydata)
}
tn90p <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  tmax <- as.numeric(tmax)
  tmin <- as.numeric(tmin)
  pr <- as.numeric(pr)
  ci_mydata <- mdata(str_year,end_year,numday,str_date,tmax,tmin,pr)
  tn90p_mydata <- climdex.tn90p(ci_mydata,freq = c("annual"))
  tn90p_mydata <- round(tn90p_mydata, digits=2)
  return(tn90p_mydata)
}
tx90p <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  tmax <- as.numeric(tmax)
  tmin <- as.numeric(tmin)
  pr <- as.numeric(pr)
  ci_mydata <- mdata(str_year,end_year,numday,str_date,tmax,tmin,pr)
  tx90p_mydata <- climdex.tx90p(ci_mydata,freq = c("annual"))
  tx90p_mydata <- round(tx90p_mydata, digits=2)
  return(tx90p_mydata)
}
rx1day <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  tmax <- as.numeric(tmax)
  tmin <- as.numeric(tmin)
  pr <- as.numeric(pr)
  ci_mydata <- mdata(str_year,end_year,numday,str_date,tmax,tmin,pr)
  rx1day_mydata <- climdex.rx1day(ci_mydata,freq = c("annual"))
  rx1day_mydata <- round(rx1day_mydata, digits=2)
  return(rx1day_mydata)
}
rx5day <- function(str_year,end_year,numday,str_date,tmax,tmin,pr){
  tmax <- as.numeric(tmax)
  tmin <- as.numeric(tmin)
  pr <- as.numeric(pr)
  ci_mydata <- mdata(str_year,end_year,numday,str_date,tmax,tmin,pr)
  rx5day_mydata <- climdex.rx5day(ci_mydata,freq = c("annual"))
  #print(rx5day_mydata)
  rx5day_mydata <- round(rx5day_mydata, digits=2)
  return(rx5day_mydata)
}

"""

powerpack = SignatureTranslatedAnonymousPackage(string, "powerpack")

class climatecal(object):

    def __init__(self, stryear,endyear):
        self.stryear = stryear
        self.endyear = endyear
        self.lisYear = range(self.stryear,self.endyear)
        self.eyear = [format(x,'04d') for x in self.lisYear]
        #self.var_pr, self.var_tsmax, self.var_tsmin = self.read_nc()

    def read_nc(self):
        print("Reading data from a multi-file NetCDF dataset")
        var_pr = {}
        var_tsmax = {}
        var_tsmin = {}
        for y in self.eyear: #self.stryear,self.endyear):
            nc_file = MFDataset("/media/nuyuyii/DATA/seaclid/IPSL/RCP85/ipsl_rcp85_dd_T2m_Pr."+y+"*nc")
            var_pr[y] = nc_file.variables['pr']
            var_tsmax[y] = nc_file.variables['tasmax']
            var_tsmin[y] = nc_file.variables['tasmin']
        # print("Time:",len(nc_file.dimensions['time']))
        return var_pr, var_tsmax, var_tsmin

    def val_ltln(self, lat, lon):
        cut_sol1 = []
        cut_sol2 = []
        cut_sol3 = []

        for y in self.eyear:
            sol1 = self.var_pr[y][:,lat,lon]
            sol2 = self.var_tsmax[y][:,lat,lon]
            sol3 = self.var_tsmin[y][:,lat,lon]
            for z in range(0,len(sol1)):
                cut_sol1.append(sol1[z]*86400)
                cut_sol2.append(sol2[z]-273.15)
                cut_sol3.append(sol3[z]-273.15)

        #print("Time:",len(cut_sol1))
        #c = np.array(cut_sol)
        cut_sol1 = [float("%.2f" % e) for e in cut_sol1]
        cut_sol2 = [float("%.2f" % e) for e in cut_sol2]
        cut_sol3 = [float("%.2f" % e) for e in cut_sol3]
        return cut_sol1, cut_sol2, cut_sol3 

    def values(self, lat, lon):
        cut_sol1 = []
        cut_sol2 = []
        cut_sol3 = []
        for y in self.eyear: #self.stryear,self.endyear):
            # print("Reading data from a multi-file NetCDF dataset: %s " %y)
            nc_file = MFDataset("/media/nuyuyii/DATA/seaclid/IPSL/RCP85/ipsl_rcp85_dd_T2m_Pr."+y+"*nc")
            val_pr = nc_file.variables['pr']
            val_tsmax = nc_file.variables['tasmax']
            val_tsmin = nc_file.variables['tasmin']
            sol1 = val_pr[:,lat,lon]
            sol2 = val_tsmax[:,lat,lon]
            sol3 = val_tsmin[:,lat,lon]
            for z in range(0,len(sol1)):
                cut_sol1.append(sol1[z]*86400)
                cut_sol2.append(sol2[z]-273.15)
                cut_sol3.append(sol3[z]-273.15)

        #print("Time:",len(cut_sol1))
        #c = np.array(cut_sol)
        cut_sol1 = [float("%.2f" % e) for e in cut_sol1]
        cut_sol2 = [float("%.2f" % e) for e in cut_sol2]
        cut_sol3 = [float("%.2f" % e) for e in cut_sol3]
        return cut_sol1, cut_sol2, cut_sol3 

    def LtLn(self):
        m = Dataset('/media/nuyuyii/DATA/seaclid/IPSL/RCP85/ipsl_rcp85_dd_T2m_Pr.200601.nc', 'r')
        lons = m.variables['lon']
        lats = m.variables['lat']
        return lons, lats

# ---- Global Attribute ----- #
start_time = time.time()
eindex=["index_su","index_fd","index_id",
    "index_tr","index_gsl","index_dtr",
    "index_txx","index_tnx","index_txn",
    "index_tnn","index_rx1day","index_rx5day",
    "index_sdii","index_r10mm","index_r20mm",
    "index_rnnmm","index_cdd","index_cwd"]

lisYear=range(2006,2101)
eyear=[format(x,'04d') for x in lisYear]
#eyear=["1970","1971"]
#print('"',eyear[0],'"',u[1])
year=[2006,2101]
str_date = "2005-12-31"

print(year[0],year[1], len(eindex))


clim = climatecal(year[0],year[1])
lons, lats = clim.LtLn()
# print("pr:%s" % pr, "\nmax:%s" % tmax, "\nmin:%s" % tmin)


# ------------------ insert_record -------------------#
# ----------------------------------------------------#

def insert_record(features):
    conn = None

    sql1 ="""INSERT INTO ipsl85
         VALUES (%s, %s, %s, %s, %s, %s, %s, %s,
          %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""

    #st='{"1970":20,"1971":25}'
    #st = ff[0]['index_tnn']
    #print('ff---',ff[0])
    #print('features---',features[0])
    sta="{'1970':20,'1971':25}"
    sts={'1970':20,'1971':25}
    #print(sts,'---sts',str(sts),'---st:',st)
    try:
        conn=psycopg2.connect("dbname='db_ipsl' user='projectuser' password='ngaeprom7861'")
        cur = conn.cursor()
        # create table one 
        #cur.execute(sql1)
        #cur.execute(sql1,(ind, st, st, st, st, st, st, st, st, st, st, st, st, st, st, st, st, st, st))
        #cur.executemany(sql3,ff)
        # commit 

        #args_str = ','.join(cur.mogrify("(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s", x) for x in ff)
        #cur.execute("INSERT INTO table VALUES " + args_str) 
        #print("INSERT INTO table VALUES ",args_str)
        #cur.executemany(sql3,ff)
        #conn.commit()
        for fea in features:            
            cur.execute(sql1,(fea['grid_id'], fea[eindex[0]], fea[eindex[1]], fea[eindex[2]], fea[eindex[3]], fea[eindex[4]],
                              fea[eindex[5]], fea[eindex[6]], fea[eindex[7]], fea[eindex[8]], fea[eindex[9]], fea[eindex[10]],
                              fea[eindex[11]], fea[eindex[12]], fea[eindex[13]], fea[eindex[14]], fea[eindex[15]], fea[eindex[16]], fea[eindex[17]]))
            #print(fea['index_tnn'])
        print("--- execute OK ---")

        #print (features[0])
        #print (feature[0]['index_tnn']['1970'])
        # close communication with thte PostgreSQL database server
        cur.close()
    except Exception as e:
        print ("I am unable to connect to the database.")

    finally:
        if conn is not None:
            conn.close()

# ------------------ calR() --------------------------#
# ----------------------------------------------------#
def calR():
    feature=[]
    ind = 1
  # Note 21499 cdd Na Lat 191 : Lon 253
    for x in range(0,1):#len(lats)):
        for y in range(0,253):#len(lons)):
            pr, tmax, tmin = clim.val_ltln(x,y)
            value={}
            value['grid_id'] = ind
            res_su = powerpack.su(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
            res_fd = powerpack.fd(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
            res_id = powerpack.id(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
            res_tr = powerpack.tr(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
            res_gsl = powerpack.gsl(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
            res_dtr = powerpack.dtr(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
            res_txx = powerpack.txx(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
            res_tnx = powerpack.tnx(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
            res_txn = powerpack.txn(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
            res_tnn = powerpack.tnn(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
            res_rx1day = powerpack.rx1day(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
            res_rx5day = powerpack.rx5day(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
            res_sdii = powerpack.sdii(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
            res_r10mm = powerpack.r10mm(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
            res_r20mm = powerpack.r20mm(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
            res_rnnmm = powerpack.rnnmm(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
            res_cdd = powerpack.cdd(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
            res_cwd = powerpack.cwd(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
            #print (value[eindex[0]])

         
            #print (str(value[eindex[0]][0]))
            for i in range(0,len(eindex)):
                value[eindex[i]]={}

            for ey in range(0,len(eyear)):
                value[eindex[0]][eyear[ey]] = res_su[ey]
                value[eindex[1]][eyear[ey]] = res_fd[ey]
                value[eindex[2]][eyear[ey]] = res_id[ey]
                value[eindex[3]][eyear[ey]] = res_tr[ey]
                value[eindex[4]][eyear[ey]] = res_gsl[ey]
                value[eindex[5]][eyear[ey]] = res_dtr[ey]
                value[eindex[6]][eyear[ey]] = res_txx[ey]
                value[eindex[7]][eyear[ey]] = res_tnx[ey]
                value[eindex[8]][eyear[ey]] = res_txn[ey]
                value[eindex[9]][eyear[ey]] = res_tnn[ey]
                value[eindex[10]][eyear[ey]] = res_rx1day[ey]
                value[eindex[11]][eyear[ey]] = res_rx5day[ey]
                value[eindex[12]][eyear[ey]] = res_sdii[ey]
                value[eindex[13]][eyear[ey]] = res_r10mm[ey]
                value[eindex[14]][eyear[ey]] = res_r20mm[ey]
                value[eindex[15]][eyear[ey]] = res_rnnmm[ey]
                value[eindex[16]][eyear[ey]] = res_cdd[ey]
                value[eindex[17]][eyear[ey]] = res_cwd[ey]
                #print(eyear[83])
               # print (eindex[i],value[eindex[i]])  

            for i in range(0,len(eindex)):
                value[eindex[i]] = json.dumps(value[eindex[i]])

            #print(value[eindex[0]])
            #values = json.dumps(value)

            feature.append(value)
            ind=ind+1

        print(lats[x],'--',lons[y],";x ",x,"-y ",y,";ind ",ind-1)
    return feature



# ------------------ calInsert -----------------------#
# ----------------------------------------------------#

def calInsert():
    sql1 ="""INSERT INTO ipsl85
         VALUES (%s, %s, %s, %s, %s, %s, %s, %s,
          %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""
    try:
        conn=psycopg2.connect("dbname='db_ipsl' user='projectuser' password='ngaeprom7861'")
        cur = conn.cursor()
        
            #print(fea['index_tnn'])
        ind = 0
        for x in range(0,191):#len(lats)):
            ind, features = calIn(ind,x)
            for fea in features:            
                cur.execute(sql1,(fea['grid_id'], fea[eindex[0]], fea[eindex[1]], fea[eindex[2]], fea[eindex[3]], fea[eindex[4]],
                                  fea[eindex[5]], fea[eindex[6]], fea[eindex[7]], fea[eindex[8]], fea[eindex[9]], fea[eindex[10]],
                                  fea[eindex[11]], fea[eindex[12]], fea[eindex[13]], fea[eindex[14]], fea[eindex[15]], fea[eindex[16]], fea[eindex[17]]))
            #print(features[0]['index_cwd'])
            conn.commit() # commit 
        cur.close() # close communication with thte PostgreSQL database server
    except Exception as e:
        print ("I am unable to connect to the database.")

    finally:
        if conn is not None:
            conn.close()

# ------------------ calIn()  ------------------------#
# ----------------------------------------------------#
def calIn(ind,x):
    feature=[]
  # Note 21499 cdd Na Lat 191 : Lon 253
    for y in range(0,253):#len(lons)):
        pr, tmax, tmin = clim.values(x,y)
        value={}
        value['grid_id'] = ind
        res_su = powerpack.su(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
        res_fd = powerpack.fd(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
        res_id = powerpack.id(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
        res_tr = powerpack.tr(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
        res_gsl = powerpack.gsl(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
        res_dtr = powerpack.dtr(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
        res_txx = powerpack.txx(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
        res_tnx = powerpack.tnx(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
        res_txn = powerpack.txn(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
        res_tnn = powerpack.tnn(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
        res_rx1day = powerpack.rx1day(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
        res_rx5day = powerpack.rx5day(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
        res_sdii = powerpack.sdii(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
        res_r10mm = powerpack.r10mm(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
        res_r20mm = powerpack.r20mm(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
        res_rnnmm = powerpack.rnnmm(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
        res_cdd = powerpack.cdd(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
        res_cwd = powerpack.cwd(year[0],year[1],len(pr),str_date,tmax,tmin,pr)
            #print (value[eindex[0]])

         
            #print (str(value[eindex[0]][0]))
        for i in range(0,len(eindex)):
           value[eindex[i]]={}
        for ey in range(0,len(eyear)):
           value[eindex[0]][eyear[ey]] = res_su[ey]
           value[eindex[1]][eyear[ey]] = res_fd[ey]
           value[eindex[2]][eyear[ey]] = res_id[ey]
           value[eindex[3]][eyear[ey]] = res_tr[ey]
           value[eindex[4]][eyear[ey]] = res_gsl[ey]
           value[eindex[5]][eyear[ey]] = res_dtr[ey]
           value[eindex[6]][eyear[ey]] = res_txx[ey]
           value[eindex[7]][eyear[ey]] = res_tnx[ey]
           value[eindex[8]][eyear[ey]] = res_txn[ey]
           value[eindex[9]][eyear[ey]] = res_tnn[ey]
           value[eindex[10]][eyear[ey]] = res_rx1day[ey]
           value[eindex[11]][eyear[ey]] = res_rx5day[ey]
           value[eindex[12]][eyear[ey]] = res_sdii[ey]
           value[eindex[13]][eyear[ey]] = res_r10mm[ey]
           value[eindex[14]][eyear[ey]] = res_r20mm[ey]
           value[eindex[15]][eyear[ey]] = res_rnnmm[ey]
           value[eindex[16]][eyear[ey]] = res_cdd[ey]
           value[eindex[17]][eyear[ey]] = res_cwd[ey]

        for i in range(0,len(eindex)):
           value[eindex[i]] = json.dumps(value[eindex[i]])


        feature.append(value)
        ind=ind+1

    print(lats[x],'--',lons[y],";x ",x,"-y ",y,";ind ",ind-1)
    return ind, feature

if __name__ == '__main__':
    calInsert()
    #calR()
    #insert_record(feature)
# ind:33902 x:133 new start ind:33903 x:134
# ind:9614 x:37 new start ind:9615 x:38
# from climate.models import grid
# o=grid.objects.get(pk=1)
# create table one 

# select climdex -> 'fd' AS fd  from climate_feature where id = 1;
print('---%s seconds---'%(time.time()-start_time))
