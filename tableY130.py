# 2084,2085

#from climate.models import Grid
import json
import numpy as np

from netCDF4 import Dataset
import datetime as dt 
from decimal import *
#-------------------Database Psql----------------------
import random, string, psycopg2,time
from string import ascii_lowercase
from random import randint


# load the psycopg extras module
import psycopg2.extras

start_time = time.time()

# 1970 - 2006
# 2006 - 2101
lisYear=range(2006,2101)
eyear=[format(x,'04d') for x in lisYear]
eindex=["ts_avg","ts_max","ts_min","pr_avg","pr_max","pr_min"]

eindex2=["index_su","index_fd","index_id",
    "index_tr","index_gsl","index_dtr",
    "index_txx","index_tnx","index_txn",
    "index_tnn","index_rx1day","index_rx5day",
    "index_sdii","index_r10mm","index_r20mm",
    "index_rnnmm","index_cdd","index_cwd"]

sql1 ="""INSERT INTO y130allGrid (year_id, ts_avg,
         ts_max, ts_min, pr_avg, pr_max, pr_min, g_size, ipsl_file)
         VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"""

sql3 = """UPDATE  t9_measure_rcp45
         SET ts_avg=%s, ts_max=%s, ts_min=%s, pr_avg=%s, pr_max=%s, pr_min=%s
         WHERE year=%s"""

# t9_measure_hist   -- t1_9measure_hist
# t9_measure_rcp45  -- t1_measure_rcp45
# t9_measure_rcp85  -- t1_measure_rcp85

# t9_index_hist  -- t1_index_hist
# t9_index_rcp45  -- t1_index_rcp45
# t9_index_rcp85  -- t1_index_rcp85

# t4_measure_hist   -- t1_measure_hist
# t4_measure_rcp45  -- t1_measure_rcp45
# t4_measure_rcp85  -- t1_measure_rcp85

# t4_index_hist  -- t1_index_hist
# t4_index_rcp45  -- t1_index_rcp45
# t4_index_rcp85  -- t1_index_rcp85

# 2086 ts_min missing -273.15 rcp45
# 2085 ts_min missing -273.15 rcp85
# 2084 ts_min missing -273.15 rcp85


try:
    conn = psycopg2.connect("dbname='db_ipsl' user='projectuser' password='ngaeprom7861'")
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    sql_hist = "SELECT * from t9_measure_rcp85"
    sql_rcp45 = "SELECT * from t9_measure_rcp45 where year={myind}"
    sql_rcp85 = "SELECT * from t9_measure_rcp85 where year={myind}"
    cnt=3
    g_size = "3x3"
    ipsl_file = "rcp85"
    va = {}

    for i in range(0,len(eindex)):
        va[eindex[i]]=[]

    #for year in lisYear:
    #    print("year:  ",year)
    #    cur.execute(sql_hist.format(myind=str(year)))
    #    row1 = cur.fetchall()
    #    for i in range(0,len(eindex)):
    #        va[str(year)] =  row1[0][eindex[i]]    
    cur.execute(sql_hist)
    row1 = cur.fetchall()
    
    for ind in range(0,len(eindex)):  
        temFt = []  # for all year on each point         
        for i in range(0,len(row1[0]["ts_max"])):
            indxCal = []  # for keep all year
            for y in range(0,len(lisYear)):
                indxCal.append(row1[y][eindex[ind]][i])
            #if (ind==0 or ind==3):
            temFt.append(float("%.2f" % np.average(indxCal)) )
            #if (ind==1 or ind==4):
            #    temFt.append(float("%.2f" % np.max(indxCal)) )
            #if (ind==2 or ind==5):
            #    temFt.append(float("%.2f" % np.min(indxCal)) )

        va[eindex[ind]] = temFt
         



    print("cnt: ",cnt, "  LEN---",len(va[eindex[0]]))
    cur.execute(sql1,(cnt, str(va[eindex[0]]), str(va[eindex[1]]), str(va[eindex[2]]), str(va[eindex[3]]), str(va[eindex[4]]), str(va[eindex[5]]), g_size, ipsl_file))
    conn.commit()


except (psycopg2.DatabaseError, e):
    print ("I am unable to connect to the database.")
    print ('Error %s' % e )


